package uk.co.mortgagemagic.client

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import androidx.annotation.NonNull
//import com.google.gson.Gson
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class HrefLogin : FlutterActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val data: Uri? = intent.data
        if (data != null) {
            var scheme = data.scheme;
            var host = data.host;
            var args = data.pathSegments;
            if (args.count() > 0) {
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("email", args[0]);
                intent.putExtra("token", args[1]);
                startActivity(intent)
                this.finish()
            }
        }
    }
}