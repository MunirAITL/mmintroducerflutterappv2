import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/misc/FcmDeviceInfoHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/misc/FcmDeviceInfoAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class DeviceInfoAPIMgr with Mixin {
  static final DeviceInfoAPIMgr _shared = DeviceInfoAPIMgr._internal();

  factory DeviceInfoAPIMgr() {
    return _shared;
  }

  DeviceInfoAPIMgr._internal();

  wsFcmDeviceInfo({
    BuildContext context,
    Function(FcmDeviceInfoAPIModel) callback,
  }) async {
    try {
      final param = await FcmDeviceInfoHelper().getParam(
        userId: userData.userModel.id,
      );
      myLog(param);
      await NetworkMgr()
          .req<FcmDeviceInfoAPIModel, Null>(
        context: context,
        url: Server.FCM_DEVICE_INFO_URL,
        param: param,
        isLoading: false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
