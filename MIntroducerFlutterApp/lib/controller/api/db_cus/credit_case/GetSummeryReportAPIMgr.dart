import 'dart:convert';

import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDertailsAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditInfoParms.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditInfoPostAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/getKbaQuestionAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/getSummeryAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';
import 'package:json_string/json_string.dart';

class GetSummeryReportApiMgr with Mixin {
  static final GetSummeryReportApiMgr _shared =
      GetSummeryReportApiMgr._internal();

  factory GetSummeryReportApiMgr() {
    return _shared;
  }

  GetSummeryReportApiMgr._internal();

  getSummeryReport({
    BuildContext context,
    String validationIdentifier,
    int userId,
    int companyId,
    Function(GetSummeryAPIModel) callback,
  }) async {
    var params = {
      "UserIdentifier": validationIdentifier,
      "UserId": userId,
      "UserCompanyId": companyId
    };
    //print("getSummeryReport bank is = $params");
    final jsonString = JsonString(json.encode(params));
    myLog(jsonString.source);
    try {
      await NetworkMgr()
          .req<GetSummeryAPIModel, Null>(
        context: context,
        reqType: ReqType.Post,
        param: params,
        url: Server.GET_SUMMARY_REPORT_URL,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog("GET_SUMMARY_REPORT_URL  ERROR = " + e.toString());
    }
  }
}
