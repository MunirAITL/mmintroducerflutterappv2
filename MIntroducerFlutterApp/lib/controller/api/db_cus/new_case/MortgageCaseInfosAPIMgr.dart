import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/CaseInfosAPIModel.dart';
import 'package:flutter/material.dart';

class MortgageCaseInfosAPIMgr with Mixin {
  static final MortgageCaseInfosAPIMgr _shared =
      MortgageCaseInfosAPIMgr._internal();

  factory MortgageCaseInfosAPIMgr() {
    return _shared;
  }

  MortgageCaseInfosAPIMgr._internal();

  static wsGetUserCaseInfos(
      {BuildContext context,
      String taskID,
      Function(CaseInfosApiModel) callback}) async {
    try {
      await NetworkMgr()
          .req<CaseInfosApiModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: Server.CASE_INFO_URL.replaceAll("#TaskId#", taskID.toString()),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      // myLog(e.toString());
      print("Error res = " + e.toString());
    }
  }
}
