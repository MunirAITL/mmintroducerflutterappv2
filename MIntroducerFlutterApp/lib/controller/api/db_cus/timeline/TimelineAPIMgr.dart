import 'package:aitl/controller/helper/db_cus/tab_timeline/TimeLineAdvisorHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLineAdvisorAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class TimelineAPIMgr with Mixin {
  static final TimelineAPIMgr _shared = TimelineAPIMgr._internal();

  factory TimelineAPIMgr() {
    return _shared;
  }

  TimelineAPIMgr._internal();

  wsOnPageLoad({
    BuildContext context,
    int pageStart,
    int pageCount,
    int status,
    Function(TimeLineAdvisorAPIModel) callback,
  }) async {
    try {
      final url = TimeLineAdvisorHelper().getUrl(
        pageStart: pageStart,
        pageCount: pageCount,
      );
      myLog("Message list url = " + url);
      await NetworkMgr()
          .req<TimeLineAdvisorAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
