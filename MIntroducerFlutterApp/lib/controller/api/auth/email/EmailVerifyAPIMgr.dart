import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/auth/email/VerifyEmailAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class EmailVerifyAPIMgr with Mixin {
  static final EmailVerifyAPIMgr _shared = EmailVerifyAPIMgr._internal();

  factory EmailVerifyAPIMgr() {
    return _shared;
  }

  EmailVerifyAPIMgr._internal();

  wsEmailVerifyAPI({
    BuildContext context,
    String code,
    Function(VerifyEmailAPIModel) callback,
  }) async {
    try {
      await NetworkMgr().req<VerifyEmailAPIModel, Null>(
        context: context,
        url: Server.EMAILVERIFY_URL,
        param: {'code': code},
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
