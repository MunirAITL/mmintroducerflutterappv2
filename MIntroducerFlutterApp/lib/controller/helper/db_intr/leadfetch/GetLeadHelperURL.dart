import 'package:aitl/config/Server.dart';
import 'package:aitl/config/ServerIntr.dart';

class GetLeadHelperURL {
  static getUrl(
      {String stage,
      String leadStatus,
      String assigneeId,
      int count,
      String criteria,
      String caseOwner,
      String sortBy,
      String searchText,
      String dateTimeSelectOption,
      String fromDateTime,
      String initiatorId,
      String negotiatorId,
      int page,
      String parentId,
      String resolutionType,
      String taskStatus,
      String toDateTime,
      String userCompanyId}) {
    var url = ServerIntr.LEAD_LIST_GET_URL;
    url = url.replaceAll("#Stage#", stage);
    url = url.replaceAll("#LeadStatus#", leadStatus);
    url = url.replaceAll("#AssigneeId#", assigneeId);
    url = url.replaceAll("#Count#", count.toString());
    url = url.replaceAll("#Criteria#", criteria);
    url = url.replaceAll("#FromDateTime#", fromDateTime);
    url = url.replaceAll("#InitiatorId#", initiatorId);
    url = url.replaceAll("#NegotiatorId#", negotiatorId);
    url = url.replaceAll("#Page#", page.toString());
    url = url.replaceAll("#ParentId#", parentId);
    url = url.replaceAll("#ResolutionType#", resolutionType);
    url = url.replaceAll("#TaskStatus#", taskStatus);
    url = url.replaceAll("#ToDateTime#", toDateTime);
    url = url.replaceAll("#UserCompanyId#", userCompanyId);
    url = url.replaceAll("#OrderBy#", sortBy);
    url = url.replaceAll("#SearchText#", searchText);
    url = url.replaceAll("#DateTimeSelectOption#", dateTimeSelectOption);
    return url;
  }
}
