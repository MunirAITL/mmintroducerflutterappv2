import 'package:aitl/config/Server.dart';

class SendOtpNotiHelper {
  getUrl({int otpId}) {
    var url = Server.SEND_OTP_NOTI_URL;
    url = url.replaceAll("#otpId#", otpId.toString());
    return url;
  }
}
