import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/UserData.dart';

class TimeLineAdvisorHelper {
  getUrl({
    pageStart,
    pageCount,
  }) {
    var url = Server.TIMELINE_ADVISOR_URL;
    url = url.replaceAll("#userId#", userData.userModel.id.toString());
    url = url.replaceAll(
        "#communityId#", userData.userModel.communityID.toString());
    url = url.replaceAll(
        "#companyId#", userData.userModel.userCompanyID.toString());
    url = url.replaceAll("#count#", pageCount.toString());
    url = url.replaceAll("#page#", pageStart.toString());
    return url;
  }
}
