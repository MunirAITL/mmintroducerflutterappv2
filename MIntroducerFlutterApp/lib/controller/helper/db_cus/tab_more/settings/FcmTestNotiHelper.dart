import 'package:aitl/config/Server.dart';

class FcmTestNotiHelper {
  getUrl({int userId}) {
    var url = Server.FCM_TEST_NOTI_URL;
    url = url.replaceAll("#userId#", userId.toString());
    return url;
  }
}
