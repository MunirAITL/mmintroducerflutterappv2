import 'package:aitl/Mixin.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';

class HomeCfg {
  static final Color red = Colors.red;
  static final Color lYellow = HexColor.fromHex("#FFFF99");
  static final Color yellow = Colors.yellow;
  static final Color lGreen = HexColor.fromHex("#90ee90");
  static final Color green = Colors.green;
}
