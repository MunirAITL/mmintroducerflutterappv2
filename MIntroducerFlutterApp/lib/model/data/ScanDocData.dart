import 'dart:io';

class ScanDocData {
  static final ScanDocData _scanData = new ScanDocData._internal();

  factory ScanDocData() {
    return _scanData;
  }

  File file_selfie;

  File file_drvlic_front;

  File file_pp_front;

  int requestId;

  ScanDocData._internal();
}

final scanDocData = ScanDocData();
