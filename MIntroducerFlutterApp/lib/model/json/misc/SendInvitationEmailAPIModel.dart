class SendInvitationEmailAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  SendInvitationEmailAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  SendInvitationEmailAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> registrationWelcomeemail;
  Messages({this.registrationWelcomeemail});
  Messages.fromJson(Map<String, dynamic> json) {
    registrationWelcomeemail = json['registration_welcomeemail'].cast<String>();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['registration_welcomeemail'] = this.registrationWelcomeemail;
    return data;
  }
}

class ResponseData {
  User user;
  ResponseData({this.user});
  ResponseData.fromJson(Map<String, dynamic> json) {
    user = json['User'] != null ? new User.fromJson(json['User']) : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    return data;
  }
}

class User {
  String firstName;
  String middleName;
  String namePrefix;
  String lastName;
  String name;
  String email;
  String userName;
  String guid;
  String password;
  String passwordSalt;
  int passwordFormat;
  bool active;
  String dateCreated;
  String dateUpdated;
  String lastLoginDate;
  bool isSystemAccount;
  String remarks;
  String lastLoginIpAddress;
  int referrerId;
  bool deleted;
  String stanfordWorkplaceURL;
  String headline;
  String briefBio;
  String referenceId;
  String referenceType;
  String token;
  bool isFirstLogin;
  String linkedUrl;
  String twitterUrl;
  String faceBookUrl;
  String cohort;
  String communityId;
  bool isOnline;
  String logOutTime;
  String userRoles;
  double taskCompletionRate;
  double averageRating;
  String mobileNumber;
  String dateofBirth;
  String address;
  String latitude;
  String longitude;
  String workHistory;
  String userTaskCategoryId;
  double userBalanceAmount;
  int status;
  String referenceUserId;
  String areYouASmoker;
  String countryCode;
  int groupId;
  int id;

  User(
      {this.firstName,
      this.middleName,
      this.namePrefix,
      this.lastName,
      this.name,
      this.email,
      this.userName,
      this.guid,
      this.password,
      this.passwordSalt,
      this.passwordFormat,
      this.active,
      this.dateCreated,
      this.dateUpdated,
      this.lastLoginDate,
      this.isSystemAccount,
      this.remarks,
      this.lastLoginIpAddress,
      this.referrerId,
      this.deleted,
      this.stanfordWorkplaceURL,
      this.headline,
      this.briefBio,
      this.referenceId,
      this.referenceType,
      this.token,
      this.isFirstLogin,
      this.linkedUrl,
      this.twitterUrl,
      this.faceBookUrl,
      this.cohort,
      this.communityId,
      this.isOnline,
      this.logOutTime,
      this.userRoles,
      this.taskCompletionRate,
      this.averageRating,
      this.mobileNumber,
      this.dateofBirth,
      this.address,
      this.latitude,
      this.longitude,
      this.workHistory,
      this.userTaskCategoryId,
      this.userBalanceAmount,
      this.status,
      this.referenceUserId,
      this.areYouASmoker,
      this.countryCode,
      this.groupId,
      this.id});

  User.fromJson(Map<String, dynamic> json) {
    firstName = json['FirstName'];
    middleName = json['MiddleName'];
    namePrefix = json['NamePrefix'];
    lastName = json['LastName'];
    name = json['Name'];
    email = json['Email'];
    userName = json['UserName'];
    guid = json['Guid'];
    password = json['Password'];
    passwordSalt = json['PasswordSalt'];
    passwordFormat = json['PasswordFormat'];
    active = json['Active'];
    dateCreated = json['DateCreated'];
    dateUpdated = json['DateUpdated'];
    lastLoginDate = json['LastLoginDate'];
    isSystemAccount = json['IsSystemAccount'];
    remarks = json['Remarks'];
    lastLoginIpAddress = json['LastLoginIpAddress'];
    referrerId = json['ReferrerId'];
    deleted = json['Deleted'];
    stanfordWorkplaceURL = json['StanfordWorkplaceURL'];
    headline = json['Headline'];
    briefBio = json['BriefBio'];
    referenceId = json['ReferenceId'];
    referenceType = json['ReferenceType'];
    token = json['Token'];
    isFirstLogin = json['IsFirstLogin'];
    linkedUrl = json['LinkedUrl'];
    twitterUrl = json['TwitterUrl'];
    faceBookUrl = json['FaceBookUrl'];
    cohort = json['Cohort'];
    communityId = json['CommunityId'];
    isOnline = json['IsOnline'];
    logOutTime = json['LogOutTime'];
    userRoles = json['UserRoles'];
    taskCompletionRate = json['TaskCompletionRate'];
    averageRating = json['AverageRating'];
    mobileNumber = json['MobileNumber'];
    dateofBirth = json['DateofBirth'];
    address = json['Address'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
    workHistory = json['WorkHistory'];
    userTaskCategoryId = json['UserTaskCategoryId'];
    userBalanceAmount = json['UserBalanceAmount'];
    status = json['Status'];
    referenceUserId = json['ReferenceUserId'];
    areYouASmoker = json['AreYouASmoker'];
    countryCode = json['CountryCode'];
    groupId = json['GroupId'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FirstName'] = this.firstName;
    data['MiddleName'] = this.middleName;
    data['NamePrefix'] = this.namePrefix;
    data['LastName'] = this.lastName;
    data['Name'] = this.name;
    data['Email'] = this.email;
    data['UserName'] = this.userName;
    data['Guid'] = this.guid;
    data['Password'] = this.password;
    data['PasswordSalt'] = this.passwordSalt;
    data['PasswordFormat'] = this.passwordFormat;
    data['Active'] = this.active;
    data['DateCreated'] = this.dateCreated;
    data['DateUpdated'] = this.dateUpdated;
    data['LastLoginDate'] = this.lastLoginDate;
    data['IsSystemAccount'] = this.isSystemAccount;
    data['Remarks'] = this.remarks;
    data['LastLoginIpAddress'] = this.lastLoginIpAddress;
    data['ReferrerId'] = this.referrerId;
    data['Deleted'] = this.deleted;
    data['StanfordWorkplaceURL'] = this.stanfordWorkplaceURL;
    data['Headline'] = this.headline;
    data['BriefBio'] = this.briefBio;
    data['ReferenceId'] = this.referenceId;
    data['ReferenceType'] = this.referenceType;
    data['Token'] = this.token;
    data['IsFirstLogin'] = this.isFirstLogin;
    data['LinkedUrl'] = this.linkedUrl;
    data['TwitterUrl'] = this.twitterUrl;
    data['FaceBookUrl'] = this.faceBookUrl;
    data['Cohort'] = this.cohort;
    data['CommunityId'] = this.communityId;
    data['IsOnline'] = this.isOnline;
    data['LogOutTime'] = this.logOutTime;
    data['UserRoles'] = this.userRoles;
    data['TaskCompletionRate'] = this.taskCompletionRate;
    data['AverageRating'] = this.averageRating;
    data['MobileNumber'] = this.mobileNumber;
    data['DateofBirth'] = this.dateofBirth;
    data['Address'] = this.address;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['WorkHistory'] = this.workHistory;
    data['UserTaskCategoryId'] = this.userTaskCategoryId;
    data['UserBalanceAmount'] = this.userBalanceAmount;
    data['Status'] = this.status;
    data['ReferenceUserId'] = this.referenceUserId;
    data['AreYouASmoker'] = this.areYouASmoker;
    data['CountryCode'] = this.countryCode;
    data['GroupId'] = this.groupId;
    data['Id'] = this.id;
    return data;
  }
}
