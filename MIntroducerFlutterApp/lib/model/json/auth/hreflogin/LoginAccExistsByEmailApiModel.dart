import 'PostEmailOtpAPIModel.dart';
import 'SendUserEmailOtpAPIModel.dart';

class LoginAccExistsByEmailApiModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  LoginAccExistsByEmailApiModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  LoginAccExistsByEmailApiModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  List<dynamic> login;
  ErrorMessages({this.login});

  ErrorMessages.fromJson(Map<String, dynamic> json) {
    login = json['login'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['login'] = this.login;
    return data;
  }
}

class Messages {
  List<dynamic> login;
  Messages({this.login});

  Messages.fromJson(Map<String, dynamic> json) {
    login = json['login'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['login'] = this.login;
    return data;
  }
}

class ResponseData {
  UserOTP userOTP;
  ResponseData({this.userOTP});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userOTP =
        json['UserOTP'] != null ? new UserOTP.fromJson(json['UserOTP']) : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userOTP != null) {
      data['UserOTP'] = this.userOTP.toJson();
    }
    return data;
  }
}
