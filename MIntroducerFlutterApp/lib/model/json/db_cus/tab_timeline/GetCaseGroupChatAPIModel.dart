class GetCaseGroupChatAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetCaseGroupChatAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetCaseGroupChatAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<CaseGroupMessageData> caseGroupMessageData;
  ResponseData({this.caseGroupMessageData});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['CaseGroupMessageData'] != null) {
      caseGroupMessageData = <CaseGroupMessageData>[];
      json['CaseGroupMessageData'].forEach((v) {
        caseGroupMessageData.add(new CaseGroupMessageData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.caseGroupMessageData != null) {
      data['CaseGroupMessageData'] =
          this.caseGroupMessageData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CaseGroupMessageData {
  int id;
  String title;
  String description;
  String addressOfPropertyToBeMortgaged;
  String howMuchDoYouWishToBorrow;
  String lastMessage;
  String isRead;
  String lastMessageDateTime;

  CaseGroupMessageData(
      {this.id,
      this.title,
      this.description,
      this.addressOfPropertyToBeMortgaged,
      this.howMuchDoYouWishToBorrow,
      this.lastMessage,
      this.isRead,
      this.lastMessageDateTime});

  CaseGroupMessageData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    title = json['Title'] ?? '';
    description = json['Description'] ?? '';
    addressOfPropertyToBeMortgaged =
        json['AddressOfPropertyToBeMortgaged'] ?? '';
    howMuchDoYouWishToBorrow = json['HowMuchDoYouWishToBorrow'] ?? '';
    lastMessage = json['LastMessage'] ?? '';
    isRead = json['IsRead'] ?? 'false';
    lastMessageDateTime = json['LastMessageDateTime'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['Title'] = this.title;
    data['Description'] = this.description;
    data['AddressOfPropertyToBeMortgaged'] =
        this.addressOfPropertyToBeMortgaged;
    data['HowMuchDoYouWishToBorrow'] = this.howMuchDoYouWishToBorrow;
    data['LastMessage'] = this.lastMessage;
    data['IsRead'] = this.isRead;
    data['LastMessageDateTime'] = this.lastMessageDateTime;
    return data;
  }
}
