
class SubmitCaseAPIModel {
  bool success;
  dynamic errorMessages;
  dynamic messages;

  SubmitCaseAPIModel(
      {this.success, this.errorMessages, this.messages});

  factory SubmitCaseAPIModel.fromJson(Map<String, dynamic> j) {
    return SubmitCaseAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},

    );
  }

  Map<String, dynamic> toMap() => {
    'Success': success,
    'ErrorMessages': errorMessages,
    'Messages': messages,
  };
}
