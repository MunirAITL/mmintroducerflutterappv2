import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/UserData.dart';

class PrivacyPolicyHelper {
  getUrl() {
    var url = Server.TERMS_POLICY;
    url = url.replaceAll(
        "#UserCompanyId#", userData.userModel.userCompanyID.toString());
    return url;
  }
}
