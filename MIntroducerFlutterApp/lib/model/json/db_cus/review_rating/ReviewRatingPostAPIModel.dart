import 'package:aitl/view/db_cus/timeline/chat/TimeLinePostScreen.dart';

class ReviewRatingPostAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  ReviewRatingPostAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  ReviewRatingPostAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  UserRating userRating;
  ResponseData({this.userRating});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userRating = json['UserRating'] != null
        ? new UserRating.fromJson(json['UserRating'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userRating != null) {
      data['UserRating'] = this.userRating.toJson();
    }
    return data;
  }
}

class UserRating {
  int userId;
  User user;
  int status;
  String creationDate;
  double rating;
  int taskBiddingId;
  int employeeId;
  String comments;
  String initiatorName;
  String taskTitle;
  bool isPoster;
  int taskId;
  int reviewRatingId;
  String profileImageUrl;
  String employeeName;
  String taskTitleUrl;
  String ownerProfileUrl;
  String reviewRatingTitile;
  String reviewRatingUserType;
  int id;

  UserRating(
      {this.userId,
      this.user,
      this.status,
      this.creationDate,
      this.rating,
      this.taskBiddingId,
      this.employeeId,
      this.comments,
      this.initiatorName,
      this.taskTitle,
      this.isPoster,
      this.taskId,
      this.reviewRatingId,
      this.profileImageUrl,
      this.employeeName,
      this.taskTitleUrl,
      this.ownerProfileUrl,
      this.reviewRatingTitile,
      this.reviewRatingUserType,
      this.id});

  UserRating.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    status = json['Status'];
    creationDate = json['CreationDate'];
    rating = json['Rating'];
    taskBiddingId = json['TaskBiddingId'];
    employeeId = json['EmployeeId'];
    comments = json['Comments'];
    initiatorName = json['InitiatorName'];
    taskTitle = json['TaskTitle'];
    isPoster = json['IsPoster'];
    taskId = json['TaskId'];
    reviewRatingId = json['ReviewRatingId'];
    profileImageUrl = json['ProfileImageUrl'];
    employeeName = json['EmployeeName'];
    taskTitleUrl = json['TaskTitleUrl'];
    ownerProfileUrl = json['OwnerProfileUrl'];
    reviewRatingTitile = json['ReviewRatingTitile'];
    reviewRatingUserType = json['ReviewRatingUserType'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = User().toJson();
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['Rating'] = this.rating;
    data['TaskBiddingId'] = this.taskBiddingId;
    data['EmployeeId'] = this.employeeId;
    data['Comments'] = this.comments;
    data['InitiatorName'] = this.initiatorName;
    data['TaskTitle'] = this.taskTitle;
    data['IsPoster'] = this.isPoster;
    data['TaskId'] = this.taskId;
    data['ReviewRatingId'] = this.reviewRatingId;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['EmployeeName'] = this.employeeName;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['OwnerProfileUrl'] = this.ownerProfileUrl;
    data['ReviewRatingTitile'] = this.reviewRatingTitile;
    data['ReviewRatingUserType'] = this.reviewRatingUserType;
    data['Id'] = this.id;
    return data;
  }
}
