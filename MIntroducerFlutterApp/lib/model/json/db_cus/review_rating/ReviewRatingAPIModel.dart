class ReviewRatingAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  ReviewRatingAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  ReviewRatingAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<ReviewRatingFormSetups> reviewRatingFormSetups;
  ResponseData({this.reviewRatingFormSetups});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['ReviewRatingFormSetups'] != null) {
      reviewRatingFormSetups = <ReviewRatingFormSetups>[];
      json['ReviewRatingFormSetups'].forEach((v) {
        reviewRatingFormSetups.add(new ReviewRatingFormSetups.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.reviewRatingFormSetups != null) {
      data['ReviewRatingFormSetups'] =
          this.reviewRatingFormSetups.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ReviewRatingFormSetups {
  int userId;
  int userCompanyId;
  int status;
  String creationDate;
  String reviewRatingUserType;
  String title;
  String titleInformation;
  int ratingCount;
  String ratingType;
  String commentBoxTitle;
  String commentBoxInformation;
  String remarks;
  int taskId;
  int reviewRatingId;
  int userRatingCount;
  String commentsData;
  int id;

  ReviewRatingFormSetups(
      {this.userId,
      this.userCompanyId,
      this.status,
      this.creationDate,
      this.reviewRatingUserType,
      this.title,
      this.titleInformation,
      this.ratingCount,
      this.ratingType,
      this.commentBoxTitle,
      this.commentBoxInformation,
      this.remarks,
      this.taskId,
      this.reviewRatingId,
      this.userRatingCount,
      this.commentsData,
      this.id});

  ReviewRatingFormSetups.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    userCompanyId = json['UserCompanyId'] ?? 0;
    status = json['Status'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    reviewRatingUserType = json['ReviewRatingUserType'] ?? '';
    title = json['Title'] ?? '';
    titleInformation = json['TitleInformation'] ?? '';
    ratingCount = json['RatingCount'] ?? 0;
    ratingType = json['RatingType'] ?? '';
    commentBoxTitle = json['CommentBoxTitle'] ?? '';
    commentBoxInformation = json['CommentBoxInformation'] ?? '';
    remarks = json['Remarks'] ?? '';
    taskId = json['TaskId'] ?? 0;
    reviewRatingId = json['ReviewRatingId'] ?? 0;
    userRatingCount = json['UserRatingCount'] ?? 0;
    commentsData = json['CommentsData'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['ReviewRatingUserType'] = this.reviewRatingUserType;
    data['Title'] = this.title;
    data['TitleInformation'] = this.titleInformation;
    data['RatingCount'] = this.ratingCount;
    data['RatingType'] = this.ratingType;
    data['CommentBoxTitle'] = this.commentBoxTitle;
    data['CommentBoxInformation'] = this.commentBoxInformation;
    data['Remarks'] = this.remarks;
    data['TaskId'] = this.taskId;
    data['ReviewRatingId'] = this.reviewRatingId;
    data['UserRatingCount'] = this.userRatingCount;
    data['CommentsData'] = this.commentsData;
    data['Id'] = this.id;
    return data;
  }
}
