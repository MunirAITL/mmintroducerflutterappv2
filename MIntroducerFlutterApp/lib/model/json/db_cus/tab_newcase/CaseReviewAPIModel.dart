
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseInfos.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseRecomendationInfos.dart';

class CaseReviewAPIModel{


  bool success;
  //_ErrorMessages errorMessages;
  //_Messages messages;
  dynamic errorMessages;
  dynamic messages;
  ResponseData responseData;

  CaseReviewAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory CaseReviewAPIModel.fromJson(Map<String, dynamic> j) {
    return CaseReviewAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
    'Success': success,
    'ErrorMessages': errorMessages,
    'Messages': messages,
    'ResponseData': responseData,
  };
}
class ResponseData {
  List<MortgageCaseRecomendationInfos> caseDocumentReviewModelList;
  List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList;

  ResponseData({this.caseDocumentReviewModelList,this.caseMortgageAgreementReviewInfosList});
  ResponseData.fromJson(Map<String, dynamic> json) {
    //catch agreement review
    if (json['MortgageCaseInfos'] != null) {

      caseMortgageAgreementReviewInfosList = [];
      json['MortgageCaseInfos'].forEach((v) {
        try {

          caseMortgageAgreementReviewInfosList.add(new MortgageCaseInfos.fromJson(v));

        } catch (e) {
          print("Test task Jason getting not add because  = ${e.toString()}");

        }
      });
    }else{
      print("Test task Jason getting null = ${json['mortgageCaseRecomendationInfos']}");
    }
    //catch document review
    if (json['MortgageCaseRecomendationInfos'] != null) {

      caseDocumentReviewModelList = [];
      json['MortgageCaseRecomendationInfos'].forEach((v) {
        try {

          caseDocumentReviewModelList.add(new MortgageCaseRecomendationInfos.fromJson(v));

        } catch (e) {
          print("Test task Jason getting not add because  = ${e.toString()}");

        }
      });
    }else{
      print("Test task Jason getting null = ${json['mortgageCaseRecomendationInfos']}");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.caseDocumentReviewModelList != null) {
      try {
        data['MortgageCaseRecomendationInfos'] = this.caseDocumentReviewModelList.map((v) => v.toJson()).toList();
      } catch (e) {}
    }
    return data;
  }
}
/*

class _ResponseData {
  List<dynamic> mortgageCaseRecomendationInfos;
  _ResponseData({this.mortgageCaseRecomendationInfos});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    var list_Locations = [];
    try {
      list_Locations = (j['MortgageCaseRecomendationInfos'] != null)
          ? j['MortgageCaseRecomendationInfos'].map((i) => CaseReviewModel.fromJson(i)).toList()
          : [];
    } catch (e) {
      print(e.toString());
    }
    return _ResponseData(mortgageCaseRecomendationInfos: list_Locations ?? []);
  }
  Map<String, dynamic> toMap() => {
    'MortgageCaseRecomendationInfos': mortgageCaseRecomendationInfos,
  };
}
*/
