
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseInfoEntityModelListModel.dart';

class CaseInfosApiModel {
  bool success;
  //_ErrorMessages errorMessages;
  //_Messages messages;
  dynamic errorMessages;
  dynamic messages;
  _ResponseData responseData;

  CaseInfosApiModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory CaseInfosApiModel.fromJson(Map<String, dynamic> j) {
    return CaseInfosApiModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
    'Success': success,
    'ErrorMessages': errorMessages,
    'Messages': messages,
    'ResponseData': responseData,
  };
}

class _ResponseData {
  List<dynamic> caseInfosEntityModel;
  _ResponseData({this.caseInfosEntityModel});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    var list_Locations = [];
    try {
      list_Locations = (j['MortgageCaseInfos'] != null)
          ? j['MortgageCaseInfos'].map((i) => MortgageCaseInfoEntityModelListModel.fromJson(i)).toList()
          : [];
    } catch (e) {
      print(e.toString());
    }
    return _ResponseData(caseInfosEntityModel: list_Locations ?? []);
  }
  Map<String, dynamic> toMap() => {
    'MortgageCaseInfos': caseInfosEntityModel,
  };
}
