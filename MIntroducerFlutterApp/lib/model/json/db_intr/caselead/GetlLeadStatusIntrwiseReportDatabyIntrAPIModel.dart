import 'LeadStatusAdviserWiseReportDatasModel.dart';

class GetlLeadStatusIntrwiseReportDatabyIntrAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  GetlLeadStatusIntrwiseReportDatabyIntrAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetlLeadStatusIntrwiseReportDatabyIntrAPIModel.fromJson(
      Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  Messages();
  Messages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<LeadStatusAdviserWiseReportDatasModel> leadStatusAdviserWiseReportDatas;
  ResponseData({this.leadStatusAdviserWiseReportDatas});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['LeadStatusAdviserWiseReportDatas'] != null) {
      leadStatusAdviserWiseReportDatas = [];
      json['LeadStatusAdviserWiseReportDatas'].forEach((v) {
        leadStatusAdviserWiseReportDatas
            .add(new LeadStatusAdviserWiseReportDatasModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.leadStatusAdviserWiseReportDatas != null) {
      data['LeadStatusAdviserWiseReportDatas'] =
          this.leadStatusAdviserWiseReportDatas.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
