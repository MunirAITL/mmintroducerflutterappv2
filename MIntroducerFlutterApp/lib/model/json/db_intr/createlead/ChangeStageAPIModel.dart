import 'ResolutionModel.dart';

class ChangeStageAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  ChangeStageAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  ChangeStageAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  List<dynamic> resolutionPost;
  ErrorMessages({this.resolutionPost});
  ErrorMessages.fromJson(Map<String, dynamic> json) {
    resolutionPost = json['resolution_post'] ?? [];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['resolution_post'] = this.resolutionPost;
    return data;
  }
}

class Messages {
  Messages();
  Messages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  ResolutionModel resolution;
  ResponseData({this.resolution});
  ResponseData.fromJson(Map<String, dynamic> json) {
    resolution = json['Resolution'] != null
        ? new ResolutionModel.fromJson(json['Resolution'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resolution != null) {
      data['Resolution'] = this.resolution.toJson();
    }
    return data;
  }
}
