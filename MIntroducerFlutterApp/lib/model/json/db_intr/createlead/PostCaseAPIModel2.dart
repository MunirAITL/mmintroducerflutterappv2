import '../../db_cus/tab_newcase/LocationsModel.dart';

class PostCaseAPIModel2 {
  bool success;
  //_ErrorMessages errorMessages;
  //_Messages messages;
  dynamic errorMessages;
  dynamic messages;
  _ResponseData responseData;

  PostCaseAPIModel2(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory PostCaseAPIModel2.fromJson(Map<String, dynamic> j) {
    return PostCaseAPIModel2(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ErrorMessages {}

class _Messages {
  List<dynamic> task_post;
  _Messages({this.task_post});
  factory _Messages.fromJson(Map<String, dynamic> j) {
    return _Messages(
      task_post: j['task_post'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'task_post': task_post,
      };
}

class _ResponseData {
  LocationsModel task;
  _ResponseData({this.task});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    var taskMap = (j['Task'] != null) ? LocationsModel.fromJson(j['Task']) : {};
    return _ResponseData(task: taskMap);
  }
  Map<String, dynamic> toMap() => {
        'Task': task,
      };
}
