class UserDetailsDataAutoSuggAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  UserDetailsDataAutoSuggAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  UserDetailsDataAutoSuggAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<UserDetailsDatas> userDetailsDatas;
  ResponseData({this.userDetailsDatas});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['UserDetailsDatas'] != null) {
      userDetailsDatas = <UserDetailsDatas>[];
      json['UserDetailsDatas'].forEach((v) {
        userDetailsDatas.add(new UserDetailsDatas.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userDetailsDatas != null) {
      data['UserDetailsDatas'] =
          this.userDetailsDatas.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UserDetailsDatas {
  int id;
  String name;
  int status;
  String firstName;
  String lastName;
  String address;
  String middleName;
  String userName;
  String email;
  String mobileNumber;
  String namePrefix;
  String lastLoginDate;
  String dateUpdated;
  String communityId;
  String communityName;
  String imageServerUrl;
  String thumbnailPath;
  String profileImageUrl;
  int userCompanyId;
  String companyName;
  int active;
  String lastLoginDateUtc;
  String lastLoginDateLocal;
  String createdUser;
  int isFirstLogin;
  String stanfordWorkplaceURL;
  String groupName;
  String publishDate;
  int unreadCount;
  String departmentName;
  String brokerCompanyName;

  UserDetailsDatas(
      {this.id,
      this.name,
      this.status,
      this.firstName,
      this.lastName,
      this.address,
      this.middleName,
      this.userName,
      this.email,
      this.mobileNumber,
      this.namePrefix,
      this.lastLoginDate,
      this.dateUpdated,
      this.communityId,
      this.communityName,
      this.imageServerUrl,
      this.thumbnailPath,
      this.profileImageUrl,
      this.userCompanyId,
      this.companyName,
      this.active,
      this.lastLoginDateUtc,
      this.lastLoginDateLocal,
      this.createdUser,
      this.isFirstLogin,
      this.stanfordWorkplaceURL,
      this.groupName,
      this.publishDate,
      this.unreadCount,
      this.departmentName,
      this.brokerCompanyName});

  UserDetailsDatas.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['Name'];
    status = json['Status'];
    firstName = json['FirstName'];
    lastName = json['LastName'];
    address = json['Address'];
    middleName = json['MiddleName'];
    userName = json['UserName'];
    email = json['Email'];
    mobileNumber = json['MobileNumber'];
    namePrefix = json['NamePrefix'];
    lastLoginDate = json['LastLoginDate'];
    dateUpdated = json['DateUpdated'];
    communityId = json['CommunityId'];
    communityName = json['CommunityName'];
    imageServerUrl = json['ImageServerUrl'];
    thumbnailPath = json['ThumbnailPath'];
    profileImageUrl = json['ProfileImageUrl'];
    userCompanyId = json['UserCompanyId'];
    companyName = json['CompanyName'];
    active = json['Active'];
    lastLoginDateUtc = json['LastLoginDateUtc'];
    lastLoginDateLocal = json['LastLoginDateLocal'];
    createdUser = json['CreatedUser'];
    isFirstLogin = json['IsFirstLogin'];
    stanfordWorkplaceURL = json['StanfordWorkplaceURL'];
    groupName = json['GroupName'];
    publishDate = json['PublishDate'];
    unreadCount = json['UnreadCount'];
    departmentName = json['DepartmentName'];
    brokerCompanyName = json['BrokerCompanyName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['Name'] = this.name;
    data['Status'] = this.status;
    data['FirstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['Address'] = this.address;
    data['MiddleName'] = this.middleName;
    data['UserName'] = this.userName;
    data['Email'] = this.email;
    data['MobileNumber'] = this.mobileNumber;
    data['NamePrefix'] = this.namePrefix;
    data['LastLoginDate'] = this.lastLoginDate;
    data['DateUpdated'] = this.dateUpdated;
    data['CommunityId'] = this.communityId;
    data['CommunityName'] = this.communityName;
    data['ImageServerUrl'] = this.imageServerUrl;
    data['ThumbnailPath'] = this.thumbnailPath;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['UserCompanyId'] = this.userCompanyId;
    data['CompanyName'] = this.companyName;
    data['Active'] = this.active;
    data['LastLoginDateUtc'] = this.lastLoginDateUtc;
    data['LastLoginDateLocal'] = this.lastLoginDateLocal;
    data['CreatedUser'] = this.createdUser;
    data['IsFirstLogin'] = this.isFirstLogin;
    data['StanfordWorkplaceURL'] = this.stanfordWorkplaceURL;
    data['GroupName'] = this.groupName;
    data['PublishDate'] = this.publishDate;
    data['UnreadCount'] = this.unreadCount;
    data['DepartmentName'] = this.departmentName;
    data['BrokerCompanyName'] = this.brokerCompanyName;
    return data;
  }
}
