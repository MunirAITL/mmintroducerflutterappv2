class ResolutionModel {
  int userId;
  String creationDate;
  int status;
  String title;
  String description;
  String remarks;
  String emailAddress;
  String phoneNumber;
  int initiatorId;
  String serviceDate;
  String resolutionType;
  int parentId;
  String profileImageUrl;
  String profileOwnerName;
  String complainee;
  String complaineeUrl;
  int assigneeId;
  String assigneeName;
  String assignDate;
  String resolvedDate;
  String taskStatus;
  String userComunity;
  String resolutionsUrl;
  int userCompanyId;
  String leadStatus;
  String leadNote;
  String firstName;
  String middleName;
  String namePrefix;
  String lastName;
  String initiatorImageUrl;
  String initiatorName;
  String initiatorCompanyName;
  String stage;
  int probability;
  String companyName;
  String fileUrl;
  String leadReferenceId;
  String dateOfBirth;
  String address;
  String mortgageLengthLeft;
  String homeValue;
  String mortgagePurpose;
  String mortgageAmount;
  String mortgageBorrowLength;
  String creditRating;
  String employmentStatus;
  String yearlyIncome;
  String emailConsent;
  String qualifier;
  double estimatedEarning;
  String forwardedCompanyName;
  int createdByUserId;
  String createdByUserName;
  int isLockAutoCall;
  int supportAdminId;
  String userNoteEntityName;
  int userNoteEntityId;
  int negotiatorId;
  String negotiatorName;
  int groupId;
  String areYouChargingAFee;
  double chargeFeeAmount;
  String chargingFeeWhenPayable;
  String chargingFeeRefundable;
  String areYouChargingAnotherFee;
  double chargeAnotherFeeAmount;
  String chargingAnotherFeeWhenPayable;
  String chargingAnotherFeeRefundable;
  String notePermissionType;
  int consultantId;
  String consultantName;
  int id;

  ResolutionModel(
      {this.userId,
      this.creationDate,
      this.status,
      this.title,
      this.description,
      this.remarks,
      this.emailAddress,
      this.phoneNumber,
      this.initiatorId,
      this.serviceDate,
      this.resolutionType,
      this.parentId,
      this.profileImageUrl,
      this.profileOwnerName,
      this.complainee,
      this.complaineeUrl,
      this.assigneeId,
      this.assigneeName,
      this.assignDate,
      this.resolvedDate,
      this.taskStatus,
      this.userComunity,
      this.resolutionsUrl,
      this.userCompanyId,
      this.leadStatus,
      this.leadNote,
      this.firstName,
      this.middleName,
      this.namePrefix,
      this.lastName,
      this.initiatorImageUrl,
      this.initiatorName,
      this.initiatorCompanyName,
      this.stage,
      this.probability,
      this.companyName,
      this.fileUrl,
      this.leadReferenceId,
      this.dateOfBirth,
      this.address,
      this.mortgageLengthLeft,
      this.homeValue,
      this.mortgagePurpose,
      this.mortgageAmount,
      this.mortgageBorrowLength,
      this.creditRating,
      this.employmentStatus,
      this.yearlyIncome,
      this.emailConsent,
      this.qualifier,
      this.estimatedEarning,
      this.forwardedCompanyName,
      this.createdByUserId,
      this.createdByUserName,
      this.isLockAutoCall,
      this.supportAdminId,
      this.userNoteEntityName,
      this.userNoteEntityId,
      this.negotiatorId,
      this.negotiatorName,
      this.groupId,
      this.areYouChargingAFee,
      this.chargeFeeAmount,
      this.chargingFeeWhenPayable,
      this.chargingFeeRefundable,
      this.areYouChargingAnotherFee,
      this.chargeAnotherFeeAmount,
      this.chargingAnotherFeeWhenPayable,
      this.chargingAnotherFeeRefundable,
      this.notePermissionType,
      this.consultantId,
      this.consultantName,
      this.id});

  ResolutionModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    creationDate = json['CreationDate'];
    status = json['Status'];
    title = json['Title'];
    description = json['Description'];
    remarks = json['Remarks'];
    emailAddress = json['EmailAddress'];
    phoneNumber = json['PhoneNumber'];
    initiatorId = json['InitiatorId'];
    serviceDate = json['ServiceDate'];
    resolutionType = json['ResolutionType'];
    parentId = json['ParentId'];
    profileImageUrl = json['ProfileImageUrl'];
    profileOwnerName = json['ProfileOwnerName'];
    complainee = json['Complainee'];
    complaineeUrl = json['ComplaineeUrl'];
    assigneeId = json['AssigneeId'];
    assigneeName = json['AssigneeName'];
    assignDate = json['AssignDate'];
    resolvedDate = json['ResolvedDate'];
    taskStatus = json['TaskStatus'];
    userComunity = json['UserComunity'];
    resolutionsUrl = json['ResolutionsUrl'];
    userCompanyId = json['UserCompanyId'];
    leadStatus = json['LeadStatus'];
    leadNote = json['LeadNote'];
    firstName = json['FirstName'];
    middleName = json['MiddleName'];
    namePrefix = json['NamePrefix'];
    lastName = json['LastName'];
    initiatorImageUrl = json['InitiatorImageUrl'];
    initiatorName = json['InitiatorName'];
    initiatorCompanyName = json['InitiatorCompanyName'];
    stage = json['Stage'];
    probability = json['Probability'];
    companyName = json['CompanyName'];
    fileUrl = json['FileUrl'];
    leadReferenceId = json['LeadReferenceId'];
    dateOfBirth = json['DateOfBirth'];
    address = json['Address'];
    mortgageLengthLeft = json['MortgageLengthLeft'];
    homeValue = json['HomeValue'];
    mortgagePurpose = json['MortgagePurpose'];
    mortgageAmount = json['MortgageAmount'];
    mortgageBorrowLength = json['MortgageBorrowLength'];
    creditRating = json['CreditRating'];
    employmentStatus = json['EmploymentStatus'];
    yearlyIncome = json['YearlyIncome'];
    emailConsent = json['EmailConsent'];
    qualifier = json['Qualifier'];
    estimatedEarning = json['EstimatedEarning'];
    forwardedCompanyName = json['ForwardedCompanyName'];
    createdByUserId = json['CreatedByUserId'];
    createdByUserName = json['CreatedByUserName'];
    isLockAutoCall = json['IsLockAutoCall'];
    supportAdminId = json['SupportAdminId'];
    userNoteEntityName = json['UserNoteEntityName'];
    userNoteEntityId = json['UserNoteEntityId'];
    negotiatorId = json['NegotiatorId'];
    negotiatorName = json['NegotiatorName'];
    groupId = json['GroupId'];
    areYouChargingAFee = json['AreYouChargingAFee'];
    chargeFeeAmount = json['ChargeFeeAmount'];
    chargingFeeWhenPayable = json['ChargingFeeWhenPayable'];
    chargingFeeRefundable = json['ChargingFeeRefundable'];
    areYouChargingAnotherFee = json['AreYouChargingAnotherFee'];
    chargeAnotherFeeAmount = json['ChargeAnotherFeeAmount'];
    chargingAnotherFeeWhenPayable = json['ChargingAnotherFeeWhenPayable'];
    chargingAnotherFeeRefundable = json['ChargingAnotherFeeRefundable'];
    notePermissionType = json['NotePermissionType'];
    consultantId = json['ConsultantId'];
    consultantName = json['ConsultantName'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['CreationDate'] = this.creationDate;
    data['Status'] = this.status;
    data['Title'] = this.title;
    data['Description'] = this.description;
    data['Remarks'] = this.remarks;
    data['EmailAddress'] = this.emailAddress;
    data['PhoneNumber'] = this.phoneNumber;
    data['InitiatorId'] = this.initiatorId;
    data['ServiceDate'] = this.serviceDate;
    data['ResolutionType'] = this.resolutionType;
    data['ParentId'] = this.parentId;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['ProfileOwnerName'] = this.profileOwnerName;
    data['Complainee'] = this.complainee;
    data['ComplaineeUrl'] = this.complaineeUrl;
    data['AssigneeId'] = this.assigneeId;
    data['AssigneeName'] = this.assigneeName;
    data['AssignDate'] = this.assignDate;
    data['ResolvedDate'] = this.resolvedDate;
    data['TaskStatus'] = this.taskStatus;
    data['UserComunity'] = this.userComunity;
    data['ResolutionsUrl'] = this.resolutionsUrl;
    data['UserCompanyId'] = this.userCompanyId;
    data['LeadStatus'] = this.leadStatus;
    data['LeadNote'] = this.leadNote;
    data['FirstName'] = this.firstName;
    data['MiddleName'] = this.middleName;
    data['NamePrefix'] = this.namePrefix;
    data['LastName'] = this.lastName;
    data['InitiatorImageUrl'] = this.initiatorImageUrl;
    data['InitiatorName'] = this.initiatorName;
    data['InitiatorCompanyName'] = this.initiatorCompanyName;
    data['Stage'] = this.stage;
    data['Probability'] = this.probability;
    data['CompanyName'] = this.companyName;
    data['FileUrl'] = this.fileUrl;
    data['LeadReferenceId'] = this.leadReferenceId;
    data['DateOfBirth'] = this.dateOfBirth;
    data['Address'] = this.address;
    data['MortgageLengthLeft'] = this.mortgageLengthLeft;
    data['HomeValue'] = this.homeValue;
    data['MortgagePurpose'] = this.mortgagePurpose;
    data['MortgageAmount'] = this.mortgageAmount;
    data['MortgageBorrowLength'] = this.mortgageBorrowLength;
    data['CreditRating'] = this.creditRating;
    data['EmploymentStatus'] = this.employmentStatus;
    data['YearlyIncome'] = this.yearlyIncome;
    data['EmailConsent'] = this.emailConsent;
    data['Qualifier'] = this.qualifier;
    data['EstimatedEarning'] = this.estimatedEarning;
    data['ForwardedCompanyName'] = this.forwardedCompanyName;
    data['CreatedByUserId'] = this.createdByUserId;
    data['CreatedByUserName'] = this.createdByUserName;
    data['IsLockAutoCall'] = this.isLockAutoCall;
    data['SupportAdminId'] = this.supportAdminId;
    data['UserNoteEntityName'] = this.userNoteEntityName;
    data['UserNoteEntityId'] = this.userNoteEntityId;
    data['NegotiatorId'] = this.negotiatorId;
    data['NegotiatorName'] = this.negotiatorName;
    data['GroupId'] = this.groupId;
    data['AreYouChargingAFee'] = this.areYouChargingAFee;
    data['ChargeFeeAmount'] = this.chargeFeeAmount;
    data['ChargingFeeWhenPayable'] = this.chargingFeeWhenPayable;
    data['ChargingFeeRefundable'] = this.chargingFeeRefundable;
    data['AreYouChargingAnotherFee'] = this.areYouChargingAnotherFee;
    data['ChargeAnotherFeeAmount'] = this.chargeAnotherFeeAmount;
    data['ChargingAnotherFeeWhenPayable'] = this.chargingAnotherFeeWhenPayable;
    data['ChargingAnotherFeeRefundable'] = this.chargingAnotherFeeRefundable;
    data['NotePermissionType'] = this.notePermissionType;
    data['ConsultantId'] = this.consultantId;
    data['ConsultantName'] = this.consultantName;
    data['Id'] = this.id;
    return data;
  }
}
