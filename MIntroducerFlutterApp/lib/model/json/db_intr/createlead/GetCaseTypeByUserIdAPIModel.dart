class GetCaseTypeByUserIdAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetCaseTypeByUserIdAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetCaseTypeByUserIdAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  UserCaseTypePermission userCaseTypePermission;
  ResponseData({this.userCaseTypePermission});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userCaseTypePermission = json['UserCaseTypePermission'] != null
        ? new UserCaseTypePermission.fromJson(json['UserCaseTypePermission'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userCaseTypePermission != null) {
      data['UserCaseTypePermission'] = this.userCaseTypePermission.toJson();
    }
    return data;
  }
}

class UserCaseTypePermission {
  int userId;
  bool isResidentialMortgage;
  bool isResidentialRemortgage;
  bool isSecondChargeResidential;
  bool isBuytoLetMortgage;
  bool isBuytoLetRemortgage;
  bool isLettoBuy;
  bool isSecondChargeBuytoLetCommercial;
  bool isCommercialMortgagesLoans;
  bool isBridgingLoan;
  bool isBusinessLending;
  bool isDevelopmentFinance;
  bool isProtectionHub;
  bool isEquityRelease;
  bool isBuildingAndContents;
  String caseType;
  int id;

  UserCaseTypePermission(
      {this.userId,
      this.isResidentialMortgage,
      this.isResidentialRemortgage,
      this.isSecondChargeResidential,
      this.isBuytoLetMortgage,
      this.isBuytoLetRemortgage,
      this.isLettoBuy,
      this.isSecondChargeBuytoLetCommercial,
      this.isCommercialMortgagesLoans,
      this.isBridgingLoan,
      this.isBusinessLending,
      this.isDevelopmentFinance,
      this.isProtectionHub,
      this.isEquityRelease,
      this.isBuildingAndContents,
      this.caseType,
      this.id});

  UserCaseTypePermission.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    isResidentialMortgage = json['IsResidentialMortgage'];
    isResidentialRemortgage = json['IsResidentialRemortgage'];
    isSecondChargeResidential = json['IsSecondChargeResidential'];
    isBuytoLetMortgage = json['IsBuytoLetMortgage'];
    isBuytoLetRemortgage = json['IsBuytoLetRemortgage'];
    isLettoBuy = json['IsLettoBuy'];
    isSecondChargeBuytoLetCommercial = json['IsSecondChargeBuytoLetCommercial'];
    isCommercialMortgagesLoans = json['IsCommercialMortgagesLoans'];
    isBridgingLoan = json['IsBridgingLoan'];
    isBusinessLending = json['IsBusinessLending'];
    isDevelopmentFinance = json['IsDevelopmentFinance'];
    isProtectionHub = json['IsProtectionHub'];
    isEquityRelease = json['IsEquityRelease'];
    isBuildingAndContents = json['IsBuildingAndContents'];
    caseType = json['CaseType'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['IsResidentialMortgage'] = this.isResidentialMortgage;
    data['IsResidentialRemortgage'] = this.isResidentialRemortgage;
    data['IsSecondChargeResidential'] = this.isSecondChargeResidential;
    data['IsBuytoLetMortgage'] = this.isBuytoLetMortgage;
    data['IsBuytoLetRemortgage'] = this.isBuytoLetRemortgage;
    data['IsLettoBuy'] = this.isLettoBuy;
    data['IsSecondChargeBuytoLetCommercial'] =
        this.isSecondChargeBuytoLetCommercial;
    data['IsCommercialMortgagesLoans'] = this.isCommercialMortgagesLoans;
    data['IsBridgingLoan'] = this.isBridgingLoan;
    data['IsBusinessLending'] = this.isBusinessLending;
    data['IsDevelopmentFinance'] = this.isDevelopmentFinance;
    data['IsProtectionHub'] = this.isProtectionHub;
    data['IsEquityRelease'] = this.isEquityRelease;
    data['IsBuildingAndContents'] = this.isBuildingAndContents;
    data['CaseType'] = this.caseType;
    data['Id'] = this.id;
    return data;
  }
}
