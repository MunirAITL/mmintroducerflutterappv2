class PostCaseNoteAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  PostCaseNoteAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostCaseNoteAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  UserNote userNote;
  ResponseData({this.userNote});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userNote = json['UserNote'] != null
        ? new UserNote.fromJson(json['UserNote'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userNote != null) {
      data['UserNote'] = this.userNote.toJson();
    }
    return data;
  }
}

class UserNote {
  int userId;
  String user;
  String status;
  String creationDate;
  String title;
  String comments;
  String serviceDate;
  bool isTag;
  int initiatorId;
  String initiatorName;
  String profileImageUrl;
  String type;
  int leadId;
  String priority;
  String category;
  String complainMode;
  String noteCategory;
  String serviceEndDate;
  int adviserId;
  String adviserName;
  String directionType;
  String resolutionsUrl;
  int entityId;
  String entityName;
  String email;
  String emailStatus;
  String eventId;
  int assigneeId;
  String ownerName;
  String ownerEmail;
  String phoneNumber;
  String dateofBirth;
  String address;
  String securityAddress;
  String userType;
  String assigneeName;
  String stage;
  int id;

  UserNote(
      {this.userId,
      this.user,
      this.status,
      this.creationDate,
      this.title,
      this.comments,
      this.serviceDate,
      this.isTag,
      this.initiatorId,
      this.initiatorName,
      this.profileImageUrl,
      this.type,
      this.leadId,
      this.priority,
      this.category,
      this.complainMode,
      this.noteCategory,
      this.serviceEndDate,
      this.adviserId,
      this.adviserName,
      this.directionType,
      this.resolutionsUrl,
      this.entityId,
      this.entityName,
      this.email,
      this.emailStatus,
      this.eventId,
      this.assigneeId,
      this.ownerName,
      this.ownerEmail,
      this.phoneNumber,
      this.dateofBirth,
      this.address,
      this.securityAddress,
      this.userType,
      this.assigneeName,
      this.stage,
      this.id});

  UserNote.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    title = json['Title'];
    comments = json['Comments'];
    serviceDate = json['ServiceDate'];
    isTag = json['IsTag'];
    initiatorId = json['InitiatorId'];
    initiatorName = json['InitiatorName'];
    profileImageUrl = json['ProfileImageUrl'];
    type = json['Type'];
    leadId = json['LeadId'];
    priority = json['Priority'];
    category = json['Category'];
    complainMode = json['ComplainMode'];
    noteCategory = json['NoteCategory'];
    serviceEndDate = json['ServiceEndDate'];
    adviserId = json['AdviserId'];
    adviserName = json['AdviserName'];
    directionType = json['DirectionType'];
    resolutionsUrl = json['ResolutionsUrl'];
    entityId = json['EntityId'];
    entityName = json['EntityName'];
    email = json['Email'];
    emailStatus = json['EmailStatus'];
    eventId = json['EventId'];
    assigneeId = json['AssigneeId'];
    ownerName = json['OwnerName'];
    ownerEmail = json['OwnerEmail'];
    phoneNumber = json['PhoneNumber'];
    dateofBirth = json['DateofBirth'];
    address = json['Address'];
    securityAddress = json['SecurityAddress'];
    userType = json['UserType'];
    assigneeName = json['AssigneeName'];
    stage = json['Stage'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['Title'] = this.title;
    data['Comments'] = this.comments;
    data['ServiceDate'] = this.serviceDate;
    data['IsTag'] = this.isTag;
    data['InitiatorId'] = this.initiatorId;
    data['InitiatorName'] = this.initiatorName;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['Type'] = this.type;
    data['LeadId'] = this.leadId;
    data['Priority'] = this.priority;
    data['Category'] = this.category;
    data['ComplainMode'] = this.complainMode;
    data['NoteCategory'] = this.noteCategory;
    data['ServiceEndDate'] = this.serviceEndDate;
    data['AdviserId'] = this.adviserId;
    data['AdviserName'] = this.adviserName;
    data['DirectionType'] = this.directionType;
    data['ResolutionsUrl'] = this.resolutionsUrl;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['Email'] = this.email;
    data['EmailStatus'] = this.emailStatus;
    data['EventId'] = this.eventId;
    data['AssigneeId'] = this.assigneeId;
    data['OwnerName'] = this.ownerName;
    data['OwnerEmail'] = this.ownerEmail;
    data['PhoneNumber'] = this.phoneNumber;
    data['DateofBirth'] = this.dateofBirth;
    data['Address'] = this.address;
    data['SecurityAddress'] = this.securityAddress;
    data['UserType'] = this.userType;
    data['AssigneeName'] = this.assigneeName;
    data['Stage'] = this.stage;
    data['Id'] = this.id;
    return data;
  }
}
