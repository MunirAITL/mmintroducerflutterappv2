class CaseOwnerAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  CaseOwnerAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  CaseOwnerAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<Users> users;
  ResponseData({this.users});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['Users'] != null) {
      users = <Users>[];
      json['Users'].forEach((v) {
        users.add(new Users.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.users != null) {
      data['Users'] = this.users.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Users {
  String firstName;
  String lastName;
  String name;
  String userName;
  String profileImageUrl;
  String coverImageUrl;
  String profileUrl;
  String dateCreatedUtc;
  String dateCreatedLocal;
  bool active;
  String lastLoginDateUtc;
  String lastLoginDateLocal;
  int followerCount;
  int followingCount;
  int friendCount;
  bool canFollow;
  int followStatus;
  int friendStatus;
  int unreadNotificationCount;
  int unreadMessageCount;
  bool isOnline;
  String seName;
  String stanfordWorkplaceURL;
  String headline;
  String briefBio;
  String referenceId;
  String referenceType;
  String remarks;
  String community;
  String locations;
  String cohort;
  String communityId;
  bool isFirstLogin;
  String mobileNumber;
  String dateofBirth;
  bool isMobileNumberVerified;
  bool isProfileImageVerified;
  bool isDateOfBirthVerified;
  bool isElectronicIdVerified;
  bool isEmailVerified;
  String lastLoginDate;
  String userProfileUrl;
  int userAccesType;
  String address;
  String email;
  String latitude;
  String longitude;
  int bankVerifiedStatus;
  int nationalIDVerifiedStatus;
  int billingAccountVerifiedStatus;
  int isCustomerPrivacy;
  int profileImageId;
  String workHistory;
  int userTaskCategoryId;
  String deviceName;
  String skillName;
  String userTaskCategoryName;
  String status;
  int groupId;
  String countryCode;
  String namePrefix;
  String middleName;
  String areYouASmoker;
  String addressLine1;
  String addressLine2;
  String addressLine3;
  String town;
  String county;
  String postcode;
  String telNumber;
  String nationalInsuranceNumber;
  String nationality;
  String countryofBirth;
  String countryofResidency;
  String passportNumber;
  String maritalStatus;
  int userCompanyId;
  String companyName;
  String occupantType;
  String livingDate;
  String visaExpiryDate;
  String passportExpiryDate;
  String visaName;
  String otherVisaName;
  String reportLogoUrl;
  String userCompanyInfo;
  String companyType;
  int companyTypeId;
  String isActiveCustomerPrivacy;
  String isActiveCustomerAgreement;
  String companyWebsite;
  String isCallCenterActive;
  int companyCount;
  bool isSelected;
  String buildingNumber;
  String region;
  String faxNumber;
  int id;

  Users(
      {this.firstName,
      this.lastName,
      this.name,
      this.userName,
      this.profileImageUrl,
      this.coverImageUrl,
      this.profileUrl,
      this.dateCreatedUtc,
      this.dateCreatedLocal,
      this.active,
      this.lastLoginDateUtc,
      this.lastLoginDateLocal,
      this.followerCount,
      this.followingCount,
      this.friendCount,
      this.canFollow,
      this.followStatus,
      this.friendStatus,
      this.unreadNotificationCount,
      this.unreadMessageCount,
      this.isOnline,
      this.seName,
      this.stanfordWorkplaceURL,
      this.headline,
      this.briefBio,
      this.referenceId,
      this.referenceType,
      this.remarks,
      this.community,
      this.locations,
      this.cohort,
      this.communityId,
      this.isFirstLogin,
      this.mobileNumber,
      this.dateofBirth,
      this.isMobileNumberVerified,
      this.isProfileImageVerified,
      this.isDateOfBirthVerified,
      this.isElectronicIdVerified,
      this.isEmailVerified,
      this.lastLoginDate,
      this.userProfileUrl,
      this.userAccesType,
      this.address,
      this.email,
      this.latitude,
      this.longitude,
      this.bankVerifiedStatus,
      this.nationalIDVerifiedStatus,
      this.billingAccountVerifiedStatus,
      this.isCustomerPrivacy,
      this.profileImageId,
      this.workHistory,
      this.userTaskCategoryId,
      this.deviceName,
      this.skillName,
      this.userTaskCategoryName,
      this.status,
      this.groupId,
      this.countryCode,
      this.namePrefix,
      this.middleName,
      this.areYouASmoker,
      this.addressLine1,
      this.addressLine2,
      this.addressLine3,
      this.town,
      this.county,
      this.postcode,
      this.telNumber,
      this.nationalInsuranceNumber,
      this.nationality,
      this.countryofBirth,
      this.countryofResidency,
      this.passportNumber,
      this.maritalStatus,
      this.userCompanyId,
      this.companyName,
      this.occupantType,
      this.livingDate,
      this.visaExpiryDate,
      this.passportExpiryDate,
      this.visaName,
      this.otherVisaName,
      this.reportLogoUrl,
      this.userCompanyInfo,
      this.companyType,
      this.companyTypeId,
      this.isActiveCustomerPrivacy,
      this.isActiveCustomerAgreement,
      this.companyWebsite,
      this.isCallCenterActive,
      this.companyCount,
      this.isSelected,
      this.buildingNumber,
      this.region,
      this.faxNumber,
      this.id});

  Users.fromJson(Map<String, dynamic> json) {
    firstName = json['FirstName'];
    lastName = json['LastName'];
    name = json['Name'];
    userName = json['UserName'];
    profileImageUrl = json['ProfileImageUrl'];
    coverImageUrl = json['CoverImageUrl'];
    profileUrl = json['ProfileUrl'];
    dateCreatedUtc = json['DateCreatedUtc'];
    dateCreatedLocal = json['DateCreatedLocal'];
    active = json['Active'];
    lastLoginDateUtc = json['LastLoginDateUtc'];
    lastLoginDateLocal = json['LastLoginDateLocal'];
    followerCount = json['FollowerCount'];
    followingCount = json['FollowingCount'];
    friendCount = json['FriendCount'];
    canFollow = json['CanFollow'];
    followStatus = json['FollowStatus'];
    friendStatus = json['FriendStatus'];
    unreadNotificationCount = json['UnreadNotificationCount'];
    unreadMessageCount = json['UnreadMessageCount'];
    isOnline = json['IsOnline'];
    seName = json['SeName'];
    stanfordWorkplaceURL = json['StanfordWorkplaceURL'];
    headline = json['Headline'];
    briefBio = json['BriefBio'];
    referenceId = json['ReferenceId'];
    referenceType = json['ReferenceType'];
    remarks = json['Remarks'];
    community = json['Community'];
    locations = json['Locations'];
    cohort = json['Cohort'];
    communityId = json['CommunityId'];
    isFirstLogin = json['IsFirstLogin'];
    mobileNumber = json['MobileNumber'];
    dateofBirth = json['DateofBirth'];
    isMobileNumberVerified = json['IsMobileNumberVerified'];
    isProfileImageVerified = json['IsProfileImageVerified'];
    isDateOfBirthVerified = json['IsDateOfBirthVerified'];
    isElectronicIdVerified = json['IsElectronicIdVerified'];
    isEmailVerified = json['IsEmailVerified'];
    lastLoginDate = json['LastLoginDate'];
    userProfileUrl = json['UserProfileUrl'];
    userAccesType = json['UserAccesType'];
    address = json['Address'];
    email = json['Email'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
    bankVerifiedStatus = json['BankVerifiedStatus'];
    nationalIDVerifiedStatus = json['NationalIDVerifiedStatus'];
    billingAccountVerifiedStatus = json['BillingAccountVerifiedStatus'];
    isCustomerPrivacy = json['IsCustomerPrivacy'];
    profileImageId = json['ProfileImageId'];
    workHistory = json['WorkHistory'];
    userTaskCategoryId = json['UserTaskCategoryId'];
    deviceName = json['DeviceName'];
    skillName = json['SkillName'];
    userTaskCategoryName = json['UserTaskCategoryName'];
    status = json['Status'];
    groupId = json['GroupId'];
    countryCode = json['CountryCode'];
    namePrefix = json['NamePrefix'];
    middleName = json['MiddleName'];
    areYouASmoker = json['AreYouASmoker'];
    addressLine1 = json['AddressLine1'];
    addressLine2 = json['AddressLine2'];
    addressLine3 = json['AddressLine3'];
    town = json['Town'];
    county = json['County'];
    postcode = json['Postcode'];
    telNumber = json['TelNumber'];
    nationalInsuranceNumber = json['NationalInsuranceNumber'];
    nationality = json['Nationality'];
    countryofBirth = json['CountryofBirth'];
    countryofResidency = json['CountryofResidency'];
    passportNumber = json['PassportNumber'];
    maritalStatus = json['MaritalStatus'];
    userCompanyId = json['UserCompanyId'];
    companyName = json['CompanyName'];
    occupantType = json['OccupantType'];
    livingDate = json['LivingDate'];
    visaExpiryDate = json['VisaExpiryDate'];
    passportExpiryDate = json['PassportExpiryDate'];
    visaName = json['VisaName'];
    otherVisaName = json['OtherVisaName'];
    reportLogoUrl = json['ReportLogoUrl'];
    userCompanyInfo = json['UserCompanyInfo'];
    companyType = json['CompanyType'];
    companyTypeId = json['CompanyTypeId'];
    isActiveCustomerPrivacy = json['IsActiveCustomerPrivacy'];
    isActiveCustomerAgreement = json['IsActiveCustomerAgreement'];
    companyWebsite = json['CompanyWebsite'];
    isCallCenterActive = json['IsCallCenterActive'];
    companyCount = json['CompanyCount'];
    isSelected = json['IsSelected'];
    buildingNumber = json['BuildingNumber'];
    region = json['Region'];
    faxNumber = json['FaxNumber'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FirstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['Name'] = this.name;
    data['UserName'] = this.userName;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['CoverImageUrl'] = this.coverImageUrl;
    data['ProfileUrl'] = this.profileUrl;
    data['DateCreatedUtc'] = this.dateCreatedUtc;
    data['DateCreatedLocal'] = this.dateCreatedLocal;
    data['Active'] = this.active;
    data['LastLoginDateUtc'] = this.lastLoginDateUtc;
    data['LastLoginDateLocal'] = this.lastLoginDateLocal;
    data['FollowerCount'] = this.followerCount;
    data['FollowingCount'] = this.followingCount;
    data['FriendCount'] = this.friendCount;
    data['CanFollow'] = this.canFollow;
    data['FollowStatus'] = this.followStatus;
    data['FriendStatus'] = this.friendStatus;
    data['UnreadNotificationCount'] = this.unreadNotificationCount;
    data['UnreadMessageCount'] = this.unreadMessageCount;
    data['IsOnline'] = this.isOnline;
    data['SeName'] = this.seName;
    data['StanfordWorkplaceURL'] = this.stanfordWorkplaceURL;
    data['Headline'] = this.headline;
    data['BriefBio'] = this.briefBio;
    data['ReferenceId'] = this.referenceId;
    data['ReferenceType'] = this.referenceType;
    data['Remarks'] = this.remarks;
    data['Community'] = this.community;
    data['Locations'] = this.locations;
    data['Cohort'] = this.cohort;
    data['CommunityId'] = this.communityId;
    data['IsFirstLogin'] = this.isFirstLogin;
    data['MobileNumber'] = this.mobileNumber;
    data['DateofBirth'] = this.dateofBirth;
    data['IsMobileNumberVerified'] = this.isMobileNumberVerified;
    data['IsProfileImageVerified'] = this.isProfileImageVerified;
    data['IsDateOfBirthVerified'] = this.isDateOfBirthVerified;
    data['IsElectronicIdVerified'] = this.isElectronicIdVerified;
    data['IsEmailVerified'] = this.isEmailVerified;
    data['LastLoginDate'] = this.lastLoginDate;
    data['UserProfileUrl'] = this.userProfileUrl;
    data['UserAccesType'] = this.userAccesType;
    data['Address'] = this.address;
    data['Email'] = this.email;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['BankVerifiedStatus'] = this.bankVerifiedStatus;
    data['NationalIDVerifiedStatus'] = this.nationalIDVerifiedStatus;
    data['BillingAccountVerifiedStatus'] = this.billingAccountVerifiedStatus;
    data['IsCustomerPrivacy'] = this.isCustomerPrivacy;
    data['ProfileImageId'] = this.profileImageId;
    data['WorkHistory'] = this.workHistory;
    data['UserTaskCategoryId'] = this.userTaskCategoryId;
    data['DeviceName'] = this.deviceName;
    data['SkillName'] = this.skillName;
    data['UserTaskCategoryName'] = this.userTaskCategoryName;
    data['Status'] = this.status;
    data['GroupId'] = this.groupId;
    data['CountryCode'] = this.countryCode;
    data['NamePrefix'] = this.namePrefix;
    data['MiddleName'] = this.middleName;
    data['AreYouASmoker'] = this.areYouASmoker;
    data['AddressLine1'] = this.addressLine1;
    data['AddressLine2'] = this.addressLine2;
    data['AddressLine3'] = this.addressLine3;
    data['Town'] = this.town;
    data['County'] = this.county;
    data['Postcode'] = this.postcode;
    data['TelNumber'] = this.telNumber;
    data['NationalInsuranceNumber'] = this.nationalInsuranceNumber;
    data['Nationality'] = this.nationality;
    data['CountryofBirth'] = this.countryofBirth;
    data['CountryofResidency'] = this.countryofResidency;
    data['PassportNumber'] = this.passportNumber;
    data['MaritalStatus'] = this.maritalStatus;
    data['UserCompanyId'] = this.userCompanyId;
    data['CompanyName'] = this.companyName;
    data['OccupantType'] = this.occupantType;
    data['LivingDate'] = this.livingDate;
    data['VisaExpiryDate'] = this.visaExpiryDate;
    data['PassportExpiryDate'] = this.passportExpiryDate;
    data['VisaName'] = this.visaName;
    data['OtherVisaName'] = this.otherVisaName;
    data['ReportLogoUrl'] = this.reportLogoUrl;
    data['UserCompanyInfo'] = this.userCompanyInfo;
    data['CompanyType'] = this.companyType;
    data['CompanyTypeId'] = this.companyTypeId;
    data['IsActiveCustomerPrivacy'] = this.isActiveCustomerPrivacy;
    data['IsActiveCustomerAgreement'] = this.isActiveCustomerAgreement;
    data['CompanyWebsite'] = this.companyWebsite;
    data['IsCallCenterActive'] = this.isCallCenterActive;
    data['CompanyCount'] = this.companyCount;
    data['IsSelected'] = this.isSelected;
    data['BuildingNumber'] = this.buildingNumber;
    data['Region'] = this.region;
    data['FaxNumber'] = this.faxNumber;
    data['Id'] = this.id;
    return data;
  }
}
