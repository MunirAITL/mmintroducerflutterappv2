class GetUserTaskCatAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetUserTaskCatAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetUserTaskCatAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<UserTaskCategorys> userTaskCategorys;
  ResponseData({this.userTaskCategorys});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['UserTaskCategorys'] != null) {
      userTaskCategorys = <UserTaskCategorys>[];
      json['UserTaskCategorys'].forEach((v) {
        userTaskCategorys.add(new UserTaskCategorys.fromJson(v));
      });
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userTaskCategorys != null) {
      data['UserTaskCategorys'] =
          this.userTaskCategorys.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UserTaskCategorys {
  int userId;
  String user;
  String name;
  int parentId;
  String remarks;
  String initiatorBy;
  String parentName;
  String parentNameBn;
  String nameBn;
  int order;
  bool isWeb;
  bool isApp;
  String imageUrl;
  String userTaskCategoryUrl;
  String userTaskItemResponseModelList;
  int id;

  UserTaskCategorys(
      {this.userId,
      this.user,
      this.name,
      this.parentId,
      this.remarks,
      this.initiatorBy,
      this.parentName,
      this.parentNameBn,
      this.nameBn,
      this.order,
      this.isWeb,
      this.isApp,
      this.imageUrl,
      this.userTaskCategoryUrl,
      this.userTaskItemResponseModelList,
      this.id});

  UserTaskCategorys.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    name = json['Name'];
    parentId = json['ParentId'];
    remarks = json['Remarks'];
    initiatorBy = json['InitiatorBy'];
    parentName = json['ParentName'];
    parentNameBn = json['ParentName_Bn'];
    nameBn = json['Name_Bn'];
    order = json['Order'];
    isWeb = json['IsWeb'];
    isApp = json['IsApp'];
    imageUrl = json['ImageUrl'];
    userTaskCategoryUrl = json['UserTaskCategoryUrl'];
    userTaskItemResponseModelList = json['UserTaskItemResponseModelList'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['Name'] = this.name;
    data['ParentId'] = this.parentId;
    data['Remarks'] = this.remarks;
    data['InitiatorBy'] = this.initiatorBy;
    data['ParentName'] = this.parentName;
    data['ParentName_Bn'] = this.parentNameBn;
    data['Name_Bn'] = this.nameBn;
    data['Order'] = this.order;
    data['IsWeb'] = this.isWeb;
    data['IsApp'] = this.isApp;
    data['ImageUrl'] = this.imageUrl;
    data['UserTaskCategoryUrl'] = this.userTaskCategoryUrl;
    data['UserTaskItemResponseModelList'] = this.userTaskItemResponseModelList;
    data['Id'] = this.id;
    return data;
  }
}
