import 'package:get/get.dart';

class MortgageCalController extends GetxController {
  var mortgageTerm = 25.obs;
  var initialPeriod = 2.obs;
  var interestRateType = "Fixed".obs;
  var depositAmount = 100000.0.obs;
  var anualIncomeAmount = 40000.0.obs;
  var caseType = 'New mortgage'.obs;
  //
  var mortgageRate = 4.5.obs;
  var maxLoanAmount = 180000.0.obs;
  var homeAmount = 200000.0.obs;
}
