import 'package:get/get.dart';

class ManageLeadSearchController extends GetxController {
  var stageRB = 5.obs; //  none
  var statusRB = 0.obs; //  all
  var sortByRB = 0.obs; // task due date
  var searchByText = "".obs;
}
