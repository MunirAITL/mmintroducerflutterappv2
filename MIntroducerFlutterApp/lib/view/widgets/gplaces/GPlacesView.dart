// to get places detail (lat/lng)
import 'package:google_api_headers/google_api_headers.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:aitl/mixin.dart';

import '../../../config/GoogleAPIKey.dart';
import '../../../config/MyTheme.dart';

//  https://github.com/fluttercommunity/flutter_google_places/issues/140
//  https://stackoverflow.com/questions/55879550/how-to-fix-httpexception-connection-closed-before-full-header-was-received

class GPlacesView extends StatefulWidget {
  final String address;
  final String title;
  Color bgColor;
  final Function(String address, Location loc) callback;
  GPlacesView({
    Key key,
    @required this.title,
    this.address = "",
    this.bgColor = Colors.transparent,
    @required this.callback,
  }) : super(key: key);
  @override
  _GPlacesViewState createState() => _GPlacesViewState();
}

class _GPlacesViewState extends State<GPlacesView> with Mixin {
  @override
  Widget build(BuildContext context) {
    final address = (widget.address == "") ? 'Search' : widget.address;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        (widget.title != null)
            ? Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: Txt(
                    txt: widget.title,
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              )
            : SizedBox(),
        new GestureDetector(
          onTap: () => _handlePressButton(),
          child: new Container(
            width: getW(context),
            decoration: BoxDecoration(
              color: widget.bgColor,
              borderRadius: BorderRadius.circular(5),
              border: Border.all(
                color: Colors.grey, //
                width: 1,
              ),
            ),
            child: IntrinsicHeight(
              child: Padding(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(width: 5),
                    Icon(
                      Icons.location_on_outlined,
                      color: MyTheme.purpleColor,
                      size: 20,
                    ),
                    VerticalDivider(
                      color: MyTheme.purpleColor,
                      thickness: 1,
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Txt(
                          txt: address,
                          txtColor: (address == 'Search')
                              ? Colors.grey
                              : MyTheme.inputColor,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                    (address == 'Search')
                        ? SizedBox(height: 30)
                        : GestureDetector(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Icon(
                                Icons.close,
                                color: Colors.black38,
                              ),
                            ),
                            onTap: () {
                              widget.callback('', null);
                            },
                          ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void onError(PlacesAutocompleteResponse response) {
    print(response.errorMessage.toString());
    //homeScaffoldKey.currentState.showSnackBar(
    //SnackBar(content: Text(response.errorMessage)),
    //);
  }

  Future<void> _handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: GoogleServerAPIKey.ApiKey,
      onError: onError,
      mode: Mode.overlay,
      language: "en",
      types: [""],
      strictbounds: false,
      components: [Component(Component.country, "uk")],
    );
    if (p != null) {
      //displayPrediction(p, (String address, Location loc) {
      //widget.callback(address, loc);
      //});

      try {
        GoogleMapsPlaces _places = GoogleMapsPlaces(
          apiKey: GoogleServerAPIKey.ApiKey,
          apiHeaders: await GoogleApiHeaders().getHeaders(),
        );
        if (_places != null) {
          PlacesDetailsResponse detail =
              await _places.getDetailsByPlaceId(p.placeId);

          String address = "";
          for (final addr in detail.result.addressComponents) {
            if (addr.types[0] != 'postal_code') {
              address += (addr.longName + ' ');
            }
          }

          //detail.result.formattedAddress; //p.description;
          widget.callback(address.trim(), detail.result.geometry.location);
        }
      } catch (e) {
        myLog(e.toString());
      }
    }
  }
}
