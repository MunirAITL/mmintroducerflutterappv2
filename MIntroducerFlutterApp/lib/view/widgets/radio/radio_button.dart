import 'package:flutter/material.dart';

import '../../../Mixin.dart';
import '../txt/Txt.dart';

radioButtonitem(
    {BuildContext context,
    String text,
    double txtSize = 2,
    String bgColor,
    Color textColor}) {
  return Container(
    width: MediaQuery.of(context).size.width / 2,
    padding: EdgeInsets.only(left: 10),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border(
            left: BorderSide(color: Colors.grey, width: 1),
            right: BorderSide(color: Colors.grey, width: 1),
            top: BorderSide(color: Colors.grey, width: 1),
            bottom: BorderSide(color: Colors.grey, width: 1)),
        color: HexColor.fromHex(bgColor)),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: 20,
          height: 20,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              border: Border(
                  left: BorderSide(color: Colors.grey, width: 1),
                  right: BorderSide(color: Colors.grey, width: 1),
                  top: BorderSide(color: Colors.grey, width: 1),
                  bottom: BorderSide(color: Colors.grey, width: 1)),
              color: Colors.white),
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border(
                      left: BorderSide(color: Colors.grey, width: 1),
                      right: BorderSide(color: Colors.grey, width: 1),
                      top: BorderSide(color: Colors.grey, width: 1),
                      bottom: BorderSide(color: Colors.grey, width: 1)),
                  color: Colors.grey),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Txt(
                txt: text,
                txtColor: textColor,
                txtSize: txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
        ),
      ],
    ),
  );
}
