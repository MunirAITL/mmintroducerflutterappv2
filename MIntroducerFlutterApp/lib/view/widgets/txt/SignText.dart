import 'package:flutter/material.dart';
import '../../../config/AppDefine.dart';
import '../../../config/MyTheme.dart';
import '../txt/Txt.dart';

drawSignText({String title, String txt, String sign = AppDefine.CUR_SIGN}) {
  return Container(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        title != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Txt(
                    txt: title,
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              )
            : SizedBox(),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5), bottomLeft: Radius.circular(5)),
          ),
          child: Row(
            children: [
              Container(
                color: Colors.grey.shade400,
                child: Padding(
                  padding: const EdgeInsets.all(12),
                  child: Txt(
                      txt: sign,
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize + .3,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Txt(
                    txt: txt,
                    txtColor: txt != '0' ? Colors.black : Colors.grey,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
