import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class MMBtn extends StatelessWidget with Mixin {
  final String txt;
  Color bgColor;
  Color txtColor;
  final double width;
  final double height;
  final double radius;
  final Function callback;

  MMBtn({
    Key key,
    @required this.txt,
    @required this.width,
    @required this.height,
    this.radius = 10,
    @required this.callback,
    this.bgColor,
    this.txtColor,
  }) {
    if (this.bgColor == null) bgColor = MyTheme.statusBarColor;
    if (this.txtColor == null) txtColor = Colors.white;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Container(
        width: width,
        height: (height != null) ? height : getHP(context, 6),
        alignment: Alignment.center,

        //decoration: MyTheme.statusBarColor,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(radius),
        ),
        child: Txt(
          txt: txt,
          txtColor: txtColor,
          txtSize: MyTheme.txtSize - .2,
          txtAlign: TextAlign.center,
          isBold: false,
        ),
      ),
    );
  }
}
