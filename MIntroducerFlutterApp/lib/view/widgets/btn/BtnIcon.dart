import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';

class BtnIcon extends StatelessWidget {
  final width, height;
  final txt;
  final IconData icon;
  final Widget image;
  final bool isRightIco;
  final txtColor;
  final bgColor;
  final isCurve;
  final Function callback;

  const BtnIcon({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.bgColor,
    @required this.width,
    @required this.height,
    @required this.icon,
    @required this.image,
    @required this.isRightIco,
    @required this.isCurve,
    @required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: MaterialButton(
        child: (isRightIco) ? drawRIco(context) : drawLIco(context),
        onPressed: () {
          callback();
        },
        textColor: txtColor,
        //color: bgColor,
        shape: OutlineInputBorder(
            borderSide: BorderSide(
                style: BorderStyle.solid,
                width: 1.0,
                color: Colors.transparent),
            borderRadius: BorderRadius.circular((isCurve) ? 8 : 0)),
      ),
    );
  }

  drawLIco(context) => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          (icon != null)
              ? Icon(
                  icon,
                  size: 30,
                )
              : image,
          Flexible(
            child: Container(
              //color: Colors.black,
              width: width * 90 / 100,
              child: Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                child: Txt(
                  txt: txt,
                  txtColor: txtColor,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
              ),
            ),
          ),
        ],
      );

  drawRIco(context) => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
            child: Container(
              //color: Colors.black,
              width: width * 90 / 100,
              child: Txt(
                txt: txt,
                txtColor: txtColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
            ),
          ),
          //Spacer(),
          (icon != null)
              ? Icon(
                  icon,
                  size: 30,
                )
              : image,
        ],
      );
}
