import 'dart:ui';
import 'package:aitl/controller/helper/db_cus/tab_more/HelpTutHelper.dart';
import 'package:aitl/controller/observer/StateListnerIntro.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';
import '../txt/Txt.dart';

class HelpTutIntroDialog extends StatefulWidget {
  @override
  State createState() => _HelpTutIntroDialogState();
}

class _HelpTutIntroDialogState extends State<HelpTutIntroDialog> with Mixin {
  int index = 0;
  StateProviderIntro _stateProvider = StateProviderIntro();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    HelpTutHelperIntro().listTips = null;
    _stateProvider = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        // crossAxisAlignment: CrossAxisAlignment.end,
        //mainAxisAlignment:
        //index == 0 ? MainAxisAlignment.end : MainAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Dialog(
            /*shape: RoundedRectangleBorder(
                side: BorderSide(color: MyTheme.dBlueAirColor),
                borderRadius: BorderRadius.all(Radius.circular(5))),*/
            backgroundColor: Colors.transparent,
            elevation: 0,
            insetPadding: EdgeInsets.only(
                left: 50,
                right: 50,
                bottom: getHP(context,
                    17)), //index == 0 ? getHP(context, 35) : getHP(context, 20)),
            child: dialogContent(context),
          )
        ]);
  }

  Widget dialogContent(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 0.0, right: 0.0),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Material(
            elevation: 0,
            color: Colors.transparent,
            child: Container(
              padding: EdgeInsets.only(
                top: 18.0,
              ),
              margin: EdgeInsets.only(top: 30, right: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    width: 1,
                    color: MyTheme.brandColor,
                    style: BorderStyle.solid,
                  ),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ]),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 20, left: 20, right: 20, bottom: 10),
                    child: Txt(
                      txt: HelpTutHelperIntro().listTips[index],
                      txtColor: MyTheme.dBlueAirColor,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      fontWeight: FontWeight.w500,
                      isBold: true,
                      //txtLineSpace: 1.5,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 10, left: 20, right: 20, bottom: 20),
                    child: Container(
                      //color: Colors.transparent,
                      //height: getHP(context, 6),
                      width: double.infinity,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              if (index != 0) {
                                setState(() {
                                  index--;
                                  _switchTabsHand(index);
                                });
                              }
                            },
                            child: Container(
                              width: 60,
                              height: 40,
                              margin: const EdgeInsets.all(2),
                              padding: const EdgeInsets.all(2),
                              decoration: BoxDecoration(
                                  color: (index == 0)
                                      ? Colors.grey
                                      : MyTheme.brandColor,
                                  border: Border.all(
                                      color: Colors.white, width: .5)),
                              //iconSize: 75,
                              child: Icon(
                                Icons.arrow_left,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          /*Expanded(
                            child: Txt(
                              // txt: HelpTutHelperIntro().listTab[index].toString(),
                              txt: index < 4 ? "Next" : "Previous",
                              txtColor: MyTheme.dBlueAirColor,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.center,
                              isBold: false,
                            ),
                          ),*/
                          //Spacer(),
                          SizedBox(width: 50),
                          GestureDetector(
                            onTap: () {
                              if (index !=
                                  HelpTutHelperIntro().listTips.length - 1) {
                                setState(() {
                                  index++;
                                  _switchTabsHand(index);
                                });
                              }
                            },
                            child: Container(
                              width: 60,
                              height: 40,
                              margin: const EdgeInsets.all(2),
                              padding: const EdgeInsets.all(2),
                              decoration: BoxDecoration(
                                  color: (index ==
                                          HelpTutHelperIntro().listTips.length -
                                              1)
                                      ? Colors.grey
                                      : MyTheme.brandColor,
                                  border: Border.all(
                                      color: Colors.white, width: .5)),
                              child: Icon(
                                Icons.arrow_right,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 5,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                      border: Border.all(
                        width: .5,
                        color: MyTheme.brandColor,
                        style: BorderStyle.solid,
                      ),
                    ),
                    child:
                        Icon(Icons.help, color: MyTheme.brandColor, size: 30)),
              ),
            ),
          ),
          Positioned(
            top: 15,
            right: 0,
            child: Align(
              alignment: Alignment.topRight,
              child: GestureDetector(
                  child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        2), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                      border: Border.all(
                        width: 1,
                        color: MyTheme.brandColor,
                        style: BorderStyle.solid,
                      ),
                    ),
                    child: Icon(
                      Icons.close,
                      color: MyTheme.brandColor,
                    ),
                  ),
                  onTap: () {
                    Get.back();
                  }),
            ),
          ),
        ],
      ),
    );
  }

  _switchTabsHand(int index2) {
    switch (index2) {
      case 0:
        _stateProvider.notify(ObserverStateIntro.STATE_CHANGED_tabbar1);
        break;
      case 1:
        _stateProvider.notify(ObserverStateIntro.STATE_CHANGED_tabbar2);
        break;
      case 2:
        _stateProvider.notify(ObserverStateIntro.STATE_CHANGED_tabbar3);
        break;
      case 3:
        _stateProvider.notify(ObserverStateIntro.STATE_CHANGED_tabbar4);
        break;
      case 4:
        _stateProvider.notify(ObserverStateIntro.STATE_CHANGED_tabbar5);
        break;
      default:
    }
  }
}
