import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AlertDialogCreditCardDetails extends StatelessWidget {
  const AlertDialogCreditCardDetails({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(Icons.close, color: Colors.black)),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  children: [
                    userInfoItem(
                        title: "Name", value: "Loan (unspecified type)"),
                    userInfoItem(title: "Address", value: "Address"),
                    userInfoItem(title: "Date of birth", value: "01/01/1970"),
                    userInfoItem(title: "Account type", value: "LN"),
                    userInfoItem(
                        title: "Account number", value: "**********515"),
                    userInfoItem(
                        title: "Account start date", value: "21/08/2021"),
                    userInfoItem(
                        title: "Account end date", value: "21/08/2021"),
                    userInfoItem(
                        title: "Payment start date", value: "21/08/2021"),
                    userInfoItem(
                        title: "Repayment frequency", value: "Monthly"),
                    SizedBox(
                      height: 50,
                    ),
                  ],
                ),
              ),
              /*Container(
                width: MediaQuery.of(context).size.width,
                child: Btn(
                    txt: "Ok",
                    txtColor: Colors.white,
                    bgColor: MyTheme.brandColor,
                    width: 100,
                    height: 50,
                    callback: () {
                      Get.back();
                    }),
              )*/
            ],
          ),
        ),
      ),
    );
  }

  userInfoItem({String title, String value}) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Row(
        children: [
          Txt(
              txt: "$title: ",
              txtColor: Colors.black87,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: true),
          Expanded(
              child: Txt(
                  txt: "$value",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .3,
                  txtAlign: TextAlign.start,
                  isBold: false))
        ],
      ),
    );
  }
}
