import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AlertDialogWhereDoYouStand extends StatelessWidget with Mixin {
  const AlertDialogWhereDoYouStand({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getWP(context, 70),
      height: getHP(context, 40),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                //color: Colors.black,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Txt(
                              txt: "Your Standing",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 20),
                          Txt(
                              txt:
                                  "Here’s what your credit score says about your credit history.",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 10),
                          Txt(
                              txt:
                                  "Below is a brief explanation on where you may stand when it comes to obtaining credit.",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 20),
                          Txt(
                              txt: "Excellent",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 20),
                          Txt(
                              txt:
                                  "Your credit applications are unlikely to be rejected and you’re most likely to be accepted for "
                                  "the best deals on the market. Lenders are likely to see you as a low risk.",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 20),
                          Txt(
                              txt: "Good",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 20),
                          Txt(
                              txt:
                                  "You’re more likely to be accepted for credit and have more freedom to choose between "
                                  "different credit providers. This is a healthy credit score and signals that lenders may view you as low risk.",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 20),
                          Txt(
                              txt: "Fair",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 20),
                          Txt(
                              txt:
                                  "Lenders might occasionally reject you and you may find yourself excluded from the best deals in the market alongside being offered higher interest rates.",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 20),
                          Txt(
                              txt: "Poor",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 20),
                          Txt(
                              txt:
                                  "You may sometimes be rejected for credit. Similar to people with a rating of Very Poor, you may find you are subjected to higher interest rates than most people.",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt: "Very Poor",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt:
                                  "It’s likely that you will find it difficult to obtain credit. Even if you do, the interest rates may be higher than most.",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          /*Container(
            width: MediaQuery.of(context).size.width,
            child: Btn(
                txt: "Ok",
                txtColor: Colors.black,
                bgColor: MyTheme.brandColor,
                width: 100,
                height: 50,
                callback: () {
                  Get.back();
                }),
          )*/
        ],
      ),
    );
  }
}
