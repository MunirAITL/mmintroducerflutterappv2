import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class ShowCreditOverViewItem extends StatelessWidget {
  String firstTitle;
  String firstPrice;

  ShowCreditOverViewItem({this.firstTitle, this.firstPrice});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 150,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Card(
                elevation: 10,
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Txt(
                        txt: "$firstTitle",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: true,
                        maxLines: 5,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Txt(
                          txt: (firstTitle == "Short Term Debt" ||
                                  firstTitle == "Long Term Debt")
                              ? (AppDefine.CUR_SIGN + "$firstPrice")
                              : "$firstPrice",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: true),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
