import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class StoreItem extends StatelessWidget {
  String txtData;

  StoreItem({this.txtData});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, top: 10, right: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Icon(
              Icons.circle,
              color: Colors.black,
              size: 8,
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Txt(
                txt: "$txtData",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
        ],
      ),
    );
  }
}
