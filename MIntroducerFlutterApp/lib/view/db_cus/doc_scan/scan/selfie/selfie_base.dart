import 'package:flutter/material.dart';

import '../../../../../Mixin.dart';

abstract class SelfieBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout();
}
