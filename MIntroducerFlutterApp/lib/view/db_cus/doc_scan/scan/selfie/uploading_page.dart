import 'dart:convert';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/db_cus/DocScanCfg.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/ScanDocData.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostSelfieByBase64DataAPIModel.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/base_card.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/scan_id_doc_page.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/selfie/selfie_open_cam_page.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/selfie/uploading_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl_pkg/classes/ImageLib.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_string/json_string.dart';
import 'package:mime_type/mime_type.dart';

class UploadingPage extends StatefulWidget {
  const UploadingPage({Key key}) : super(key: key);

  @override
  State createState() => _UploadingPageState();
}

class _UploadingPageState extends UploadingBase<UploadingPage> {
  bool isUploadingDone = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      // executes after build
      initPage();
    });
  }

  //@mustCallSuper
  @override
  void dispose() {
    //progController.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  initPage() async {
    try {
      String mimeType = mime(scanDocData.file_selfie.path);
      var fileName = (scanDocData.file_selfie.path.split('/').last);
      //String mimee = mimeType.split('/')[0];
      String type = mimeType.split('/')[1];
      List<int> fileInByte = scanDocData.file_selfie.readAsBytesSync();
      final imageData = base64Encode(fileInByte);
      print(imageData);
      final param = {
        "UserCompanyId": userData.userModel.userCompanyID,
        "ImageType": type,
        "FileName": fileName,
        "UserId": userData.userModel.id,
        "ContentData": imageData,
      };
      final jsonString = JsonString(json.encode(param));
      myLog(jsonString.source);
      await APIViewModel().req<PostSelfieByBase64DataAPIModel>(
          context: context,
          url: DocScanCfg.SELFIE_POST_URL,
          reqType: ReqType.Post,
          param: param,
          callback: (model) async {
            if (mounted) {
              if (model != null) {
                if (model.success) {
                  try {
                    scanDocData.requestId =
                        model.responseData.responsedata.requestid;
                    Get.off(() => ScanIDDocPage()).then((value) => Get.back());
                  } catch (e) {
                    tryAgainAlert();
                  }
                } else {
                  tryAgainAlert();
                }
              } else {
                tryAgainAlert();
              }
            }
          });
    } catch (e) {}
  }

  tryAgainAlert() {
    progController.progress.value = 0;
    isUploadingDone = false;
    setState(() {});
    confirmDialog(
      context: context,
      title: "Uploading Alert",
      msg: "Something went wrong. Try again?",
      callbackYes: () {
        initPage();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Take your selfie"),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Center(
      child: Container(
        width: getW(context),
        //height: getH(context),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 120),
              Icon(
                Icons.cloud_upload,
                color: MyTheme.brandColor,
                size: 60,
              ),
              SizedBox(height: 5),
              Txt(
                  txt: "Uploading the documents",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 10),
              Obx(() => Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: LinearProgressIndicator(
                      backgroundColor: Colors.grey,
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                      value: progController.progress.value,
                    ),
                  )),
              (!isUploadingDone)
                  ? Padding(
                      padding:
                          const EdgeInsets.only(top: 20, left: 20, right: 20),
                      child: MMBtn(
                          txt: "Try again",
                          width: getWP(context, 40),
                          height: getHP(context, 5),
                          radius: 5,
                          callback: () async {
                            Get.off(() => OpenCamSelfiePage());
                          }))
                  : SizedBox()
            ],
          ),
        ),
      ),
    );
  }
}
