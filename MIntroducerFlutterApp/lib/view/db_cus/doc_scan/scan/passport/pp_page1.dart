import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/ScanDocData.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/base_card.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/passport/pp_open_cam_page.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/passport/pp_review_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl_pkg/classes/ImageLib.dart';
import 'package:aitl_pkg/widgets/crop_image.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'pp_base1.dart';

class PPPage1 extends StatefulWidget {
  const PPPage1({Key key}) : super(key: key);
  @override
  State createState() => _PPPage1State();
}

class _PPPage1State extends PP1Base<PPPage1> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Scan identity document"),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: Colors.white,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: getW(context),
              height: getHP(context, 50),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                      'assets/images/doc_scan/scan_driving_lic_bg.png'),
                  fit: BoxFit.fill,
                ),
                shape: BoxShape.rectangle,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt: "Capture the front side of the passport",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Txt(
                  txt:
                      "1- To prove your identity, you need to scan your passport. Make sure that passport main page should be clearly visible.",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Txt(
                  txt:
                      "2- Display the passport side/page with your photo in front of camera so that it fits into the frame.",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            Padding(
                padding: const EdgeInsets.only(
                    top: 20, left: 20, right: 20, bottom: 20),
                child: MMBtn(
                    txt: "Continue",
                    width: getW(context),
                    height: getHP(context, 7),
                    radius: 5,
                    callback: () async {
                      if (Server.isOtp) {
                        Get.off(() => OpenCamPPPage());
                      } else {
                        scanDocData.file_pp_front =
                            await ImageLib.getImageFromAssets(
                                "eid_scan/" + BaseCard.PP);
                        await Get.off(() => CropImagePage(
                              file: scanDocData.file_pp_front,
                              title: "Crop your passport",
                              txtColor: Colors.white,
                              appbarColor: MyTheme.statusBarColor,
                            )).then((file) {
                          if (file != null) {
                            scanDocData.file_pp_front = file;
                            Get.to(() => PPReviewPage());
                          }
                        });
                      }
                    })),
          ],
        ),
      ),
    );
  }
}
