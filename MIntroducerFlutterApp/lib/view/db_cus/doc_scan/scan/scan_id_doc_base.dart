import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl_pkg/classes/ImageLib.dart';
import 'package:aitl/model/data/ScanDocData.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/driving_lic/drvlic_page1.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/driving_lic/drvlic_review_page.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/passport/pp_page1.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/passport/pp_review_page.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../Mixin.dart';

abstract class ScanIDDocBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout();

  drawRow() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt: "What identity document do you want to scan?",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
            SizedBox(height: 10),
            drawCell(
                0,
                Icon(
                  Icons.drive_eta_outlined,
                  color: Colors.green,
                  size: 20,
                ),
                "Driving licence",
                true),
            drawCell(
                1,
                Icon(
                  Icons.account_box,
                  color: Colors.green,
                  size: 20,
                ),
                "Passport",
                false),
          ],
        ),
      ),
    );
  }

  drawCell(i, ico, title, isLine) {
    return Container(
      color: MyTheme.greyColor,
      child: ListTile(
        onTap: () async {
          if (i == 0) {
            //  Driving Licence
            Get.to(() => DrvLicPage1());
          } else if (i == 1) {
            //  Passport
            Get.to(() => PPPage1());
          }
        },
        minLeadingWidth: 0,
        leading: ico,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            (isLine)
                ? Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: UIHelper().drawLine())
                : SizedBox(
                    height: 20,
                  )
          ],
        ),
      ),
    );
  }
}
