import 'package:aitl/view/db_cus/doc_scan/utils/fun.dart';
import 'package:aitl/view/db_cus/doc_scan/utils/parser.dart';
import 'package:aitl/view/db_cus/doc_scan/utils/scan_mixin.dart';
import 'package:flutter/material.dart';

abstract class BaseCard<T extends StatefulWidget> extends State<T>
    with ScanMixin, Fun, ParserMixin {
  static const SELFIE = "kid.jpg";
  static const DL = "dada_driving.png";
  static const PP = "ppkid.jpg";
}
