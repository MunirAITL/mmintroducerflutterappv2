import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/data/ScanDocData.dart';
import 'package:aitl/view/db_cus/doc_scan/utils/scan_mixin.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

import '../../../../../Mixin.dart';

abstract class DrvLicReviewBase<T extends StatefulWidget> extends State<T>
    with
        Mixin,
        WidgetsBindingObserver,
        SingleTickerProviderStateMixin,
        ScanMixin {
  bool isFrontAnim = false;

  AnimationController animationController;

  drawLayout();

  drawPicBox() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        width: getW(context),
        height: getHP(context, 30),
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(color: Colors.white, width: 2.5)),
        child: Image.file(
          scanDocData.file_drvlic_front,
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  drawIssues(List<String> list) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
      child: ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: list.length,
          itemBuilder: (context, index) {
            return (index == 0)
                ? Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: Txt(
                              txt: list[index],
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                        ),
                        Flexible(
                          child: Container(
                            width: 50,
                            height: 50,
                            child: Image.asset(
                                "assets/images/doc_scan/eid_prove_id_header.png"),
                          ),
                        )
                      ],
                    ),
                  )
                : Padding(
                    padding: EdgeInsets.only(bottom: 15),
                    child: Txt(
                        txt: list[index],
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false));
          }),
    );
  }

  void animateScanAnimation(bool reverse) {
    if (reverse) {
      animationController.reverse(from: 1.0);
    } else {
      animationController.forward(from: 0.0);
    }
  }
}
