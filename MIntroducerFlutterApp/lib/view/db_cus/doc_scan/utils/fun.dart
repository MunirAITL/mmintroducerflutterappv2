import 'package:intl/intl.dart';

class Fun {
  static trim(String str) {
    //for (var skip in listSkip) {
    if (str[1] == ".")
      str = str.replaceRange(0, 2, "").trim();
    else
      str = str.replaceRange(0, 1, "").trim();
    //str = str.replaceFirst(skip, "").trim();
    //}
    return str;
  }

  static hasStringOnly(String str) {
    return RegExp('[a-zA-Z]').hasMatch(str);
  }

  static removeSpecialChar(String str) {
    return str.replaceAll(new RegExp(r'[^\w\s]+'), '');
  }

  static changeDate2(String str) {
    try {
      return str[0] + str[1] + '.' + str[3] + str[4] + '.' + str[6] + str[7];
    } catch (e) {
      return str;
    }
  }

  static getDateYY2YYYY(String dt) {
    try {
      final dateFormat = DateFormat("dd-MMM-yy");
      // Workaround for issue 123 and 275 in the intl package for parsing years with two digit.
      // The logic could be more advanced but will for know just add 2000 to the year.
      dateFormat.dateTimeConstructor = (int year, int month, int day,
          int hour24, int minute, int second, int fractionalSecond, bool utc) {
        if (utc) {
          return DateTime.utc(year + 2000, month, day, hour24, minute, second,
              fractionalSecond);
        } else {
          return DateTime(year + 2000, month, day, hour24, minute, second,
              fractionalSecond);
        }
      };
      final dtArr = dt.split("-");
      final mm = capitalize(dtArr[1].toLowerCase());
      final d = dateFormat.parse(dtArr[0] + '-' + mm + '-' + dtArr[2]);
      final DateFormat formatter = DateFormat('dd-MMM-yyyy');
      final String formatted = formatter.format(d);
      return formatted;
    } catch (e) {}
    return dt;
  }

  static String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
}
