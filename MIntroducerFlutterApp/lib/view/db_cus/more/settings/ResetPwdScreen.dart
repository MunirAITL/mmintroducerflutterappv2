import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class ResetPwdScreen extends StatefulWidget {
  const ResetPwdScreen({Key key}) : super(key: key);

  @override
  State createState() => _ResetPwdScreenState();
}

class _ResetPwdScreenState extends State<ResetPwdScreen> with Mixin {
  final TextEditingController email = TextEditingController();
  final focusEmail = FocusNode();

  bool isSentCode = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      decoration: MyTheme.bgBlueColor,
      width: getW(context),
      height: getH(context),
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30),
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 30),
                Row(
                  children: [
                    Flexible(
                      child: Txt(
                        txt: "Reset your password",
                        txtColor: MyTheme.lightTxtColor,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.left,
                        fontWeight: FontWeight.w300,
                        txtLineSpace: 1.4,
                        isBold: false,
                      ),
                    ),
                    //Image.asset("assets/images/ico/")
                  ],
                ),
                SizedBox(height: 40),
                drawInputBox(
                    context: context,
                    title: "Repeat Password",
                    input: email,
                    kbType: TextInputType.text,
                    inputAction: TextInputAction.done,
                    focusNode: focusEmail,
                    len: 20,
                    isPwd: true,
                    isobscureText: false,
                    callback: (v) {
                      setState(() {});
                    }),
                SizedBox(height: 30),
                MMBtn(
                    txt: "Reset Password",
                    height: getHP(context, 7),
                    width: getW(context),
                    callback: () {}),
                SizedBox(height: 50),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
