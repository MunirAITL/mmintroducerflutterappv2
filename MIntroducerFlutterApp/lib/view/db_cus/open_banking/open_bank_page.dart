import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/open_banking/GetForeCasts4PersonalAccountsAPIModel.dart';
import 'package:aitl/model/json/db_cus/open_banking/GetInsight4PersonnelAccountsAPIModel.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'open_bank_base.dart';
import 'package:get/get.dart';

class OpenBankPage extends StatefulWidget {
  const OpenBankPage({Key key}) : super(key: key);

  @override
  State createState() => _OpenBankPageState();
}

class _OpenBankPageState extends OpenBankBase<OpenBankPage> {
  wsOpenBankAPIcall() async {
    await APIViewModel().req<GetInsight4PersonnelAccountsAPIModel>(
      context: context,
      url: Server.GET_INSIGHT4PERSONNELACCOUNTS_URL.replaceAll(
          "#customerId#", "183382"), //userData.userModel.id.toString()),
      reqType: ReqType.Get,
      callback: (model) async {
        if (mounted && model != null) {
          if (model.success) {
            getInsight4AccResponseDataModel = model.responseData.responseData;
            setIncomeList();
            //setExpList();
            setChartData();
            setChart2Data();
            setState(() {});
          } else {
            showToast(context: context, msg: "Something went wrong");
          }
        }
      },
    );
    await APIViewModel().req<GetForeCasts4PersonalAccountsAPIModel>(
      context: context,
      url: Server.GET_FORECASTS4PERSONNELACCOUNT_URL.replaceAll(
          "#customerId#", "183382"), //userData.userModel.id.toString()),
      reqType: ReqType.Get,
      callback: (model) async {
        if (mounted && model != null) {
          if (model.success) {
            getForeCasts4AccResponseData = model.responseData.responseData;
            setState(() {});
          } else {
            showToast(context: context, msg: "Something went wrong");
          }
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    getInsight4AccResponseDataModel = null;
    scrollController.dispose();
    optTitle = null;
    chartData = null;

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    wsOpenBankAPIcall();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.grey.shade200,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          elevation: MyTheme.appbarElevation,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          title: UIHelper().drawAppbarTitle(title: "Open Banking"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: (getForeCasts4AccResponseData != null &&
                  getInsight4AccResponseDataModel != null)
              ? drawLayout()
              : SizedBox(),
        ),
      ),
    );
  }

  void _showPopupMenu() async {
    await showMenu(
      context: context,
      color: Colors.white,
      position: RelativeRect.fromLTRB(30, 80, 0.0, 0.0),
      items: [
        PopupMenuItem(
          child:
              Text("PDF", style: TextStyle(color: Colors.purple, fontSize: 17)),
        ),
        PopupMenuItem(
          child: Text("Excel",
              style: TextStyle(color: Colors.purple, fontSize: 17)),
        ),
      ],
      elevation: 8.0,
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            //drawReportView(),
            drawTabView(),
            SizedBox(height: 10),
            drawStats(),
            SizedBox(height: 10),
            drawFinancialMarkers(),
            SizedBox(height: 10),
            drawLineChart(),
            SizedBox(height: 10),
            drawCatIncome(),
            SizedBox(height: 10),
            drawCatExpense(),
          ],
        ),
      ),
    );
  }
}
