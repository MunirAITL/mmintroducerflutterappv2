import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class NoAccess extends StatefulWidget {
  const NoAccess({Key key}) : super(key: key);

  @override
  _NoAccessState createState() => _NoAccessState();
}

class _NoAccessState extends State<NoAccess> with Mixin {
  DateTime currentBackPressTime = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyTheme.bgColor2,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "You don't have permission to this client application.",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.normal),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 50,
            ),
            MMBtn(
                txt: "Close",
                height: getHP(context, 6),
                width: getW(context) / 2,
                callback: () {
                  CookieMgr().delCookiee();
                  DBMgr.shared.delTable("User");
                  SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                  /*Get.offAll(
                    () => LoginLandingScreen(),
              ).then((value) {
                //callback(route);
              });*/
                })
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    /* Get.dialog(
      AlertDialogOkButton(which: 1,msg: "You don't have permission to this client application.",callback: (){
        Get.offAll(LoginLandingScreen());
      },txtColor: Colors.black87,bgColor: Colors.white,),
      transitionDuration: Duration(milliseconds: 400),
    );*/
    super.initState();
  }
}
