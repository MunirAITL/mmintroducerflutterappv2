import 'package:aitl/Mixin.dart';
import 'package:flutter/material.dart';

class AnimatedListItem extends StatefulWidget {
  final int index;
  final Widget itemDesign;

  AnimatedListItem({this.index, this.itemDesign});

  @override
  _AnimatedListItemState createState() => _AnimatedListItemState();
}

class _AnimatedListItemState extends State<AnimatedListItem> with Mixin {
  bool _animate;
  static bool _isStart;

  @override
  void initState() {
    _animate = false;
    _isStart = true;
    // debugPrint("inst state call ${widget.index}");
    super.initState();
    _isStart
        ? Future.delayed(Duration(milliseconds: 100), () {
            setState(() {
              _animate = true;
              _isStart = false;
            });
          })
        : _animate = true;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: Duration(milliseconds: 500),
      opacity: _animate ? 1 : 0,
      curve: Curves.easeInOutQuart,
      child: AnimatedPadding(
          duration: Duration(milliseconds: 500),
          padding: _animate
              ? const EdgeInsets.all(4.0)
              : const EdgeInsets.only(top: 10),
          child: widget.itemDesign),
    );
  }
}
