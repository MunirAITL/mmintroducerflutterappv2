import 'package:flutter/material.dart';
import '../../config/MyTheme.dart';
import '../../controller/helper/db_cus/tab_newcase/action_req_helper.dart';
import '../../model/DashBoardListType.dart';
import '../../model/json/db_cus/tab_newcase/MortgageCaseInfos.dart';
import '../../model/json/db_cus/tab_newcase/MortgageCaseRecomendationInfos.dart';
import '../../view_model/helper/ui_helper.dart';
import '../dashboard_main_base.dart';

class ActionReqPage extends StatefulWidget {
  final List<DashBoardListType> listUserList;
  final List<dynamic> listUserNotesModel;
  final List<MortgageCaseRecomendationInfos> caseReviewModelList;
  final List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList;
  ActionReqPage(
      {Key key,
      @required this.listUserList,
      @required this.listUserNotesModel,
      @required this.caseReviewModelList,
      @required this.caseMortgageAgreementReviewInfosList})
      : super(key: key);
  @override
  State<ActionReqPage> createState() => _ActionReqPageState();
}

class _ActionReqPageState extends BaseMainDashboard<ActionReqPage> {
  @override
  refreshData() {}

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        setState(() {
          isLoading = true;
        });
        if (mounted && widget.listUserList != null) {
          if (widget.listUserList.length > 0) {
            drawActionRequiredItems(
                widget.listUserList,
                widget.listUserNotesModel,
                widget.caseReviewModelList,
                widget.caseMortgageAgreementReviewInfosList,
                false);
          }
        }
        if (mounted) {
          setState(() {
            isLoading = false;
          });
        }
        /*await ActionReqHelper().wsActionReqAPIcall(
          context: context,
          callback: (
            List<DashBoardListType> listUserList,
            List<dynamic> listUserNotesModel,
            List<MortgageCaseRecomendationInfos> caseReviewModelList,
            List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList,
          ) {
            if (mounted && listUserList != null) {
              if (listUserList.length > 0) {
                drawActionRequiredItems(
                    listUserList,
                    listUserNotesModel,
                    caseReviewModelList,
                    caseMortgageAgreementReviewInfosList,
                    false);
              }
            }
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          },
        );*/
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          centerTitle: false,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Action Required"),
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.titleColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return isLoading
        ? SizedBox()
        : Container(
            child: SingleChildScrollView(
                primary: true,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: 20),
                      (widActionRequired != null)
                          ? Container(child: widActionRequired)
                          : SizedBox(),
                    ])));
  }
}
