import 'package:aitl/mixin.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class BaseApp<T extends StatefulWidget> extends State<T> with Mixin {
  drawAppbar({
    Function onBack,
    String backTitle,
    String title,
    List<IconButton> listBtn,
    double elevation = 0,
    PreferredSizeWidget bottom,
    Color bgColor = Colors.transparent,
  }) {
    return AppBar(
      backgroundColor: bgColor,
      automaticallyImplyLeading: false,
      elevation: elevation,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          onBack != null
              ? Flexible(
                  child: GestureDetector(
                    onTap: () {
                      onBack();
                    },
                    child: Container(
                      color: Colors.transparent,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                            //size: 20,
                          ),
                          //SizedBox(width: 5),
                          /*(backTitle != null)
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 0),
                                  child: Text(backTitle,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 17,
                                          color: Colors.white,
                                          fontWeight: FontWeight.normal)),
                                )
                              : SizedBox()*/
                        ],
                      ),
                    ),
                  ),
                )
              : SizedBox(),
          (title != null)
              ? Expanded(
                  flex: 3,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      //color: Colors.yellow,
                      child: Flexible(
                        child: FittedBox(
                          fit: BoxFit.fitWidth,
                          child: AutoSizeText(title,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal)),
                        ),
                      ),
                    ),
                  ),
                )
              : SizedBox(),
        ],
      ),
      actions: listBtn,
      bottom: bottom,
    );
  }
}
