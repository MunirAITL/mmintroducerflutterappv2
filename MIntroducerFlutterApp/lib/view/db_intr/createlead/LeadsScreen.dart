import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/ServerIntr.dart';
import 'package:aitl/config/db_intro/intro_cfg.dart';
import 'package:aitl/controller/api/db_intr/leadfetch/LeadCaseAPIMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_intr/createlead/ResolutionModel.dart';
import 'package:aitl/model/json/db_intr/leadfetch/LeadecaseApiModel.dart';
import 'package:aitl/view/db_intr/createlead/LeadBase.dart';
import 'package:aitl/view/db_intr/createlead/LeadDetails.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view_model/rx/ManageLeadSearchController.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../controller/network/NetworkMgr.dart';
import '../../../model/json/db_intr/createlead/CaseOwnerAPIModel.dart';
import '../../../model/json/db_intr/createlead/GetAllResSupportAdminAPIModel.dart';
import '../../../model/json/db_intr/createlead/GetTwilioPhoneNumberAPIModel.dart';
import '../../../model/json/db_intr/createlead/GetUserTaskCatAPIModel.dart';
import '../../../view_model/api/api_view_model.dart';
import '../../../view_model/helper/ui_helper.dart';
import '../../widgets/dialog/DatePickerView.dart';
import '../../widgets/images/MyNetworkImage.dart';
import '../../widgets/progress/AppbarBotProgbar.dart';
import '../../widgets/txt/Txt.dart';

class LeadsScreen extends StatefulWidget {
  const LeadsScreen({Key key}) : super(key: key);

  @override
  State createState() => _LeadsScreenState();
}

class _LeadsScreenState extends BaseLead<LeadsScreen> {
  List<ResolutionModel> listResolutions = [];
  List<dynamic> listAllSupAdminResolutions = [];
  List<UserTaskCategorys> userTaskCategorys = [];
  List<Users> listOwnerName;
  String twilioPhoneNumber = '';

  int pageStart = 1;
  int pageCount = AppConfig.page_limit;
  bool isPageDone = false;
  bool isLoading = false;
  final customDateTime = "Custom Date Time";

  final List<String> listStage = [
    "Added",
    "Qualifying",
    "Processing",
    "Converted",
    "Junk",
    "None"
  ];

  final List<String> listStatus = [
    "All",
    "Converted to Customer",
    "Call back later",
    "Attempt to contact",
    "Needs information",
    "Wait for now",
    "Unsolicited call",
    "Do not contact again",
    "Not found property yet",
    "No longer proceeding",
    "Changed circumstances",
    "Wrong number",
    "Gone elsewhere",
    "Lost to competition",
    "Deleted"
  ];

  caseOwnerAPI() async {
    try {
      ddCaseOwner =
          DropListModel([OptionItem(id: "0", title: selectCaseOwner)]);
      optCaseOwner = OptionItem(id: "0", title: selectCaseOwner);
      var url = ServerIntr
          .GET_USERBYCOMMUNITYID_COMPUSERID_FOR_SELECT_OPT_ADVISOR_URL;
      url = url.replaceAll("#communityId#", userData.userModel.communityID);
      url = url.replaceAll(
          "#userCompanyId#", userData.userModel.userCompanyID.toString());
      await APIViewModel().req<CaseOwnerAPIModel>(
          context: context,
          url: url,
          reqType: ReqType.Get,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                listOwnerName = model.responseData.users;
                for (final caseOwner in listOwnerName) {
                  if (IntroCfg.listCaseOwnerCommunityID
                          .contains(caseOwner.communityId) &&
                      caseOwner.name != null) {
                    ddCaseOwner.listOptionItems.add(OptionItem(
                        id: caseOwner.id.toString(), title: caseOwner.name));
                  }
                }
              }
            }
          });
    } catch (e) {}
  }

  getUserTaskCategoryAPI() async {
    try {
      await APIViewModel().req<GetUserTaskCatAPIModel>(
          context: context,
          url: ServerIntr.GET_USER_TASK_CATEGORY_URL,
          reqType: ReqType.Get,
          isLoading: false,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                userTaskCategorys = model.responseData.userTaskCategorys;
              }
            }
          });
    } catch (e) {}
  }

  geTwilioPhoneNumberAPI() async {
    try {
      await APIViewModel().req<GetTwilioPhoneNumberAPIModel>(
          context: context,
          url: ServerIntr.GET_TWILIO_PHONENUMBER_URL
              .replaceAll("#userId#", userData.userModel.id.toString()),
          reqType: ReqType.Get,
          isLoading: false,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                twilioPhoneNumber = model.responseData.twilioPhoneNumber;
              }
            }
          });
    } catch (e) {}
  }

  geAllResSupAdminIDAPI() async {
    try {
      var url = ServerIntr.GET_ALL_RES_SUPPORT_ADMINID_URL;
      url =
          url.replaceAll("#SupportAdminId#", userData.userModel.id.toString());
      url = url.replaceAll("#IsLockAutoCall#", "1");
      await APIViewModel().req<GetAllResSupportAdminAPIModel>(
          context: context,
          url: url,
          reqType: ReqType.Get,
          isLoading: false,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                listAllSupAdminResolutions = model.responseData.resolutions;
              }
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listResolutions = null;
    super.dispose();
  }

  @override
  doSearch(eSearch v) {
    switch (v) {
      case eSearch.Stage:
        break;
      case eSearch.Status:
        break;
      case eSearch.DueDate:
        break;
      default:
    }
    refreshData();
  }

  appInit() async {
    try {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        // executes after build
        wBox = getW(context) / 3.5;
      });
      await caseOwnerAPI();
      await refreshData();
      await getUserTaskCategoryAPI();
      await geTwilioPhoneNumberAPI();
      await geAllResSupAdminIDAPI();
      setState(() {});
    } catch (e) {}
  }

  Future<void> refreshData() async {
    setState(() {
      pageStart = 1;
      isPageDone = false;
      isLoading = true;
      listResolutions.clear();
    });
    onLazyLoadAPI();
  }

  onLazyLoadAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
      final stage = listStage[manageLeadController.stageRB.value];
      final orderBy = listSortBy[manageLeadController.sortByRB.value];

      if (date1 == '' && date2 == '') {
        date1 = DateFormat('dd-MMM-yyyy HH:mm')
            .format(DateTime.now().subtract(new Duration(days: 7)));
        date2 = DateFormat('dd-MMM-yyyy HH:mm').format(DateTime.now());
      }

      await LeadCaseAPIMgr().getLeadCase(
          context: context,
          param: {
            'UserCompanyId': userData.userModel.userCompanyID.toString(),
            'Criteria': '1',
            'AssigneeId': optCaseOwner.id,
            'Status': listStatus[manageLeadController.statusRB.value],
            'IsSpecificDate': "",
            'FromDateTime': date1,
            'ToDateTime': date2,
            'InitiatorId': '0',
            'Title': '',
            'Stage': stage != "None" ? stage : "",
            'LeadStatus': '',
            'Page': pageStart.toString(),
            'Count': pageCount.toString(),
            'SearchText': searchByTxt.text.trim(),
            'OrderBy': orderBy != "None" ? orderBy : "",
            'ResolutionType': "All",
            'DateTimeSelectOption': selectedDateTime
          } /*{
            'Status': listStatus[manageLeadController.statusRB.value],
            'Title': "",
            'Description': "",
            'Remarks': "",
            'InitiatorId': "0",
            'ServiceDate': "",
            'ResolutionType': "All",
            'ParentId': "0",
            'Page': pageStart.toString(),
            'Count': pageCount.toString(),
            'AssigneeId': "0",
            'FromDateTime': date1,
            'ToDateTime': date2,
            'Criteria': "1",
            'TaskStatus': "901",
            'UserCompanyId': userData.userModel.userCompanyID.toString(),
            'LeadStatus': "",
            'Stage': stage != "None" ? stage : "",
            'OrderBy': orderBy != "None" ? orderBy : "",
            'SearchText': "",
            'IsSpecificDate': "",
            'DateTimeSelectOption':
                (selectedDateTime != customDateTime) ? selectedDateTime : '',
          }*/
          ,
          callback: (model) {
            if (mounted && model != null) {
              if (model.success) {
                final List<ResolutionModel> resolutions =
                    model.responseData.resolutions;
                if (resolutions != null) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (resolutions.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (ResolutionModel res in resolutions) {
                      listResolutions.add(res);
                    }
                  } catch (e) {
                    myLog(e.toString());
                  }

                  setState(() {
                    isLoading = false;
                  });
                } else {
                  setState(() {
                    isLoading = false;
                  });
                }
              } else {
                listResolutions = [];
              }
              setState(() {});
            }
          });
    }
    if (mounted) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: MyTheme.titleColor,
          title: UIHelper().drawAppbarTitle(title: "Manage Leads"),
          centerTitle: false,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight((isLoading) ? .5 : 0),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return NotificationListener(
      onNotification: (scrollNotification) {
        if (scrollNotification is ScrollStartNotification) {
          //print('Widget has started scrolling');
        } else if (scrollNotification is ScrollEndNotification) {
          if (!isPageDone) {
            pageStart++;
            onLazyLoadAPI();
          }
        }
        return true;
      },
      child: RefreshIndicator(
        color: Colors.white,
        backgroundColor: MyTheme.brandColor,
        onRefresh: refreshData,
        child: SingleChildScrollView(
          primary: true,
          child: Column(
            children: [
              //drawSearchHeader(),
              drawSearchHeader(),
              (listResolutions.length > 0)
                  ? ListView.builder(
                      addAutomaticKeepAlives: true,
                      cacheExtent: AppConfig.page_limit.toDouble(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      primary: false,
                      itemCount: listResolutions.length,
                      itemBuilder: (BuildContext context, int index) {
                        return drawItems(listResolutions[index]);
                      },
                    )
                  : (isLoading)
                      ? CircularProgressIndicator()
                      : drawNF(),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawItems(ResolutionModel resolutions) {
    var stageColor = MyTheme.titleColor;
    try {
      stageColor = IntroCfg.stageColor[resolutions.stage];
    } catch (e) {}

    final name = (resolutions.firstName ?? '') +
        " " +
        (resolutions.middleName ?? '') +
        " " +
        (resolutions.lastName ?? '');

    return GestureDetector(
      onTap: () {
        Get.to(() => LeadDetails(
              resolution: resolutions,
            )).then((value) => refreshData());
      },
      child: (resolutions != null)
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Card(
                  elevation: 5,
                  //color: Colors.transparent,
                  child: IntrinsicHeight(
                    child: Row(
                      children: [
                        Container(width: getWP(context, .5), color: stageColor),
                        SizedBox(width: 10),
                        Container(
                          width: getWP(context, 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: 10),
                              Container(
                                width: getWP(context, 15),
                                height: getWP(context, 15),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: MyNetworkImage.loadProfileImage(
                                        resolutions.profileImageUrl),
                                    fit: BoxFit.cover,
                                  ),
                                  shape: BoxShape.circle,
                                ),
                              ),
                              SizedBox(height: 5),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.grey.shade300,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(2),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      SizedBox(width: 5),
                                      Txt(
                                          txt: "Stage",
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .5,
                                          txtAlign: TextAlign.start,
                                          isBold: false),
                                      drawCircle(
                                          context: context,
                                          color: stageColor,
                                          size: 4),
                                      SizedBox(width: 2),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 5),
                            ],
                          ),
                        ),
                        SizedBox(width: 10),
                        Expanded(
                          child: Container(
                            width: getWP(context, 79),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10),
                                Txt(
                                    txt: name ?? '',
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: true),
                                SizedBox(height: 2),
                                Txt(
                                    txt: resolutions.resolutionType ?? '',
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: true),
                                SizedBox(height: 2),
                                Txt(
                                    txt: resolutions.title ?? '',
                                    txtColor: Colors.red,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                                SizedBox(height: 2),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.email,
                                      color: Colors.grey,
                                      size: 15,
                                    ),
                                    SizedBox(width: 5),
                                    Flexible(
                                      child: Txt(
                                          txt: resolutions.emailAddress ?? '',
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .2,
                                          txtAlign: TextAlign.start,
                                          isBold: false),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 2),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.phone,
                                      color: Colors.grey,
                                      size: 15,
                                    ),
                                    SizedBox(width: 5),
                                    Flexible(
                                      child: Txt(
                                          txt: resolutions.phoneNumber ?? '',
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .2,
                                          txtAlign: TextAlign.start,
                                          isBold: false),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 2),
                                Row(
                                  children: [
                                    Flexible(
                                      child: Txt(
                                          txt: resolutions.stage ?? '',
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .4,
                                          txtAlign: TextAlign.start,
                                          isBold: false),
                                    ),
                                    SizedBox(width: 5),
                                    Icon(Icons.watch_later_rounded,
                                        color: Colors.grey.shade400, size: 20),
                                    SizedBox(width: 5),
                                    Flexible(
                                      child: Txt(
                                          txt: DateFun.getDate(
                                                  resolutions.creationDate,
                                                  "kk:mm, dd MMM yyyy") ??
                                              '',
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .4,
                                          txtAlign: TextAlign.start,
                                          isBold: false),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 10),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )
          : SizedBox(),
    );
  }

  drawSearchHeader() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        width: getW(context),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                    child: drawHeaderRadioDialog(
                        title: manageLeadController.stageRB != null
                            ? listStage[manageLeadController.stageRB.value]
                            : "Stage",
                        list: listStage,
                        e: eSearch.Stage)),
                //SizedBox(width: 5),
                Flexible(
                    child: drawHeaderRadioDialog(
                        title: manageLeadController.statusRB != null
                            ? listStatus[manageLeadController.statusRB.value]
                            : "Status",
                        list: listStatus,
                        e: eSearch.Status)),
                // SizedBox(width: 5),
                Flexible(child: drawHeaderCustomDialog()),
              ],
            ),
            SizedBox(height: 5),
            Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
              Container(
                width: getWP(context, 30),
                decoration: BoxDecoration(
                    color: Colors.grey.shade200,
                    border: Border.all(
                      color: Colors.black26,
                    ),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      bottomLeft: Radius.circular(5),
                    )),
                child: Padding(
                  padding: const EdgeInsets.all(9),
                  child: Txt(
                      txt: "Search by text",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
              ),
              //SizedBox(width: 10),
              Expanded(
                //flex: 3,
                child: TextField(
                  controller: searchByTxt,
                  style: TextStyle(color: Colors.black, fontSize: 13),
                  decoration: new InputDecoration(
                    isDense: true,
                    counterText: "",
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                    suffixIcon: searchByTxt.text.length > 0
                        ? IconButton(
                            onPressed: () {
                              searchByTxt.clear();
                            },
                            icon: Icon(
                              Icons.close,
                              color: Colors.black,
                            ))
                        : null,
                    labelText: "Lead id/Name/Email/Phone Number etc",
                    labelStyle: TextStyle(color: Colors.grey, fontSize: 13),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(5),
                        bottomRight: Radius.circular(5),
                      ),
                      borderSide: const BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(5),
                        bottomRight: Radius.circular(5),
                      ),
                      borderSide: BorderSide(color: Colors.black),
                    ),
                  ),
                ),
              ),
            ]),
            SizedBox(height: 5),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: getWP(context, 25),
                  decoration: BoxDecoration(
                      color: Colors.grey.shade200,
                      border: Border.all(
                        color: Colors.black26,
                      ),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5),
                        bottomLeft: Radius.circular(5),
                      )),
                  child: Padding(
                    padding: const EdgeInsets.all(9),
                    child: Txt(
                        txt: "Date Time",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                ),
                //SizedBox(width: 10),
                Expanded(
                  //flex: 3,
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton2(
                      buttonHeight: 37,
                      buttonWidth: getW(context),
                      buttonPadding: const EdgeInsets.only(left: 14, right: 14),
                      buttonDecoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(0),
                        border: Border.all(
                          color: Colors.black26,
                        ),
                        color: MyTheme.bgColor2,
                      ),
                      dropdownDecoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: MyTheme.bgColor2,
                      ),
                      iconEnabledColor: Colors.black,
                      iconDisabledColor: Colors.grey,
                      hint: Text(
                        '12 Hours',
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                      items: itemsDateTime
                          .map((item) => DropdownMenuItem<String>(
                                value: item,
                                child: Text(
                                  item,
                                  style: const TextStyle(
                                    fontSize: 14,
                                    color: Colors.black,
                                  ),
                                ),
                              ))
                          .toList(),
                      value: selectedDateTime,
                      onChanged: (value) {
                        selectedDateTime = value;
                        setState(() {
                          if (value != customDateTime) {
                            date1 = "";
                            date2 = "";
                            if (value == "12 Hours") {
                              date1 = DateFormat('dd-MMM-yyyy HH:mm').format(
                                  DateTime.now()
                                      .subtract(new Duration(hours: 12)));
                              date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                  .format(DateTime.now());
                            } else if (value == "24 Hours") {
                              date1 = DateFormat('dd-MMM-yyyy HH:mm').format(
                                  DateTime.now()
                                      .subtract(new Duration(days: 1)));
                              date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                  .format(DateTime.now());
                            } else if (value == "2 Days") {
                              date1 = DateFormat('dd-MMM-yyyy HH:mm').format(
                                  DateTime.now()
                                      .subtract(new Duration(days: 2)));
                              date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                  .format(DateTime.now());
                            } else if (value == "7 Days") {
                              date1 = DateFormat('dd-MMM-yyyy HH:mm').format(
                                  DateTime.now()
                                      .subtract(new Duration(days: 7)));
                              date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                  .format(DateTime.now());
                            } else if (value == "All Dates") {
                              date1 = DateFormat('dd-MMM-yyyy HH:mm').format(
                                  DateTime.now()
                                      .subtract(new Duration(days: 365)));
                              date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                  .format(DateTime.now());
                            }
                          } else {
                            datelast = DateTime(dateNow.year, dateNow.month,
                                dateNow.day, dateNow.hour, dateNow.minute);
                            datefirst = DateTime(
                                dateNow.year - 10,
                                dateNow.month,
                                dateNow.day - 7,
                                dateNow.hour,
                                dateNow.minute);
                            datelast2 = DateTime(dateNow.year, dateNow.month,
                                dateNow.day, dateNow.hour, dateNow.minute);
                            datefirst2 = DateTime(
                                dateNow.year - 10,
                                dateNow.month,
                                dateNow.day,
                                dateNow.hour,
                                dateNow.minute);
                            date1 = DateFormat('dd-MMM-yyyy HH:mm').format(
                                DateTime.now().subtract(new Duration(days: 7)));
                            date2 = DateFormat('dd-MMM-yyyy HH:mm')
                                .format(DateTime.now());
                          }
                        });
                      },
                      //itemHeight: 40,
                    ),
                  ),
                ),
              ],
            ),
            (selectedDateTime == customDateTime)
                ? drawCustomDateTime()
                : SizedBox(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                    child: OutlinedButton(
                        onPressed: () {
                          manageLeadController.stageRB.value = 5;
                          manageLeadController.statusRB.value = 0;
                          manageLeadController.sortByRB.value = 0;
                          optCaseOwner =
                              OptionItem(id: null, title: "Select case owner");
                          selectedDateTime = itemsDateTime[0];
                          searchByTxt.clear();
                          refreshData();
                        },
                        style: OutlinedButton.styleFrom(
                          backgroundColor: MyTheme.greyColor,
                          side: BorderSide(width: .5, color: Colors.black),
                        ),
                        child: Text(
                          "Reset",
                          style: TextStyle(color: Colors.black54),
                        ))),
                SizedBox(width: 50),
                Flexible(
                  child: OutlinedButton(
                    onPressed: () {
                      refreshData();
                    },
                    style: OutlinedButton.styleFrom(
                      backgroundColor: MyTheme.greyColor,
                      side: BorderSide(width: .5, color: Colors.black),
                    ),
                    child: const Text(
                      "Search",
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
