import 'package:flutter/material.dart';

import '../../../../config/AppDefine.dart';
import '../../../../config/MyTheme.dart';
import '../../../../config/ServerIntr.dart';
import '../../../../controller/network/NetworkMgr.dart';
import '../../../../mixin.dart';
import '../../../../model/data/UserData.dart';
import '../../../../model/json/db_intr/createlead/CaseOwnerAPIModel.dart';
import '../../../../view_model/api/api_view_model.dart';
import '../../../widgets/dropdown/DropDownListDialog.dart';
import '../../../widgets/dropdown/DropListModel.dart';
import '../../../widgets/input/utils/DecimalTextInputFormatter.dart';
import '../../../widgets/txt/Txt.dart';
import '../../intro_mixin.dart';

abstract class BaseLeadCase<T extends StatefulWidget> extends State<T>
    with Mixin, IntroMixin {
  //  are you charging fee or another fee layout
  DropListModel ddChargingFee = DropListModel([
    OptionItem(id: "1", title: "Application"),
    OptionItem(id: "2", title: "Offer"),
    OptionItem(id: "3", title: "Completion"),
  ]);
  OptionItem optChargingFee = OptionItem(id: null, title: "Select");

  DropListModel ddChargingAnotherFee = DropListModel([
    OptionItem(id: "1", title: "Application"),
    OptionItem(id: "2", title: "Offer"),
    OptionItem(id: "3", title: "Completion"),
  ]);
  OptionItem optChargingAnotherFee = OptionItem(id: null, title: "Select");

  //
  final chargingFee = TextEditingController();
  final chargingAnotherFee = TextEditingController();
  final estEarning = TextEditingController();

  //
  final focusChargingFee = FocusNode();
  final focusChargingAnotherFee = FocusNode();
  final focusEstEarning = FocusNode();

  bool isChargingFee = false;
  bool isChargingFeeRefundable = false;
  bool isFeeRefundAble = false;

  bool isChargingAnotherFee = false;
  bool isChargingAnotherFeeRefundable = false;
  bool isAnotherChargingFeeRefundAble = false;

  //  case owner layout
  DropListModel ddCaseOwner;
  OptionItem optCaseOwner = OptionItem(id: null, title: "Select case owner");

  caseOwnerAPI(Function(List<Users>) callback) async {
    try {
      var url = ServerIntr
          .GET_USERBYCOMMUNITYID_COMPUSERID_FOR_SELECT_OPT_ADVISOR_URL;
      url = url.replaceAll(
          "#communityId#", userData.userModel.communityID); //  BS consultant
      url = url.replaceAll("#userCompanyId#",
          userData.userModel.userCompanyID.toString()); //  BS consultant
      //url = url.replaceAll(
      //"#userId#", userData.userModel.id.toString()); //  MM negotiator
      await APIViewModel().req<CaseOwnerAPIModel>(
          context: context,
          url: url,
          reqType: ReqType.Get,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                callback(model.responseData.users);
              }
            }
          });
    } catch (e) {}
  }

  drawChargingFeeBox(
      TextEditingController tf,
      FocusNode focus,
      DropListModel dd,
      OptionItem opt,
      bool isCharging,
      bool isRef,
      Function(OptionItem) callback,
      Function(bool) callbackRefunable) {
    return isCharging
        ? Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Txt(
                    txt: "How much?",
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 5),
                Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10)),
                        color: Colors.grey.shade400,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Txt(
                            txt: AppDefine.CUR_SIGN,
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize + .3,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        textInputAction: TextInputAction.next,
                        focusNode: focus,
                        controller: tf,
                        onEditingComplete: () {
                          //FocusScope.of(context).requestFocus(reqFocus);
                        },
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2)
                        ],
                        keyboardType:
                            TextInputType.numberWithOptions(decimal: true),
                        maxLength: 8,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize),
                          height: MyTheme.txtLineSpace,
                        ),
                        decoration: new InputDecoration(
                          counterText: "",
                          isDense: true,
                          hintText: "0",
                          hintStyle: new TextStyle(
                            color: Colors.grey,
                            fontSize: getTxtSize(
                                context: context, txtSize: MyTheme.txtSize),
                            //height: MyTheme.txtLineSpace,
                          ),
                          contentPadding: EdgeInsets.only(
                              top: 10, bottom: 10, left: 20, right: 20),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                bottomRight: Radius.circular(10)),
                            borderSide:
                                BorderSide(width: .5, color: Colors.black),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                bottomRight: Radius.circular(10)),
                            borderSide:
                                BorderSide(width: .5, color: Colors.grey),
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(10)),
                              borderSide: BorderSide(
                                width: .5,
                              )),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Txt(
                    txt: "When payable?",
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 5),
                DropDownListDialog(
                  context: context,
                  title: opt.title,
                  ddTitleList: dd,
                  callback: (optionItem) {
                    //opt = optionItem;
                    callback(optionItem);
                  },
                ),
                SizedBox(height: 20),
                Txt(
                    txt: "Refundable",
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 5),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                        child: GestureDetector(
                            onTap: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              callbackRefunable(true);
                            },
                            child: radioButtonitem2(
                                context: context,
                                text: "Yes",
                                txtSize: 1.7,
                                bgColor: isRef
                                    ? MyTheme.purpleColor
                                    : Color(0xFF000),
                                textColor: Colors.black))),
                    SizedBox(width: 10),
                    Flexible(
                        child: GestureDetector(
                            onTap: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              callbackRefunable(false);
                            },
                            child: radioButtonitem2(
                                context: context,
                                text: "No",
                                txtSize: 1.7,
                                bgColor: !isRef
                                    ? MyTheme.purpleColor
                                    : Color(0xFF000),
                                textColor: Colors.black))),
                  ],
                ),
              ],
            ),
          )
        : SizedBox();
  }
}
