import 'dart:convert';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/json/db_intr/createlead/ResolutionModel.dart';
import 'package:aitl/view/db_intr/intro_mixin.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import '../../../../config/AppConfig.dart';
import '../../../../config/AppDefine.dart';
import '../../../../config/db_cus/NewCaseCfg.dart';
import '../../../../config/db_intro/intro_cfg.dart';
import '../../../../controller/api/db_intr/case_lead/CaseLeadNavigatorAPIMgr.dart';
import '../../../../controller/form_validator/UserProfileVal.dart';
import '../../../../controller/helper/db_intr/case_lead/CaseLeadNegotiatorHelper.dart';
import '../../../../model/json/db_cus/case_lead/CaseLeadNegotiatorUserModel.dart';
import '../../../../model/json/db_intr/leadfetch/LeadecaseApiModel.dart';
import '../../../widgets/btn/MMBtn.dart';
import '../../../widgets/dialog/DatePickerView.dart';
import '../../../widgets/gplaces/GPlacesView.dart';
import '../../../widgets/input/InputTitleBox.dart';
import '../../../widgets/input/InputTitleBoxfWithCountryCode.dart';
import '../../../widgets/input/utils/DecimalTextInputFormatter.dart';
import '../../../widgets/txt/IcoTxtIco.dart';
import '../../../widgets/txt/Txt.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:get/get.dart';

class EditLeadPage extends StatefulWidget {
  final ResolutionModel resolutions;
  const EditLeadPage({Key key, @required this.resolutions}) : super(key: key);
  @override
  State createState() => _EditLeadPageState();
}

class _EditLeadPageState extends State<EditLeadPage> with Mixin, IntroMixin {
  bool isChargingFee = false;
  bool isChargingFeeRefundable = false;

  bool isChargingAnotherFee = false;
  bool isChargingAnotherFeeRefundable = false;

  DropListModel ddChargingFee = DropListModel([
    OptionItem(id: "1", title: "Application"),
    OptionItem(id: "2", title: "Offer"),
    OptionItem(id: "3", title: "Completion"),
  ]);
  OptionItem optChargingFee = OptionItem(id: null, title: "Select");

  DropListModel ddChargingAnotherFee = DropListModel([
    OptionItem(id: "1", title: "Application"),
    OptionItem(id: "2", title: "Offer"),
    OptionItem(id: "3", title: "Completion"),
  ]);
  OptionItem optChargingAnotherFee = OptionItem(id: null, title: "Select");

  DropListModel ddLeadIntr = DropListModel([]);
  OptionItem optLeadIntr = OptionItem(id: null, title: "Select Initiator");

  //  DOB
  String dob = "";

  bool isddCaseSubTypeTextField = false;

  final title = TextEditingController();
  final fname = TextEditingController();
  final mname = TextEditingController();
  final lname = TextEditingController();
  final email = TextEditingController();
  final mobile = TextEditingController();
  final desc = TextEditingController();

  //
  final focusTitle = FocusNode();
  final focusFname = FocusNode();
  final focusMname = FocusNode();
  final focusLname = FocusNode();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();

  //
  final chargingFee = TextEditingController();
  final chargingAnotherFee = TextEditingController();
  final estEarning = TextEditingController();
  final leadRef = TextEditingController();

  //
  final focusChargingFee = FocusNode();
  final focusChargingAnotherFee = FocusNode();
  final focusEstEarning = FocusNode();
  final focusLeadRef = FocusNode();

  //  lead refid
  bool isLeadRefId = false;
  bool isLeadRefEmailConsent = false;
  String leadAddr = '';

  final leadRefMortgageLenLeft = TextEditingController();
  final leadRefHomeValue = TextEditingController();
  final leadRefMortgagePurpose = TextEditingController();
  final leadRefMortgageBorrowLen = TextEditingController();
  final leadRefCreditRating = TextEditingController();
  final leadRefEmpStatus = TextEditingController();
  final leadRefYearlyIncome = TextEditingController();
  final leadRefMortgageAmount = TextEditingController();

  //
  final focusLeadRefMortgageLenLeft = FocusNode();
  final focusLeadRefHomeValue = FocusNode();
  final focusLeadRefMortgagePurpose = FocusNode();
  final focusLeadRefMortgageBorrowLen = FocusNode();
  final focusLeadRefCreditRating = FocusNode();
  final focusLeadRefEmpStatus = FocusNode();
  final focusLeadRefYearlyIncome = FocusNode();
  final focusLeadRefMortgageAmount = FocusNode();

  String countryCode = "+44";
  String countryName = "GB";

  //  dropdown
  DropListModel ddCaseType;
  OptionItem optCaseType;

  DropListModel ddCaseSubType;
  OptionItem optCaseSubType = OptionItem(id: null, title: "Select");

  //  dropdown
  DropListModel dd = DropListModel([]);
  OptionItem opt = OptionItem(id: null, title: "Select Negotiator");

  int caseIndex = -1;

  validate() {
    if (optCaseType.id == null) {
      showToast(
          context: context,
          msg: "Please select 'Lead Type' from the above options");
      return false;
    }
    if (optCaseType.title != 'Others') {
      if (ddCaseSubType.listOptionItems.length > 0 &&
          optCaseSubType.id == null) {
        showToast(context: context, msg: "Please choose sub category");
        return false;
      }
    } else {
      if (UserProfileVal().isEmpty(context, title, "Please enter lead title")) {
        return false;
      }
    }
    if (!UserProfileVal().isFNameOK(context, fname)) {
      return false;
    }
    if (!UserProfileVal().isLNameOK(context, lname)) {
      return false;
    }
    if (!UserProfileVal().isPhoneOK(context, mobile)) {
      return false;
    }
    if (!UserProfileVal().isEmailOK(context, email, "Invalid email address")) {
      return false;
    }

    return true;
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;
          countryCode = cd;
        });
      }
      leadRef.addListener(() {
        isLeadRefId = leadRef.text.length > 0 ? true : false;
        setState(() {});
      });
    } catch (e) {
      print("sms2 Screen country code and name problem ");
    }

    try {
      optCaseType = OptionItem(id: null, title: "I'm looking for...");
      List<OptionItem> lst = [];

      for (var map in NewCaseCfg.listCreateNewCase) {
        if (map['title'] == widget.resolutions.title) {
          caseIndex = map['index'];
          optCaseType =
              OptionItem(id: map['index'].toString(), title: map['title']);
        }
        lst.add(OptionItem(id: map["index"].toString(), title: map['title']));
      }

      ddCaseType = DropListModel(lst);
      lst = null;
      if (caseIndex != -1)
        reloadCaseSubType(caseIndex, true);
      else {
        final map = NewCaseCfg
            .listCreateNewCase[NewCaseCfg.listCreateNewCase.length - 1];
        optCaseType =
            OptionItem(id: map['index'].toString(), title: map['title']);
        title.text = widget.resolutions.title;
      }
    } catch (e) {}

    try {
      fname.text = widget.resolutions.firstName ?? '';
      mname.text = widget.resolutions.middleName ?? '';
      lname.text = widget.resolutions.lastName ?? '';
      email.text = widget.resolutions.emailAddress ?? '';
      mobile.text = widget.resolutions.phoneNumber ?? '';
      desc.text = widget.resolutions.leadNote ?? '';

      try {
        estEarning.text =
            widget.resolutions.estimatedEarning.toStringAsFixed(2);
      } catch (e) {}
      try {
        dob = widget.resolutions.dateOfBirth ?? '';
      } catch (e) {}
      try {
        isChargingFee =
            widget.resolutions.areYouChargingAFee == "Yes" ? true : false;
        chargingFee.text =
            widget.resolutions.chargeFeeAmount.toStringAsFixed(2);
        optChargingFee = OptionItem(
            id: "0", title: widget.resolutions.chargingFeeWhenPayable);
        isChargingFeeRefundable =
            widget.resolutions.chargingFeeRefundable == "Yes" ? true : false;
      } catch (e) {}
      try {
        isChargingAnotherFee =
            widget.resolutions.areYouChargingAnotherFee == "Yes" ? true : false;
        chargingAnotherFee.text =
            widget.resolutions.chargeAnotherFeeAmount.toStringAsFixed(2);
        optChargingAnotherFee = OptionItem(
            id: "0", title: widget.resolutions.chargingAnotherFeeWhenPayable);
        isChargingAnotherFeeRefundable =
            widget.resolutions.chargingAnotherFeeRefundable == "Yes"
                ? true
                : false;
      } catch (e) {}

      //  lead ref ID
      try {
        leadRef.text = widget.resolutions.leadReferenceId ?? '';
      } catch (e) {}
      try {
        leadAddr = widget.resolutions.address;
      } catch (e) {}
      try {
        leadRefMortgageBorrowLen.text =
            widget.resolutions.mortgageBorrowLength ?? '';
      } catch (e) {}
      try {
        leadRefMortgageLenLeft.text =
            widget.resolutions.mortgageLengthLeft ?? '';
      } catch (e) {}
      try {
        leadRefEmpStatus.text = widget.resolutions.employmentStatus ?? '';
      } catch (e) {}
      try {
        leadRefYearlyIncome.text = widget.resolutions.yearlyIncome ?? "0";
      } catch (e) {}
      try {
        leadRefHomeValue.text = widget.resolutions.homeValue ?? "0";
      } catch (e) {}
      try {
        leadRefMortgagePurpose.text = widget.resolutions.mortgagePurpose ?? "0";
      } catch (e) {}
      try {
        leadRefMortgageAmount.text = widget.resolutions.mortgageAmount ?? "0";
      } catch (e) {}
      try {
        leadRefCreditRating.text = widget.resolutions.creditRating ?? '0';
      } catch (e) {}
      try {
        isLeadRefEmailConsent =
            widget.resolutions.emailConsent == "Yes" ? true : false;
      } catch (e) {}
    } catch (e) {}

    try {
      optLeadIntr =
          OptionItem(id: "0", title: widget.resolutions.createdByUserName);
      ddLeadIntr = DropListModel([optLeadIntr]);
    } catch (e) {}

    try {
      CaseLeadNavigatorAPIMgr().wsGetCaseLeadNavigatorByIntroducerID(
        context: context,
        //userNotesModel: widget.userNotesModel,
        callback: (model) {
          if (mounted) {
            if (model.success) {
              final List<CaseLeadNegotiatorUserModel>
                  listCaseLeadNegotiatorUserModel = model.responseData.users;
              for (CaseLeadNegotiatorUserModel caseLeadNegotiatorUserModel
                  in listCaseLeadNegotiatorUserModel) {
                dd.listOptionItems.add(OptionItem(
                    id: caseLeadNegotiatorUserModel.id.toString(),
                    title: caseLeadNegotiatorUserModel.name));
              }
              setState(() {});
            } else {}
          }
        },
      );

      setState(() {});
    } catch (e) {}
  }

  reloadCaseSubType(int index, bool isInit) {
    for (var map in NewCaseCfg.listCreateNewCase) {
      if (map['index'] == index) {
        List subList = map['subItem'];
        if (subList.length > 0) {
          if (isInit) {
            title.text = widget.resolutions.description;
            optCaseSubType =
                OptionItem(id: "0", title: widget.resolutions.description);
          } else {
            title.clear();
            optCaseSubType = OptionItem(id: null, title: "Select");
          }
          List<OptionItem> lst = [];
          for (final subMap in subList) {
            lst.add(OptionItem(
                id: subMap['index'].toString(), title: subMap['title']));
          }
          ddCaseSubType = DropListModel(lst);
          isddCaseSubTypeTextField = false;
          lst = null;
        } else {
          if (map['title'] == 'Others')
            isddCaseSubTypeTextField = true;
          else
            isddCaseSubTypeTextField = false;
          ddCaseSubType = DropListModel([]);
        }
        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: MyTheme.titleColor,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          title: Txt(
              txt: "Edit Lead",
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);

    return (ddCaseType != null)
        ? Container(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: "Case Type",
                            txtColor: MyTheme.inputColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        SizedBox(height: 10),
                        DropDownListDialog(
                          context: context,
                          title: optCaseType.title,
                          ddTitleList: ddCaseType,
                          callback: (optionItem) {
                            caseIndex = int.parse(optionItem.id);
                            optCaseType = optionItem;
                            optCaseSubType =
                                OptionItem(id: null, title: "Select");
                            reloadCaseSubType(int.parse(optCaseType.id), false);
                            title.clear();
                            setState(() {});
                          },
                        ),
                      ],
                    ),
                    caseIndex != -1
                        ? (ddCaseSubType.listOptionItems.length > 0 &&
                                optCaseType.id != null)
                            ? Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Txt(
                                        txt: "Sub category",
                                        txtColor: MyTheme.inputColor,
                                        txtSize: MyTheme.txtSize,
                                        txtAlign: TextAlign.start,
                                        isBold: false),
                                    SizedBox(height: 10),
                                    DropDownListDialog(
                                      context: context,
                                      title: optCaseSubType.title,
                                      ddTitleList: ddCaseSubType,
                                      callback: (optionItem) {
                                        optCaseSubType = optionItem;
                                        title.text = optCaseSubType.title;
                                        setState(() {});
                                      },
                                    ),
                                  ],
                                ),
                              )
                            : (isddCaseSubTypeTextField)
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 20),
                                    child: drawInputBox(
                                      context: context,
                                      title: "Title",
                                      input: title,
                                      ph: "Please specify",
                                      kbType: TextInputType.text,
                                      inputAction: TextInputAction.next,
                                      focusNode: focusTitle,
                                      focusNodeNext: focusFname,
                                      len: 50,
                                    ))
                                : SizedBox()
                        : Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: drawInputBox(
                              context: context,
                              title: "Title",
                              input: title,
                              ph: "Please specify",
                              kbType: TextInputType.text,
                              inputAction: TextInputAction.next,
                              focusNode: focusTitle,
                              focusNodeNext: focusFname,
                              len: 50,
                            )),
                    SizedBox(height: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: "Are you charging a fee?",
                            txtColor: MyTheme.inputColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                  child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        isChargingFee = true;
                                        setState(() {});
                                      },
                                      child: radioButtonitem(
                                          context: context,
                                          text: "Yes",
                                          txtSize: 1.7,
                                          bgColor: isChargingFee
                                              ? '#252551'
                                              : '#FFF',
                                          textColor: isChargingFee
                                              ? Colors.white
                                              : Colors.black))),
                              SizedBox(width: 10),
                              Flexible(
                                  child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        isChargingFee = false;
                                        setState(() {});
                                      },
                                      child: radioButtonitem(
                                          context: context,
                                          text: "No",
                                          txtSize: 1.7,
                                          bgColor: !isChargingFee
                                              ? '#252551'
                                              : '#FFF',
                                          textColor: !isChargingFee
                                              ? Colors.white
                                              : Colors.black))),
                            ],
                          ),
                        ),
                      ],
                    ),
                    drawChargingFeeBox(
                        chargingFee,
                        focusChargingFee,
                        ddChargingFee,
                        optChargingFee,
                        isChargingFee,
                        isChargingFeeRefundable, (opt) {
                      optChargingFee.title = opt.title;
                      setState(() {});
                    }, (isRef) {
                      isChargingFeeRefundable = isRef;
                      setState(() {});
                    }),
                    SizedBox(height: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: "Are you charging another fee?",
                            txtColor: MyTheme.inputColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                  child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        isChargingAnotherFee = true;
                                        setState(() {});
                                      },
                                      child: radioButtonitem(
                                          context: context,
                                          text: "Yes",
                                          txtSize: 1.7,
                                          bgColor: isChargingAnotherFee
                                              ? '#252551'
                                              : '#FFF',
                                          textColor: isChargingAnotherFee
                                              ? Colors.white
                                              : Colors.black))),
                              SizedBox(width: 10),
                              Flexible(
                                  child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        isChargingAnotherFee = false;
                                        setState(() {});
                                      },
                                      child: radioButtonitem(
                                          context: context,
                                          text: "No",
                                          txtSize: 1.7,
                                          bgColor: !isChargingAnotherFee
                                              ? '#252551'
                                              : '#FFF',
                                          textColor: !isChargingAnotherFee
                                              ? Colors.white
                                              : Colors.black))),
                            ],
                          ),
                        ),
                      ],
                    ),
                    drawChargingFeeBox(
                        chargingAnotherFee,
                        focusChargingAnotherFee,
                        ddChargingAnotherFee,
                        optChargingAnotherFee,
                        isChargingAnotherFee,
                        isChargingAnotherFeeRefundable, (opt) {
                      optChargingAnotherFee.title = opt.title;
                      setState(() {});
                    }, (isRef) {
                      isChargingAnotherFeeRefundable = isRef;
                      setState(() {});
                    }),
                    SizedBox(height: 20),
                    Divider(color: Colors.black),
                    SizedBox(height: 10),
                    drawInputBox(
                      context: context,
                      title: "First Name",
                      input: fname,
                      ph: "",
                      kbType: TextInputType.name,
                      inputAction: TextInputAction.next,
                      focusNode: focusFname,
                      focusNodeNext: focusMname,
                      len: 20,
                    ),
                    SizedBox(height: 20),
                    drawInputBox(
                      context: context,
                      title: "Middle Name",
                      input: mname,
                      ph: "",
                      kbType: TextInputType.name,
                      inputAction: TextInputAction.next,
                      focusNode: focusMname,
                      focusNodeNext: focusLname,
                      len: 20,
                    ),
                    SizedBox(height: 20),
                    drawInputBox(
                      context: context,
                      title: "Last Name",
                      input: lname,
                      ph: "",
                      kbType: TextInputType.name,
                      inputAction: TextInputAction.next,
                      focusNode: focusLname,
                      focusNodeNext: focusMobile,
                      len: 20,
                    ),
                    SizedBox(height: 20),
                    drawInputBoxWithCountryCode(
                        context: context,
                        title: "Phone Number",
                        input: mobile,
                        ph: "xxxx xxx xxx",
                        kbType: TextInputType.phone,
                        inputAction: TextInputAction.next,
                        focusNode: focusMobile,
                        focusNodeNext: focusEmail,
                        len: 15,
                        countryCode: countryCode,
                        countryName: countryName,
                        getCountryCode: (value) {
                          countryCode = value.toString();
                          print("Country Code Clik = " + countryCode);
                          PrefMgr.shared.setPrefStr("countryName", value.code);
                          PrefMgr.shared
                              .setPrefStr("countryCode", value.toString());
                        }),
                    SizedBox(height: 20),
                    drawInputBox(
                      context: context,
                      title: "Email",
                      input: email,
                      ph: "",
                      kbType: TextInputType.emailAddress,
                      inputAction: TextInputAction.done,
                      focusNode: focusEmail,
                      len: 50,
                    ),
                    dd.listOptionItems.length > 0
                        ? Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Txt(
                                    txt: "Negotiator",
                                    txtColor: MyTheme.inputColor,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                                SizedBox(height: 10),
                                DropDownListDialog(
                                  context: context,
                                  title: opt.title,
                                  ddTitleList: dd,
                                  callback: (optionItem) {
                                    opt = optionItem;
                                    setState(() {});
                                  },
                                ),
                              ],
                            ),
                          )
                        : SizedBox(),
                    SizedBox(height: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: "Lead Notes",
                            txtColor: MyTheme.inputColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        SizedBox(height: 10),
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          child: TextField(
                            controller: desc,
                            minLines: 2,
                            maxLines: 4,
                            //expands: true,
                            autocorrect: false,
                            maxLength: 255,
                            keyboardType: TextInputType.multiline,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: getTxtSize(
                                  context: context, txtSize: MyTheme.txtSize),
                            ),
                            decoration: InputDecoration(
                              hintText: 'lead notes if any there',
                              hintStyle: TextStyle(color: Colors.grey),
                              //labelText: 'Your message',
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  left: 15, bottom: 11, top: 11, right: 15),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Divider(color: Colors.black),
                    SizedBox(height: 10),
                    /*Txt(
                        txt: "Select Group",
                        txtColor: MyTheme.inputColor,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(height: 5),
                    DropDownListDialog(
                      context: context,
                      title: optGroup.title,
                      ddTitleList: ddGroup,
                      callback: (optionItem) {
                        optGroup = optionItem;
                        setState(() {});
                      },
                    ),
                    SizedBox(height: 20),*/
                    Txt(
                        txt: "Estimated Earning",
                        txtColor: MyTheme.inputColor,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(height: 5),
                    Txt(
                        txt:
                            "If you're not sure, your best guess is fine at this point.",
                        txtColor: Colors.black54,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(height: 5),
                    Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomLeft: Radius.circular(10)),
                            color: Colors.grey.shade400,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Txt(
                                txt: AppDefine.CUR_SIGN,
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize + .3,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                        ),
                        Expanded(
                          child: TextField(
                            textInputAction: TextInputAction.next,
                            focusNode: focusEstEarning,
                            controller: estEarning,
                            onEditingComplete: () {
                              FocusScope.of(context).requestFocus(focusLeadRef);
                            },
                            inputFormatters: [
                              DecimalTextInputFormatter(decimalRange: 2)
                            ],
                            keyboardType:
                                TextInputType.numberWithOptions(decimal: true),
                            maxLength: 8,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: getTxtSize(
                                  context: context, txtSize: MyTheme.txtSize),
                              height: MyTheme.txtLineSpace,
                            ),
                            decoration: new InputDecoration(
                              counterText: "",
                              isDense: true,
                              hintText: "0",
                              hintStyle: new TextStyle(
                                color: Colors.grey,
                                fontSize: getTxtSize(
                                    context: context, txtSize: MyTheme.txtSize),
                                //height: MyTheme.txtLineSpace,
                              ),
                              contentPadding: EdgeInsets.only(
                                  top: 10, bottom: 10, left: 20, right: 20),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(10),
                                    bottomRight: Radius.circular(10)),
                                borderSide:
                                    BorderSide(width: .5, color: Colors.black),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(10),
                                    bottomRight: Radius.circular(10)),
                                borderSide:
                                    BorderSide(width: .5, color: Colors.grey),
                              ),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      bottomRight: Radius.circular(10)),
                                  borderSide: BorderSide(
                                    width: .5,
                                  )),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10),
                          child: Txt(
                            txt: "Date of birth",
                            txtColor: MyTheme.inputColor,
                            txtSize: MyTheme.txtSize,
                            isBold: false,
                            txtAlign: TextAlign.start,
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            showDatePicker(
                              context: context,
                              initialDate: dateDOBlast,
                              firstDate: dateDOBfirst,
                              lastDate: dateDOBlast,
                              fieldHintText: "dd/mm/yyyy",
                              builder: (context, child) {
                                return Theme(
                                  data: ThemeData.light().copyWith(
                                    colorScheme: ColorScheme.light(
                                        primary: MyTheme.statusBarColor),
                                    buttonTheme: ButtonThemeData(
                                        textTheme: ButtonTextTheme.primary),
                                  ), // This will change to light theme.
                                  child: child,
                                );
                              },
                            ).then((value) {
                              if (value != null) {
                                dob = DateFormat('dd-MM-yyyy')
                                    .format(value)
                                    .toString();
                                setState(() {});
                              }
                            });
                          },
                          child: IcoTxtIco(
                            leftIcon: Icons.calendar_today,
                            txt: dob == "" ? 'Select date of birth' : dob,
                            txtSize: MyTheme.txtSize,
                            txtColor: Colors.black,
                            rightIcon: Icons.keyboard_arrow_down,
                            txtAlign: TextAlign.left,
                            rightIconSize: 30,
                            leftIconSize: 20,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    drawInputBox(
                      context: context,
                      title: "Lead ReferenceId",
                      input: leadRef,
                      ph: "",
                      kbType: TextInputType.name,
                      inputAction: TextInputAction.done,
                      focusNode: focusLeadRef,
                      len: 20,
                    ),
                    isLeadRefId
                        ? Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Column(
                              children: [
                                GPlacesView(
                                    title: "Address",
                                    address: leadAddr,
                                    callback: (String address, Location loc) {
                                      leadAddr = address;
                                      setState(() {});
                                    }),
                                SizedBox(height: 20),
                                drawInputBox(
                                  context: context,
                                  title: "Mortgage Length Left",
                                  input: leadRefMortgageLenLeft,
                                  ph: "",
                                  kbType: TextInputType.text,
                                  inputAction: TextInputAction.next,
                                  focusNode: focusLeadRefMortgageLenLeft,
                                  focusNodeNext: focusLeadRefHomeValue,
                                  len: 255,
                                ),
                                SizedBox(height: 20),
                                drawInputBox(
                                  context: context,
                                  title: "Home Value",
                                  input: leadRefHomeValue,
                                  ph: "",
                                  kbType: TextInputType.text,
                                  inputAction: TextInputAction.next,
                                  focusNode: focusLeadRefHomeValue,
                                  focusNodeNext: focusLeadRefMortgagePurpose,
                                  len: 255,
                                ),
                                SizedBox(height: 20),
                                drawInputBox(
                                  context: context,
                                  title: "Mortgage Purpose",
                                  input: leadRefMortgagePurpose,
                                  ph: "",
                                  kbType: TextInputType.text,
                                  inputAction: TextInputAction.next,
                                  focusNode: focusLeadRefMortgagePurpose,
                                  focusNodeNext: focusLeadRefMortgageBorrowLen,
                                  len: 255,
                                ),
                                SizedBox(height: 20),
                                drawInputBox(
                                  context: context,
                                  title: "Mortgage Borrow Length",
                                  input: leadRefMortgageBorrowLen,
                                  ph: "",
                                  kbType: TextInputType.text,
                                  inputAction: TextInputAction.next,
                                  focusNode: focusLeadRefMortgageBorrowLen,
                                  focusNodeNext: focusLeadRefCreditRating,
                                  len: 255,
                                ),
                                SizedBox(height: 20),
                                drawInputBox(
                                  context: context,
                                  title: "Credit Rating",
                                  input: leadRefCreditRating,
                                  ph: "",
                                  kbType: TextInputType.text,
                                  inputAction: TextInputAction.next,
                                  focusNode: focusLeadRefCreditRating,
                                  focusNodeNext: focusLeadRefEmpStatus,
                                  len: 255,
                                ),
                                SizedBox(height: 20),
                                drawInputBox(
                                  context: context,
                                  title: "Employment Status",
                                  input: leadRefEmpStatus,
                                  ph: "",
                                  kbType: TextInputType.text,
                                  inputAction: TextInputAction.next,
                                  focusNode: focusLeadRefEmpStatus,
                                  focusNodeNext: focusLeadRefYearlyIncome,
                                  len: 255,
                                ),
                                SizedBox(height: 20),
                                drawInputBox(
                                  context: context,
                                  title: "Yearly Income",
                                  input: leadRefYearlyIncome,
                                  ph: "",
                                  kbType: TextInputType.text,
                                  inputAction: TextInputAction.next,
                                  focusNode: focusLeadRefYearlyIncome,
                                  focusNodeNext: focusLeadRefMortgageAmount,
                                  len: 255,
                                ),
                                SizedBox(height: 20),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Txt(
                                        txt: "Email Consent",
                                        txtColor: MyTheme.inputColor,
                                        txtSize: MyTheme.txtSize,
                                        txtAlign: TextAlign.center,
                                        isBold: false),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 10),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Flexible(
                                              child: GestureDetector(
                                                  onTap: () {
                                                    FocusScope.of(context)
                                                        .requestFocus(
                                                            FocusNode());
                                                    isLeadRefEmailConsent =
                                                        true;
                                                    setState(() {});
                                                  },
                                                  child: radioButtonitem(
                                                      context: context,
                                                      text: "Yes",
                                                      txtSize: 1.7,
                                                      bgColor:
                                                          isLeadRefEmailConsent
                                                              ? '#252551'
                                                              : '#FFF',
                                                      textColor:
                                                          isLeadRefEmailConsent
                                                              ? Colors.white
                                                              : Colors.black))),
                                          SizedBox(width: 10),
                                          Flexible(
                                              child: GestureDetector(
                                                  onTap: () {
                                                    FocusScope.of(context)
                                                        .requestFocus(
                                                            FocusNode());
                                                    isLeadRefEmailConsent =
                                                        false;
                                                    setState(() {});
                                                  },
                                                  child: radioButtonitem(
                                                      context: context,
                                                      text: "No",
                                                      txtSize: 1.7,
                                                      bgColor:
                                                          !isLeadRefEmailConsent
                                                              ? '#252551'
                                                              : '#FFF',
                                                      textColor:
                                                          !isLeadRefEmailConsent
                                                              ? Colors.white
                                                              : Colors.black))),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20),
                                drawInputBox(
                                  context: context,
                                  title: "Mortgage Amount",
                                  input: leadRefMortgageAmount,
                                  ph: "",
                                  kbType: TextInputType.text,
                                  inputAction: TextInputAction.done,
                                  focusNode: focusLeadRefMortgageAmount,
                                  len: 255,
                                ),
                              ],
                            ),
                          )
                        : SizedBox(),
                    ddLeadIntr != null
                        ? Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Txt(
                                    txt: "Lead Introducer",
                                    txtColor: MyTheme.inputColor,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.center,
                                    isBold: false),
                                SizedBox(height: 5),
                                DropDownListDialog(
                                  context: context,
                                  title: optLeadIntr.title,
                                  ddTitleList: ddLeadIntr,
                                  callback: (optionItem) {
                                    optLeadIntr = optionItem;
                                  },
                                ),
                              ],
                            ),
                          )
                        : SizedBox(),
                    SizedBox(height: 20),
                    MMBtn(
                      txt: "Save",
                      width: getW(context),
                      height: getHP(context, 6),
                      radius: 10,
                      callback: () {
                        if (validate()) {
                          final param = CaseLeadNegotiatorHelper().putParam(
                            caseType: optCaseType.title,
                            title: title.text.trim(),
                            fname: fname.text.trim(),
                            mname: mname.text.trim(),
                            lname: lname.text.trim(),
                            email: email.text.trim(),
                            mobile: mobile.text.trim(),
                            note: desc.text.trim(),
                            dob: dob,
                            //
                            negotiatorId: dd.listOptionItems.length > 0
                                ? int.parse(opt.id)
                                : 0,
                            groupId: 0,
                            //
                            leadReferenceId: leadRef.text.trim(),
                            leadAddr: leadAddr,
                            leadMortgageLengthLeft:
                                leadRefMortgageLenLeft.text.trim(),
                            leadHomeValue: leadRefHomeValue.text.trim(),
                            leadMortgagePurpose:
                                leadRefMortgagePurpose.text.trim(),
                            leadMortgageAmount:
                                leadRefMortgageAmount.text.trim(),
                            leadMortgageBorrowLength:
                                leadRefMortgageBorrowLen.text.trim(),
                            leadCreditRating: leadRefCreditRating.text.trim(),
                            leadEmploymentStatus: leadRefEmpStatus.text.trim(),
                            leadYearlyIncome: leadRefYearlyIncome.text.trim(),
                            leadEmailConsent:
                                isLeadRefEmailConsent ? "Yes" : "No",
                            estimatedEarning: estEarning.text.trim(),
                            //
                            areYouChargingAFee: isChargingFee ? 'Yes' : 'No',
                            chargeFeeAmount: chargingFee.text.trim(),
                            chargingFeeWhenPayable: optChargingFee.title,
                            chargingFeeRefundable:
                                isChargingFeeRefundable ? 'Yes' : 'No',
                            areYouChargingAnotherFee:
                                isChargingAnotherFee ? 'Yes' : 'No',
                            chargeAnotherFeeAmount:
                                chargingAnotherFee.text.trim(),
                            chargingAnotherFeeWhenPayable:
                                optChargingAnotherFee.title,
                            chargingAnotherFeeRefundable:
                                isChargingAnotherFeeRefundable ? 'Yes' : 'No',
                            //
                            resolution: widget.resolutions,
                          );
                          myLog(json.encode(param));
                          CaseLeadNavigatorAPIMgr().wsPutResolutionAPI(
                            context: context,
                            param: param,
                            callback: (model) {
                              if (model != null && mounted) {
                                try {
                                  if (model.success) {
                                    showToast(
                                        context: context,
                                        msg:
                                            "Lead has been updated successfully",
                                        which: 1);
                                    Future.delayed(
                                        Duration(
                                            seconds: AppConfig.AlertDismisSec -
                                                1), () {
                                      Get.back(result: true);
                                    });
                                  } else {
                                    final err = model
                                        .errorMessages.resolution_post[0]
                                        .toString();
                                    showToast(
                                        context: context, msg: err, which: 0);
                                    setState(() {});
                                  }
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              }
                            },
                          );
                        }
                      },
                    )
                  ],
                ),
              ),
            ),
          )
        : SizedBox();
  }

  drawChargingFeeBox(
      TextEditingController tf,
      FocusNode focus,
      DropListModel dd,
      OptionItem opt,
      bool isCharging,
      bool isRef,
      Function(OptionItem) callback,
      Function(bool) callbackRefunable) {
    return isCharging
        ? Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Txt(
                    txt: "How much?",
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 5),
                Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10)),
                        color: Colors.grey.shade400,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Txt(
                            txt: AppDefine.CUR_SIGN,
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize + .3,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        textInputAction: TextInputAction.next,
                        focusNode: focus,
                        controller: tf,
                        onEditingComplete: () {
                          FocusScope.of(context).requestFocus(focusFname);
                        },
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2)
                        ],
                        keyboardType:
                            TextInputType.numberWithOptions(decimal: true),
                        maxLength: 8,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize),
                          height: MyTheme.txtLineSpace,
                        ),
                        decoration: new InputDecoration(
                          counterText: "",
                          isDense: true,
                          hintText: "0",
                          hintStyle: new TextStyle(
                            color: Colors.grey,
                            fontSize: getTxtSize(
                                context: context, txtSize: MyTheme.txtSize),
                            //height: MyTheme.txtLineSpace,
                          ),
                          contentPadding: EdgeInsets.only(
                              top: 10, bottom: 10, left: 20, right: 20),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                bottomRight: Radius.circular(10)),
                            borderSide:
                                BorderSide(width: .5, color: Colors.black),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                bottomRight: Radius.circular(10)),
                            borderSide:
                                BorderSide(width: .5, color: Colors.grey),
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(10)),
                              borderSide: BorderSide(
                                width: .5,
                              )),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Txt(
                    txt: "When payable?",
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 5),
                DropDownListDialog(
                  context: context,
                  title: opt.title,
                  ddTitleList: dd,
                  callback: (optionItem) {
                    //opt = optionItem;
                    callback(optionItem);
                  },
                ),
                SizedBox(height: 20),
                Txt(
                    txt: "Refundable",
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 5),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                        child: GestureDetector(
                            onTap: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              callbackRefunable(true);
                            },
                            child: radioButtonitem2(
                                context: context,
                                text: "Yes",
                                txtSize: 1.7,
                                bgColor: isRef
                                    ? MyTheme.purpleColor
                                    : Color(0xFF000),
                                textColor: Colors.black))),
                    SizedBox(width: 10),
                    Flexible(
                        child: GestureDetector(
                            onTap: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              callbackRefunable(false);
                            },
                            child: radioButtonitem2(
                                context: context,
                                text: "No",
                                txtSize: 1.7,
                                bgColor: !isRef
                                    ? MyTheme.purpleColor
                                    : Color(0xFF000),
                                textColor: Colors.black))),
                  ],
                ),
              ],
            ),
          )
        : SizedBox();
  }
}
