import 'dart:async';
import 'dart:convert';

import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_intro/intro_cfg.dart';
import 'package:aitl/controller/api/db_intr/case_lead/CaseLeadNavigatorAPIMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view/db_intr/createlead/LeadDetailsBase.dart';
import 'package:aitl/view/db_intr/createlead/leaddetails/change_stage.dart';
import 'package:aitl/view/widgets/dialog/ConfirmInputDialog.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:stream_transform/stream_transform.dart';

import '../../../config/ServerIntr.dart';
import '../../../controller/form_validator/UserProfileVal.dart';
import '../../../controller/helper/db_intr/case_lead/CaseLeadNegotiatorHelper.dart';
import '../../../controller/network/NetworkMgr.dart';
import '../../../model/json/db_intr/createlead/CaseOwnerAPIModel.dart';
import '../../../model/json/db_intr/createlead/GetLeadByResCompanyIdAPIModel.dart';
import '../../../model/json/db_intr/createlead/Post_UserByCommunityIdCompanyIdAugSugAPIModel.dart';
import '../../../model/json/db_intr/createlead/ResolutionModel.dart';
import '../../../model/json/db_intr/createlead/UpdateCaseOwner4MeAPIModel.dart';
import '../../../model/json/db_intr/createlead/UserDetailsDataAutoSuggAPIModel.dart';
import '../../../model/json/db_intr/createlead/UserNotesAPIModel.dart';
import '../../../view_model/api/api_view_model.dart';
import '../../../view_model/generic/enum_gen.dart';
import '../../widgets/dialog/ConfirmationDialog.dart';
import '../../widgets/dropdown/DropListModel.dart';
import '../../widgets/images/MyNetworkImage.dart';
import '../../widgets/progress/AppbarBotProgbar.dart';
import '../../widgets/txt/Txt.dart';
import '../customer/CreateLeadProfile.dart';
import 'email/email_editor.dart';
import 'leaddetails/add_note.dart';
import 'leaddetails/create_lead.dart';

class LeadDetails extends StatefulWidget {
  final ResolutionModel resolution;
  LeadDetails({
    Key key,
    @required this.resolution,
  }) : super(key: key);
  @override
  State createState() => _LeadDetailsState();
}

class _LeadDetailsState extends BaseLeadDetails<LeadDetails> {
  Color stageColor = Colors.blue;

  UserDetailsDatas userDetailsDatas;
  List<UserDetailsDatas> listUserDetailsDatasAutoSug = [];
  final searchController = TextEditingController();
  final ScrollController scrollController = ScrollController();
  final ExpandableController expandableController = ExpandableController();
  final StreamController<String> streamController = StreamController();

  final focusSearch = FocusNode();

  //function I am using to perform some logic
  _validateValues() async {
    if (searchController.text.length > 2 && !isLoading) {
      if (mounted) {
        setState(() {
          isLoading = true;
        });
        try {
          await APIViewModel()
              .req<PostUserByCommunityIdCompanyIdAugSugAPIModel>(
                  context: context,
                  url: ServerIntr.POST_USER_BY_COMMUNITYID_COMPANYID_AUTOSUG,
                  reqType: ReqType.Post,
                  isLoading: false,
                  param: {
                    "SearchText": searchController.text.trim(),
                    "UserCompanyId": userData.userModel.userCompanyID,
                    "CommunityIdList": IntroCfg.listCaseOwnerAutoSug
                  },
                  callback: (model) async {
                    if (mounted && model != null) {
                      if (model.success) {
                        listUserDetailsDatasAutoSug.clear();
                        listUserDetailsDatasAutoSug = model.responseData.users;
                        if (listUserDetailsDatasAutoSug.length > 0) {
                          expandableController.expanded = true;
                        }
                        FocusScope.of(context).requestFocus(new FocusNode());
                        setState(() {
                          isLoading = false;
                        });
                      } else {}
                    }
                  });
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        } catch (e) {
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        }
      }
    }
  }

  getUserNoteByLeadIDAPI() async {
    try {
      await APIViewModel().req<UserNotesAPIModel>(
          context: context,
          url: ServerIntr.GET_USER_NOTE_BY_LEADID_URL
              .replaceAll("#leadId#", resolutions.id.toString()),
          reqType: ReqType.Get,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                listUserNotes = model.responseData.userNotes;
              }
            }
          });
    } catch (e) {}
  }

  getLeadbyResCompanyIdAPI() async {
    try {
      await APIViewModel().req<GetLeadByResCompanyIdAPIModel>(
          context: context,
          url: ServerIntr.GET_RES_URL
              .replaceAll("#resId#", widget.resolution.id.toString()),
          reqType: ReqType.Get,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                resolutions = model.responseData.resolution;
                try {
                  searchController.text = resolutions.assigneeName;
                } catch (e) {}
              }
            }
          });
    } catch (e) {}
  }

  getSyncRefreshAPI() async {
    try {
      final query = "?CurrentUserId=" +
          resolutions.userId.toString() +
          "&EntityId=" +
          resolutions.id.toString() +
          "&EntityName=Lead&Type=Email&UserCompanyId=" +
          userData.userModel.id.toString();
      await APIViewModel().req<GetLeadByResCompanyIdAPIModel>(
          context: context,
          url: ServerIntr.GET_USERNOTE_EMAILSYNC + query,
          reqType: ReqType.Get,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {}
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    resolutions = null;
    listUserNotes = null;
    listUserDetailsDatasAutoSug = null;
    userDetailsDatas = null;
    scrollController.dispose();
    expandableController.dispose();
    streamController.close();
    super.dispose();
  }

  appInit() async {
    try {
      streamController.stream
          .debounce(Duration(seconds: 1))
          .listen((s) => _validateValues());

      await getLeadbyResCompanyIdAPI();
      await getUserNoteByLeadIDAPI();
      setState(() {});
    } catch (e) {
      print(e.toString());
    }
  }

  go2PostUserNote() async {
    await Get.to(() => LeadDetailsAddNote(resolutions: resolutions));
    await getUserNoteByLeadIDAPI();
    setState(() {});
  }

  go2NewEmail() {
    Get.to(() => EmailEditorPage(resolutions: resolutions)).then((value) {
      getUserNoteByLeadIDAPI();
    });
  }

  doAdded() {
    //Get.to(() => ChangeStage(eStage: eStageName.Added));
  }

  doQualifying() {
    if (resolutions.stage != EnumGen.getEnum2Str(eLeadStage.Converted)) {
      Get.to(() => ChangeStage(
          eStage: eStageName.Qualifying, resolutions: resolutions)).then((res) {
        if (res != null) {
          resolutions = res;
          setState(() {});
        }
      });
    }
  }

  doProcessing() {
    if (resolutions.stage != EnumGen.getEnum2Str(eLeadStage.Converted)) {
      Get.to(() => ChangeStage(
          eStage: eStageName.Processing, resolutions: resolutions)).then((res) {
        if (res != null) {
          resolutions = res;
          setState(() {});
        }
      });
    }
  }

  doConverted() async {
    /*if (resolutions.stage != EnumGen.getEnum2Str(eLeadStage.Converted)) {
      await Get.to(() =>
          ChangeStage(eStage: eStageName.Converted, resolutions: resolutions));
      await getLeadbyResCompanyIdAPI();
    }*/
  }

  doJunkLead() {
    Get.to(() =>
            ChangeStage(eStage: eStageName.JunkLead, resolutions: resolutions))
        .then((res) {
      if (res != null) {
        resolutions = res;
        setState(() {});
      }
    });
  }

  doJunk() {
    //Get.to(() => ChangeStage(eStage: eStageName.Junk));
  }

  doJunk2Qualifying() {
    Get.to(() => ChangeStage(
        eStage: eStageName.Junk_to_Qualifying,
        resolutions: resolutions)).then((res) {
      if (res != null) {
        resolutions = res;
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          elevation: 1,
          backgroundColor: MyTheme.titleColor,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          title: FittedBox(
            fit: BoxFit.fitWidth,
            child: AutoSizeText("Lead details",
                style: TextStyle(
                    color: Colors.white,
                    //fontSize: 17,
                    fontWeight: FontWeight.bold)),
          ),
          bottom: (widget.resolution.stage !=
                  EnumGen.getEnum2Str(eLeadStage.Converted))
              ? PreferredSize(
                  preferredSize: new Size(getW(context), getHP(context, 7)),
                  child: Column(
                    children: [
                      (isLoading)
                          ? AppbarBotProgBar(
                              backgroundColor: MyTheme.appbarProgColor,
                            )
                          : SizedBox(),
                      resolutions != null
                          ? drawTopbar(resolutions)
                          : SizedBox(),
                    ],
                  ),
                )
              : null,
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.to(() => CreateLeadPage(
                        resolutions: resolutions,
                      )).then((value) {
                    if (value != null) Get.back();
                  });
                },
                icon: Icon(Icons.add_outlined, color: Colors.white)),
          ],
        ),
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    if (resolutions == null) return SizedBox();
    final name = (resolutions.firstName ?? '') +
        " " +
        (resolutions.middleName ?? '') +
        " " +
        (resolutions.lastName ?? '');
    Widget wid;
    switch (botIndex) {
      case 0:
        wid = drawBotActivity();
        break;
      case 1:
        wid = drawBotNotes();
        break;
      case 2:
        wid = drawBotEmail();
        break;
      case 3:
        wid = drawBotSMS();
        break;
      default:
    }
    return Container(
      width: getW(context),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 10),
            Container(
              width: getWP(context, 16),
              height: getWP(context, 16),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: MyNetworkImage.loadProfileImage(
                      resolutions.profileImageUrl),
                  fit: BoxFit.cover,
                ),
                shape: BoxShape.circle,
              ),
            ),
            (resolutions.stage == EnumGen.getEnum2Str(eLeadStage.Converted))
                ? GestureDetector(
                    onTap: () {
                      Get.to(() => CreateNewCustomer(
                          resolutions: resolutions, isEditCustomer: true));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 5),
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 5,
                            horizontal: 2), //adds padding inside the button
                        decoration: BoxDecoration(
                            border: Border.all(color: MyTheme.inputColor)),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(Icons.edit,
                                size: 15, color: MyTheme.inputColor),
                            Flexible(
                                child: Text(
                              "Edit Customer",
                              style: TextStyle(
                                  fontSize: 13, color: MyTheme.inputColor),
                            )),
                            SizedBox(width: 5)
                          ],
                        ),
                      ),
                    ),
                  )
                : SizedBox(height: 5),
            Txt(
                txt: name ?? '',
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .3,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 5),
            Txt(
                txt: resolutions.resolutionType ?? '',
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .5,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 5),
            Txt(
                txt: resolutions.title ?? '',
                txtColor: Colors.red.shade400,
                txtSize: MyTheme.txtSize - .6,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 5),
            drawCircleButtons(resolutions),
            drawBasicInfo(),
            Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: Divider(color: Colors.grey, height: 10)),
            //drawLeadInfo(),
            drawActivityButtons(),
            wid != null ? wid : SizedBox(),
          ],
        ),
      ),
    );
  }

  drawBasicInfo() {
    var creditRating = "0";
    try {
      final v = double.parse(resolutions.creditRating);
      creditRating = v.toStringAsFixed(2);
    } catch (e) {}

    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.phone,
                  color: Colors.grey,
                  size: 20,
                ),
                SizedBox(width: 10),
                Flexible(
                  child: Txt(
                      txt: 'PHONE NUMBER',
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 2, left: 30),
              child: Row(
                children: [
                  Flexible(
                    child: Txt(
                        txt: resolutions.phoneNumber ?? '',
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  SizedBox(width: 5),
                  GestureDetector(
                    onTap: () {
                      confirmInputDialog(
                          context: context,
                          title: "Change Phone Number",
                          value: resolutions.phoneNumber ?? '',
                          keyboardType: TextInputType.phone,
                          inputLen: 20,
                          callback: (phoneNumber) async {
                            if (phoneNumber.isNotEmpty &&
                                phoneNumber.length >=
                                    UserProfileVal.PHONE_LIMIT) {
                              try {
                                final param =
                                    CaseLeadNegotiatorHelper().putParam(
                                  caseType: resolutions.title,
                                  title: resolutions.description,
                                  fname: resolutions.firstName,
                                  mname: resolutions.middleName,
                                  lname: resolutions.lastName,
                                  email: resolutions.emailAddress,
                                  mobile: phoneNumber,
                                  note: resolutions.leadNote,
                                  dob: resolutions.dateOfBirth,
                                  //
                                  negotiatorId: resolutions.negotiatorId ?? 0,
                                  groupId: resolutions.groupId ?? 0,
                                  //
                                  leadReferenceId: resolutions.leadReferenceId,
                                  leadAddr: resolutions.address,
                                  leadMortgageLengthLeft:
                                      resolutions.mortgageBorrowLength,
                                  leadHomeValue: resolutions.homeValue,
                                  leadMortgagePurpose:
                                      resolutions.mortgagePurpose,
                                  leadMortgageAmount:
                                      resolutions.mortgageAmount,
                                  leadMortgageBorrowLength:
                                      resolutions.mortgageBorrowLength,
                                  leadCreditRating: resolutions.creditRating,
                                  leadEmploymentStatus:
                                      resolutions.employmentStatus,
                                  leadYearlyIncome: resolutions.yearlyIncome,
                                  leadEmailConsent: resolutions.emailConsent,
                                  estimatedEarning:
                                      resolutions.estimatedEarning.toString() ??
                                          '0',
                                  //
                                  areYouChargingAFee:
                                      resolutions.areYouChargingAFee ?? 'No',
                                  chargeFeeAmount:
                                      resolutions.chargeFeeAmount.toString() ??
                                          '0',
                                  chargingFeeWhenPayable:
                                      resolutions.chargingFeeWhenPayable ?? '',
                                  chargingFeeRefundable:
                                      resolutions.chargingFeeRefundable ?? 'No',
                                  areYouChargingAnotherFee:
                                      resolutions.areYouChargingAnotherFee ??
                                          'No',
                                  chargeAnotherFeeAmount: resolutions
                                          .chargeAnotherFeeAmount
                                          .toString() ??
                                      '0',
                                  chargingAnotherFeeWhenPayable: resolutions
                                          .chargingAnotherFeeWhenPayable ??
                                      '',
                                  chargingAnotherFeeRefundable: resolutions
                                          .chargingAnotherFeeRefundable ??
                                      'No',
                                  //
                                  resolution: resolutions,
                                );
                                myLog(json.encode(param));
                                CaseLeadNavigatorAPIMgr().wsPutResolutionAPI(
                                    context: context,
                                    param: param,
                                    callback: (model) {
                                      if (mounted && model != null) {
                                        if (model.success) {
                                          resolutions =
                                              model.responseData.resolution;
                                          setState(() {});
                                        } else {
                                          final err = model
                                              .errorMessages.resolution_post[0]
                                              .toString();
                                          showToast(
                                              context: context,
                                              msg: err,
                                              which: 0);
                                        }
                                      }
                                    });
                              } catch (e) {}
                            }
                          });
                    },
                    child: Icon(Icons.edit, color: Colors.grey, size: 20),
                  )
                ],
              ),
            ),
            SizedBox(height: 20),
            Row(
              children: [
                Icon(
                  Icons.email,
                  color: Colors.grey,
                  size: 20,
                ),
                SizedBox(width: 10),
                Flexible(
                  child: Txt(
                      txt: 'EMAIL ADDRESS',
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 2, left: 30),
              child: Txt(
                  txt: resolutions.emailAddress ?? '',
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            SizedBox(height: 20),
            resolutions != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.person,
                            color: Colors.grey,
                            size: 20,
                          ),
                          SizedBox(width: 10),
                          Flexible(
                            child: Txt(
                                txt: 'LEAD REFERENCE BY',
                                txtColor: Colors.grey,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 2, left: 30),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Txt(
                                txt: "Lead Id: " + resolutions.id.toString(),
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: false),
                            SizedBox(height: 5),
                            Txt(
                                txt: "Created by: " +
                                        resolutions.createdByUserName ??
                                    '',
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: false),
                            resolutions.initiatorCompanyName != null
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 5),
                                    child: Txt(
                                        txt: resolutions.initiatorCompanyName ??
                                            '',
                                        txtColor: MyTheme.inputColor,
                                        txtSize: MyTheme.txtSize - .4,
                                        txtAlign: TextAlign.start,
                                        isBold: false),
                                  )
                                : SizedBox(),
                            SizedBox(height: 5),
                            Txt(
                                txt: "Date: " +
                                        DateFun.getDate(
                                            resolutions.creationDate,
                                            "dd MMM yyyy") ??
                                    '',
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ],
                        ),
                      ),
                      SizedBox(height: 20),
                      Row(
                        children: [
                          Icon(
                            Icons.check_circle,
                            color: Colors.grey,
                            size: 20,
                          ),
                          SizedBox(width: 10),
                          Flexible(
                            child: Txt(
                                txt: 'LEAD STATUS',
                                txtColor: Colors.grey,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 2, left: 30),
                        child: Txt(
                            txt: resolutions.leadStatus ?? '',
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .4,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ),
                      SizedBox(height: 10),
                      Divider(color: Colors.black),
                      drawAreYourCharging(resolutions.areYouChargingAFee,
                          resolutions.areYouChargingAnotherFee),
                      drawTitleTxtTable("Lead note", resolutions.leadNote),
                      drawTitleTxtTable("Description", resolutions.description),
                      drawTitleTxtTable(
                          "Lead ReferenceId", resolutions.leadReferenceId),
                      drawTitleTxtTable(
                          "Date Of birth", resolutions.dateOfBirth),
                      drawTitleTxtTable("Address", resolutions.address),
                      resolutions.areYouChargingAFee == 'Yes'
                          ? Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Container(
                                color: MyTheme.purpleColor.withOpacity(.1),
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Column(
                                    children: [
                                      drawTitleTxtTable(
                                          "How much",
                                          resolutions.chargeFeeAmount != null
                                              ? resolutions.chargeFeeAmount
                                                  .toString()
                                              : null),
                                      drawTitleTxtTable(
                                          "When payable",
                                          resolutions.chargingFeeWhenPayable !=
                                                  null
                                              ? resolutions
                                                  .chargingFeeWhenPayable
                                                  .toString()
                                              : null),
                                      drawTitleTxtTable(
                                          "Refundable",
                                          resolutions.chargingFeeRefundable !=
                                                  null
                                              ? resolutions
                                                  .chargingFeeRefundable
                                                  .toString()
                                              : null),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                      resolutions.areYouChargingAnotherFee == 'Yes'
                          ? Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Container(
                                color: MyTheme.purpleColor.withOpacity(.1),
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Column(
                                    children: [
                                      drawTitleTxtTable(
                                          "How much another",
                                          resolutions.chargeAnotherFeeAmount !=
                                                  null
                                              ? resolutions
                                                  .chargeAnotherFeeAmount
                                                  .toString()
                                              : null),
                                      drawTitleTxtTable(
                                          "When payable another",
                                          resolutions.chargingAnotherFeeWhenPayable !=
                                                  null
                                              ? resolutions
                                                  .chargingAnotherFeeWhenPayable
                                                  .toString()
                                              : null),
                                      drawTitleTxtTable(
                                          "Refundable another",
                                          resolutions.chargingAnotherFeeRefundable !=
                                                  null
                                              ? resolutions
                                                  .chargingAnotherFeeRefundable
                                                  .toString()
                                              : null),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                      drawTitleTxtTable("Mortgage length left",
                          resolutions.mortgageLengthLeft),
                      drawTitleTxtTable(
                          "Valuation/Purchase price",
                          resolutions.homeValue != null
                              ? (AppDefine.CUR_SIGN + resolutions.homeValue)
                              : null),
                      drawTitleTxtTable(
                          "Deposit amount", resolutions.mortgagePurpose),
                      drawTitleTxtTable(
                          "Loan amount",
                          resolutions.mortgageAmount != null
                              ? (AppDefine.CUR_SIGN +
                                  resolutions.mortgageAmount)
                              : null),
                      drawTitleTxtTable("Mortgage length left",
                          resolutions.mortgageLengthLeft),
                      drawTitleTxtTable("Mortgage borrow length",
                          resolutions.mortgageBorrowLength),
                      drawTitleTxtTable("LTV", creditRating + "%"),
                      drawTitleTxtTable(
                          "Employment status", resolutions.employmentStatus),
                      drawTitleTxtTable(
                          "Yearly income", resolutions.yearlyIncome),
                      drawTitleTxtTable(
                          "Email consent", resolutions.emailConsent),
                      drawTitleTxtTable(
                          "Consultant", resolutions.consultantName),
                    ],
                  )
                : SizedBox(),
            SizedBox(height: 20),
            Txt(
                txt: "ASSIGNED TO :",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 5),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: getWP(context, 25),
                  decoration: BoxDecoration(
                      color: Colors.grey.shade200,
                      border: Border.all(
                        color: Colors.black26,
                      ),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5),
                        bottomLeft: Radius.circular(5),
                      )),
                  child: Padding(
                    padding: const EdgeInsets.all(9),
                    child: Txt(
                        txt: "Case owner",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                ),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(5),
                        bottomRight: Radius.circular(5),
                      ),
                      border: Border(
                          left: BorderSide(color: Colors.grey, width: 1),
                          right: BorderSide(color: Colors.grey, width: 1),
                          top: BorderSide(color: Colors.grey, width: 1),
                          bottom: BorderSide(color: Colors.grey, width: 1)),
                      color: Colors.transparent,
                    ),
                    child: ExpandablePanel(
                      theme: ExpandableThemeData(
                          expandIcon: Icons.arrow_drop_down,
                          collapseIcon: Icons.arrow_drop_up,
                          iconSize: 20),
                      controller: expandableController,
                      collapsed: null,
                      header: Container(
                        color: Colors.transparent,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                                flex: 1,
                                child: Icon(
                                  Icons.search,
                                  color: Colors.grey,
                                  size: 20,
                                )),
                            Expanded(
                              flex: 6,
                              child: TextField(
                                controller: searchController,
                                focusNode: focusSearch,
                                textInputAction: TextInputAction.next,
                                onChanged: (t) async {
                                  streamController.add(t);
                                },
                                decoration: new InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 0, vertical: 10),
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  hintText: "Enter case owner name/email/phone",
                                  hintStyle: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal),
                                  //fillColor: Colors.black,
                                ),
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                ),
                                keyboardType: TextInputType.text,
                              ),
                            ),
                            Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: () {
                                    userDetailsDatas = null;
                                    searchController.clear();
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                    setState(() {});
                                  },
                                  child: searchController.text.length > 0
                                      ? Icon(
                                          Icons.close,
                                          color: Colors.grey,
                                          size: 15,
                                        )
                                      : SizedBox(),
                                )),
                          ],
                        ),
                      ),
                      expanded: Container(
                        height: getHP(context,
                            listUserDetailsDatasAutoSug.length > 0 ? 30 : 0),
                        //width: getW(context),
                        child: Scrollbar(
                          isAlwaysShown: listUserDetailsDatasAutoSug.length > 0
                              ? true
                              : false,
                          controller: scrollController,
                          child: ListView(
                              shrinkWrap: true,
                              primary: false,
                              scrollDirection: Axis.vertical,
                              controller: scrollController,
                              children: [
                                ...listUserDetailsDatasAutoSug.map((model) {
                                  return GestureDetector(
                                    onTap: () {
                                      expandableController.expanded = false;
                                      userDetailsDatas = model;
                                      searchController.text =
                                          userDetailsDatas.firstName +
                                              ' ' +
                                              userDetailsDatas.lastName;
                                      setState(() {});
                                      final now = DateTime.now().toString();
                                      confirmDialog(
                                          context: context,
                                          title: "",
                                          msg:
                                              "Are you sure, you want to assign to " +
                                                  userDetailsDatas.name +
                                                  "? ",
                                          callbackYes: () async {
                                            var assigneeId = 0;
                                            try {
                                              assigneeId = userDetailsDatas.id;
                                            } catch (e) {}
                                            final param = {
                                              "UserId": resolutions.userId,
                                              "CreationDate": now,
                                              "Status": 101,
                                              "Title": resolutions.title,
                                              "Description": resolutions
                                                  .description, //"Help to Buy Mortgage",
                                              "Remarks": "",
                                              "EmailAddress": resolutions
                                                  .emailAddress, //"asdfas@sdafdas.com",
                                              "PhoneNumber": resolutions
                                                  .phoneNumber, //"3545454545456",
                                              "InitiatorId": userData
                                                  .userModel.id, //137962,
                                              "ServiceDate": now,
                                              "ResolutionType": resolutions !=
                                                      null
                                                  ? resolutions.resolutionType
                                                  : 'Lead From Introducer',
                                              "ParentId": 0,
                                              "ProfileImageUrl":
                                                  resolutions.profileImageUrl ??
                                                      '',
                                              "ProfileOwnerName": resolutions
                                                      .profileOwnerName ??
                                                  '', //"fasdfas asdfasfd",
                                              "Complainee": null,
                                              "ComplaineeUrl": null,
                                              "AssigneeId":
                                                  assigneeId ?? 0, //135556,
                                              "AssigneeName": userDetailsDatas
                                                  .name, //"bela sa205",
                                              "AssignDate": now,
                                              "ResolvedDate": now,
                                              "TaskStatus": null,
                                              "UserComunity": null,
                                              "ResolutionsUrl": resolutions
                                                      .resolutionsUrl ??
                                                  '', //"residential-mortgage-230956",
                                              "UserCompanyId": userData
                                                  .userModel.userCompanyID,
                                              "LeadStatus":
                                                  resolutions.leadStatus ??
                                                      '', //"Call back Later",
                                              "LeadNote": resolutions
                                                      .leadNote ??
                                                  '', // "asfasdfasfasdfasf",
                                              "FirstName": resolutions
                                                  .firstName, // "fasdfas",
                                              "MiddleName":
                                                  resolutions.middleName ??
                                                      '', //"asdfas",
                                              "NamePrefix":
                                                  resolutions.namePrefix ??
                                                      '', //null,
                                              "LastName": resolutions
                                                  .lastName, //"asdfasfd",
                                              "InitiatorImageUrl": null,
                                              "InitiatorName": userDetailsDatas
                                                  .name, //"bela sa205",
                                              "InitiatorCompanyName": resolutions
                                                      .initiatorCompanyName ??
                                                  '', //"Ten Right Angle",
                                              "Stage": resolutions
                                                  .stage, // "Processing",
                                              "Probability":
                                                  resolutions.probability ??
                                                      0, //0,
                                              "CompanyName":
                                                  resolutions.companyName ?? '',
                                              "FileUrl":
                                                  resolutions.fileUrl ?? '',
                                              "LeadReferenceId":
                                                  resolutions.leadReferenceId ??
                                                      '', //"asdf",
                                              "DateOfBirth":
                                                  resolutions.dateOfBirth ??
                                                      '', //"31-12-2012",
                                              "Address": resolutions.address ??
                                                  '', //"adfasdf asdfasdf",
                                              "MortgageLengthLeft": resolutions
                                                      .mortgageLengthLeft ??
                                                  '', //"asdfasdfas",
                                              "HomeValue":
                                                  resolutions.homeValue ??
                                                      '', //"asdfasf",
                                              "MortgagePurpose":
                                                  resolutions.mortgagePurpose ??
                                                      '', //"asfdasdfas",
                                              "MortgageAmount":
                                                  resolutions.mortgageAmount ??
                                                      '', //"asf",
                                              "MortgageBorrowLength": resolutions
                                                      .mortgageBorrowLength ??
                                                  '', //"asdfasf",
                                              "CreditRating":
                                                  resolutions.creditRating ??
                                                      '', //"asdfasdf",
                                              "EmploymentStatus": resolutions
                                                      .employmentStatus ??
                                                  '', //"asdfas",
                                              "YearlyIncome":
                                                  resolutions.yearlyIncome ??
                                                      '', //"asdfasf",
                                              "EmailConsent":
                                                  resolutions.emailConsent ??
                                                      '', //"Yes",
                                              "Qualifier":
                                                  resolutions.qualifier ?? '',
                                              "EstimatedEarning": resolutions
                                                      .estimatedEarning ??
                                                  0, //33,
                                              "ForwardedCompanyName": resolutions
                                                      .forwardedCompanyName ??
                                                  '', //"Ten Right Angle",
                                              "CreatedByUserId": userData
                                                  .userModel.id, //137962,
                                              "CreatedByUserName":
                                                  userDetailsDatas
                                                      .name, //"bela sa205",
                                              "IsLockAutoCall":
                                                  resolutions.isLockAutoCall ??
                                                      0,
                                              "SupportAdminId":
                                                  resolutions.supportAdminId ??
                                                      0,
                                              "UserNoteEntityName": resolutions
                                                      .userNoteEntityName ??
                                                  '',
                                              "UserNoteEntityId": resolutions
                                                      .userNoteEntityId ??
                                                  0,
                                              "NegotiatorId":
                                                  resolutions.negotiatorId ?? 0,
                                              "NegotiatorName":
                                                  resolutions.negotiatorName ??
                                                      '',
                                              "GroupId":
                                                  resolutions.groupId ?? 0,
                                              "AreYouChargingAFee": resolutions
                                                      .areYouChargingAFee ??
                                                  'No',
                                              "ChargeFeeAmount":
                                                  resolutions.chargeFeeAmount ??
                                                      0,
                                              "ChargingFeeWhenPayable": resolutions
                                                      .chargingFeeWhenPayable ??
                                                  '',
                                              "ChargingFeeRefundable": resolutions
                                                      .chargingFeeRefundable ??
                                                  '',
                                              "AreYouChargingAnotherFee":
                                                  resolutions
                                                          .areYouChargingAnotherFee ??
                                                      'No',
                                              "ChargeAnotherFeeAmount": resolutions
                                                      .chargeAnotherFeeAmount ??
                                                  0,
                                              "ChargingAnotherFeeWhenPayable":
                                                  resolutions
                                                          .chargingAnotherFeeWhenPayable ??
                                                      '',
                                              "ChargingAnotherFeeRefundable":
                                                  resolutions
                                                          .chargingAnotherFeeRefundable ??
                                                      '',
                                              "NotePermissionType": null,
                                              "ConsultantId": 0,
                                              "ConsultantName": "",
                                              "Id": resolutions.id, //230956
                                            };
                                            await APIViewModel().req<
                                                    UpdateCaseOwner4MeAPIModel>(
                                                context: context,
                                                url: ServerIntr
                                                    .PUT_UPDATE_CASEOWNER_TO_ME_URL,
                                                reqType: ReqType.Put,
                                                param: param,
                                                callback: (model) async {
                                                  if (mounted &&
                                                      model != null) {
                                                    if (model.success) {
                                                      //resolution =
                                                      //model.responseData.resolution;

                                                    }
                                                  }
                                                });
                                          });
                                    },
                                    child: Card(
                                      color: Colors.white,
                                      //elevation: 2,
                                      child: ListTile(
                                        leading: CircleAvatar(
                                          radius: 20,
                                          backgroundColor: Colors.transparent,
                                          backgroundImage:
                                              new CachedNetworkImageProvider(
                                                  MyNetworkImage.checkUrl(
                                                      model.profileImageUrl)),
                                        ),
                                        title: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            model.name != null
                                                ? Text(
                                                    model.name,
                                                    style: TextStyle(
                                                        color: Colors.black),
                                                  )
                                                : SizedBox(),
                                            model.email != null
                                                ? Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5),
                                                    child: Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: [
                                                        Icon(
                                                          Icons.email,
                                                          color: Colors.grey,
                                                          size: 15,
                                                        ),
                                                        Expanded(
                                                          child: Txt(
                                                              txt: model.email,
                                                              txtColor: MyTheme
                                                                  .inputColor,
                                                              txtSize: MyTheme
                                                                      .txtSize -
                                                                  .6,
                                                              txtAlign:
                                                                  TextAlign
                                                                      .start,
                                                              isBold: false),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                : SizedBox(),
                                            model.mobileNumber != null
                                                ? Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5),
                                                    child: Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: [
                                                        Icon(
                                                          Icons.phone,
                                                          color: Colors.grey,
                                                          size: 15,
                                                        ),
                                                        Expanded(
                                                          child: Txt(
                                                              txt: model
                                                                  .mobileNumber,
                                                              txtColor: MyTheme
                                                                  .inputColor,
                                                              txtSize: MyTheme
                                                                      .txtSize -
                                                                  .6,
                                                              txtAlign:
                                                                  TextAlign
                                                                      .start,
                                                              isBold: false),
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                : SizedBox(),
                                            /*SizedBox(height: 5),
                                                    Container(
                                                      color: Colors.white,
                                                      height: 2,
                                                    )*/
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ]),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            /*ddCaseOwner.listOptionItems.length > 0
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Icon(
                                  Icons.person,
                                  color: Colors.grey,
                                  size: 20,
                                ),
                                SizedBox(width: 10),
                                Flexible(
                                  child: Txt(
                                      txt: 'ASSIGNED TO :',
                                      txtColor: Colors.grey,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.start,
                                      isBold: false),
                                ),
                              ],
                            ),
                            SizedBox(height: 5),
                            resolutions != null
                                ? Container(
                                    //height: getHP(context, 6.5),
                                    child: Row(children: [
                                      Flexible(
                                        flex: 2,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: MyTheme.purpleColor,
                                            borderRadius:
                                                BorderRadius.circular(0),
                                            border: Border(
                                                left: BorderSide(
                                                    color: Colors.grey,
                                                    width: 1),
                                                right: BorderSide(
                                                    color: Colors.grey,
                                                    width: 1),
                                                top: BorderSide(
                                                    color: Colors.grey,
                                                    width: 1),
                                                bottom: BorderSide(
                                                    color: Colors.grey,
                                                    width: 1)),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 9,
                                                bottom: 9,
                                                left: 5,
                                                right: 5),
                                            child: Txt(
                                                txt: 'Case Owner',
                                                txtColor: Colors.white,
                                                txtSize: MyTheme.txtSize - .5,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                          flex: 7,
                                          child: DropDownListDialog(
                                            context: context,
                                            title: optCaseOwner.title,
                                            ddTitleList: ddCaseOwner,
                                            radius: 0,
                                            vPadding: 2,
                                            txtSize: (MyTheme.txtSize - .4),
                                            callback: (optionItem) async {
                                              Future.delayed(
                                                  Duration(milliseconds: 500),
                                                  () {
                                                if (mounted) {
                                                  final now =
                                                      DateTime.now().toString();
                                                  confirmDialog(
                                                      context: context,
                                                      title: "",
                                                      msg:
                                                          "Are you sure, you want to assign to " +
                                                              optionItem.title +
                                                              "? ",
                                                      callbackYes: () async {
                                                        optCaseOwner =
                                                            optionItem;
                                                        setState(() {});
                                                        var assigneeId = 0;
                                                        try {
                                                          assigneeId =
                                                              int.parse(
                                                                  optCaseOwner
                                                                      .id);
                                                        } catch (e) {}
                                                        final param = {
                                                          "UserId": resolutions
                                                              .userId,
                                                          "CreationDate": now,
                                                          "Status": 101,
                                                          "Title":
                                                              resolutions.title,
                                                          "Description": resolutions
                                                              .description, //"Help to Buy Mortgage",
                                                          "Remarks": "",
                                                          "EmailAddress":
                                                              resolutions
                                                                  .emailAddress, //"asdfas@sdafdas.com",
                                                          "PhoneNumber": resolutions
                                                              .phoneNumber, //"3545454545456",
                                                          "InitiatorId":
                                                              userData.userModel
                                                                  .id, //137962,
                                                          "ServiceDate": now,
                                                          "ResolutionType":
                                                              resolutions !=
                                                                      null
                                                                  ? resolutions
                                                                      .resolutionType
                                                                  : 'Lead From Introducer',
                                                          "ParentId": 0,
                                                          "ProfileImageUrl":
                                                              resolutions
                                                                      .profileImageUrl ??
                                                                  '',
                                                          "ProfileOwnerName":
                                                              resolutions
                                                                      .profileOwnerName ??
                                                                  '', //"fasdfas asdfasfd",
                                                          "Complainee": null,
                                                          "ComplaineeUrl": null,
                                                          "AssigneeId":
                                                              assigneeId ??
                                                                  0, //135556,
                                                          "AssigneeName": optionItem
                                                              .title, //"bela sa205",
                                                          "AssignDate": now,
                                                          "ResolvedDate": now,
                                                          "TaskStatus": null,
                                                          "UserComunity": null,
                                                          "ResolutionsUrl": resolutions
                                                                  .resolutionsUrl ??
                                                              '', //"residential-mortgage-230956",
                                                          "UserCompanyId":
                                                              userData.userModel
                                                                  .userCompanyID,
                                                          "LeadStatus": resolutions
                                                                  .leadStatus ??
                                                              '', //"Call back Later",
                                                          "LeadNote": resolutions
                                                                  .leadNote ??
                                                              '', // "asfasdfasfasdfasf",
                                                          "FirstName": resolutions
                                                              .firstName, // "fasdfas",
                                                          "MiddleName": resolutions
                                                                  .middleName ??
                                                              '', //"asdfas",
                                                          "NamePrefix": resolutions
                                                                  .namePrefix ??
                                                              '', //null,
                                                          "LastName": resolutions
                                                              .lastName, //"asdfasfd",
                                                          "InitiatorImageUrl":
                                                              null,
                                                          "InitiatorName":
                                                              optionItem
                                                                  .title, //"bela sa205",
                                                          "InitiatorCompanyName":
                                                              resolutions
                                                                      .initiatorCompanyName ??
                                                                  '', //"Ten Right Angle",
                                                          "Stage": resolutions
                                                              .stage, // "Processing",
                                                          "Probability": resolutions
                                                                  .probability ??
                                                              0, //0,
                                                          "CompanyName": resolutions
                                                                  .companyName ??
                                                              '',
                                                          "FileUrl": resolutions
                                                                  .fileUrl ??
                                                              '',
                                                          "LeadReferenceId":
                                                              resolutions
                                                                      .leadReferenceId ??
                                                                  '', //"asdf",
                                                          "DateOfBirth": resolutions
                                                                  .dateOfBirth ??
                                                              '', //"31-12-2012",
                                                          "Address": resolutions
                                                                  .address ??
                                                              '', //"adfasdf asdfasdf",
                                                          "MortgageLengthLeft":
                                                              resolutions
                                                                      .mortgageLengthLeft ??
                                                                  '', //"asdfasdfas",
                                                          "HomeValue": resolutions
                                                                  .homeValue ??
                                                              '', //"asdfasf",
                                                          "MortgagePurpose": resolutions
                                                                  .mortgagePurpose ??
                                                              '', //"asfdasdfas",
                                                          "MortgageAmount":
                                                              resolutions
                                                                      .mortgageAmount ??
                                                                  '', //"asf",
                                                          "MortgageBorrowLength":
                                                              resolutions
                                                                      .mortgageBorrowLength ??
                                                                  '', //"asdfasf",
                                                          "CreditRating": resolutions
                                                                  .creditRating ??
                                                              '', //"asdfasdf",
                                                          "EmploymentStatus":
                                                              resolutions
                                                                      .employmentStatus ??
                                                                  '', //"asdfas",
                                                          "YearlyIncome": resolutions
                                                                  .yearlyIncome ??
                                                              '', //"asdfasf",
                                                          "EmailConsent":
                                                              resolutions
                                                                      .emailConsent ??
                                                                  '', //"Yes",
                                                          "Qualifier": resolutions
                                                                  .qualifier ??
                                                              '',
                                                          "EstimatedEarning":
                                                              resolutions
                                                                      .estimatedEarning ??
                                                                  0, //33,
                                                          "ForwardedCompanyName":
                                                              resolutions
                                                                      .forwardedCompanyName ??
                                                                  '', //"Ten Right Angle",
                                                          "CreatedByUserId":
                                                              userData.userModel
                                                                  .id, //137962,
                                                          "CreatedByUserName":
                                                              optionItem
                                                                  .title, //"bela sa205",
                                                          "IsLockAutoCall":
                                                              resolutions
                                                                      .isLockAutoCall ??
                                                                  0,
                                                          "SupportAdminId":
                                                              resolutions
                                                                      .supportAdminId ??
                                                                  0,
                                                          "UserNoteEntityName":
                                                              resolutions
                                                                      .userNoteEntityName ??
                                                                  '',
                                                          "UserNoteEntityId":
                                                              resolutions
                                                                      .userNoteEntityId ??
                                                                  0,
                                                          "NegotiatorId":
                                                              resolutions
                                                                      .negotiatorId ??
                                                                  0,
                                                          "NegotiatorName":
                                                              resolutions
                                                                      .negotiatorName ??
                                                                  '',
                                                          "GroupId": resolutions
                                                                  .groupId ??
                                                              0,
                                                          "AreYouChargingAFee":
                                                              resolutions
                                                                      .areYouChargingAFee ??
                                                                  'No',
                                                          "ChargeFeeAmount":
                                                              resolutions
                                                                      .chargeFeeAmount ??
                                                                  0,
                                                          "ChargingFeeWhenPayable":
                                                              resolutions
                                                                      .chargingFeeWhenPayable ??
                                                                  '',
                                                          "ChargingFeeRefundable":
                                                              resolutions
                                                                      .chargingFeeRefundable ??
                                                                  '',
                                                          "AreYouChargingAnotherFee":
                                                              resolutions
                                                                      .areYouChargingAnotherFee ??
                                                                  'No',
                                                          "ChargeAnotherFeeAmount":
                                                              resolutions
                                                                      .chargeAnotherFeeAmount ??
                                                                  0,
                                                          "ChargingAnotherFeeWhenPayable":
                                                              resolutions
                                                                      .chargingAnotherFeeWhenPayable ??
                                                                  '',
                                                          "ChargingAnotherFeeRefundable":
                                                              resolutions
                                                                      .chargingAnotherFeeRefundable ??
                                                                  '',
                                                          "NotePermissionType":
                                                              null,
                                                          "ConsultantId": 0,
                                                          "ConsultantName": "",
                                                          "Id": resolutions
                                                              .id, //230956
                                                        };
                                                        await APIViewModel().req<
                                                                UpdateCaseOwner4MeAPIModel>(
                                                            context: context,
                                                            url: ServerIntr
                                                                .PUT_UPDATE_CASEOWNER_TO_ME_URL,
                                                            reqType:
                                                                ReqType.Put,
                                                            param: param,
                                                            callback:
                                                                (model) async {
                                                              if (mounted &&
                                                                  model !=
                                                                      null) {
                                                                if (model
                                                                    .success) {
                                                                  //resolution =
                                                                  //model.responseData.resolution;

                                                                }
                                                              }
                                                            });
                                                      });
                                                }
                                              });
                                            },
                                          ))
                                    ]),
                                  )
                                : SizedBox(),
                            /*resolutions != null
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 5),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Txt(
                                            txt: 'Selected Case Owner: ',
                                            txtColor: Colors.black87,
                                            txtSize: MyTheme.txtSize - .4,
                                            txtAlign: TextAlign.start,
                                            isBold: false),
                                        SizedBox(width: 5),
                                        Txt(
                                            txt: optCaseOwner.title,
                                            txtColor: Colors.black,
                                            txtSize: MyTheme.txtSize - .4,
                                            txtAlign: TextAlign.start,
                                            isBold: true),
                                      ],
                                    ),
                                  )
                                : SizedBox()*/
                          ],
                        ),
                      ),
                    ],
                  )
                : SizedBox()*/
          ],
        ),
      ),
    );
  }

  /*drawLeadInfo() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.phone,
                      color: Colors.grey,
                      size: 20,
                    ),
                    SizedBox(width: 10),
                    Flexible(
                      child: Txt(
                          txt: 'Lead Note',
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.start,
                          isBold: true),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 2, left: 30),
                  child: Txt(
                      txt: resolutions.leadNote ?? '',
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                SizedBox(height: 20),
                Row(
                  children: [
                    Icon(
                      Icons.email,
                      color: Colors.grey,
                      size: 20,
                    ),
                    SizedBox(width: 10),
                    Flexible(
                      child: Txt(
                          txt: 'Lead ReferenceId',
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.start,
                          isBold: true),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 2, left: 30),
                  child: Txt(
                      txt: resolutions.leadReferenceId ?? 'Your refer Id',
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                SizedBox(height: 20),
                Row(
                  children: [
                    Icon(
                      Icons.person,
                      color: Colors.grey,
                      size: 20,
                    ),
                    SizedBox(width: 10),
                    Flexible(
                      child: Txt(
                          txt: 'Date Of Birth',
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.start,
                          isBold: true),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 2, left: 30),
                  child: Txt(
                      txt: resolutions.dateOfBirth ?? '',
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                SizedBox(height: 20),
                Row(
                  children: [
                    Icon(
                      Icons.check_circle,
                      color: Colors.grey,
                      size: 20,
                    ),
                    SizedBox(width: 10),
                    Flexible(
                      child: Txt(
                          txt: 'Address',
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.start,
                          isBold: true),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 2, left: 30),
                  child: Txt(
                      txt: resolutions.address ?? '',
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }*/

  drawActivityButtons() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            drawBotButton(0, "Activity", Icons.auto_graph, () {
              setState(() {});
            }),
            SizedBox(width: 20),
            drawBotButton(1, "Note", Icons.copy_outlined, () {
              setState(() {});
            }),
            /*SizedBox(width: 20),
            drawBotButton(2, "Email", Icons.email_outlined, () {
              setState(() {});
            }),
            SizedBox(width: 20),
            drawBotButton(3, "SMS", Icons.sms_outlined, () {
              setState(() {});
            })*/
          ],
        ),
      ),
    );
  }

  //  *************************************** Bottom UI

  drawBotActivity() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 2,
                  child: Txt(
                      txt: 'Contact Activity',
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                Flexible(
                  child: GestureDetector(
                    onTap: () async {
                      await getUserNoteByLeadIDAPI();
                      setState(() {});
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 5,
                            horizontal: 5), //adds padding inside the button
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                                color: MyTheme.inputColor, width: .5)),
                        child: Text(
                          "Refresh",
                          style: TextStyle(
                              fontSize: 12, color: MyTheme.inputColor),
                        )),
                  ),
                ),
                Flexible(
                  child: GestureDetector(
                    onTap: () {
                      go2PostUserNote();
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 5,
                            horizontal: 5), //adds padding inside the button
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                                color: MyTheme.inputColor, width: .5)),
                        child: Text(
                          "Add note",
                          style: TextStyle(
                              fontSize: 12, color: MyTheme.inputColor),
                        )),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: listUserNotes.length,
                itemBuilder: (context, index) {
                  return drawContactActivityView(listUserNotes[index]);
                }),
          ],
        ),
      ),
    );
  }

  drawBotNotes() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 2,
                  child: Txt(
                      txt: 'Note Activity',
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                /* Flexible(
                  child: GestureDetector(
                    onTap: () async {
                      await getUserNoteByLeadIDAPI();
                      setState(() {});
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 5,
                            horizontal: 5), //adds padding inside the button
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(color: MyTheme.inputColor, width: .5)),
                        child: Text(
                          "Refresh",
                          style: TextStyle(
                              fontSize: 12, color: MyTheme.inputColor),
                        )),
                  ),
                ),*/
                Flexible(
                  child: GestureDetector(
                    onTap: () {
                      go2PostUserNote();
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 5,
                            horizontal: 5), //adds padding inside the button
                        decoration: BoxDecoration(
                          border:
                              Border.all(color: MyTheme.inputColor, width: .5),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          "Add note",
                          style: TextStyle(
                              fontSize: 12, color: MyTheme.inputColor),
                        )),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: listUserNotes.length,
                itemBuilder: (context, index) {
                  return drawUserNoteView(listUserNotes[index]);
                }),
          ],
        ),
      ),
    );
  }

  drawBotEmail() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Txt(
                      txt: 'Email History',
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                Flexible(
                  child: GestureDetector(
                    onTap: () async {
                      await getSyncRefreshAPI();
                      setState(() {});
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 5,
                            horizontal: 5), //adds padding inside the button
                        decoration: BoxDecoration(
                            border: Border.all(color: MyTheme.inputColor)),
                        child: Text(
                          "Sync & Refresh",
                          style: TextStyle(
                              fontSize: 13, color: MyTheme.inputColor),
                        )),
                  ),
                ),
                Flexible(
                  child: GestureDetector(
                    onTap: () {
                      go2NewEmail();
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 5,
                            horizontal: 5), //adds padding inside the button
                        decoration: BoxDecoration(
                            border: Border.all(color: MyTheme.inputColor)),
                        child: Text(
                          "New Email",
                          style: TextStyle(
                              fontSize: 13, color: MyTheme.inputColor),
                        )),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: listUserNotes.length,
                itemBuilder: (context, index) {
                  return drawEmailView(listUserNotes[index]);
                }),
          ],
        ),
      ),
    );
  }

  drawBotSMS() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Txt(
                      txt: 'SMS History',
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                Flexible(
                  child: GestureDetector(
                    onTap: () {
                      go2PostUserNote();
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 5,
                            horizontal: 5), //adds padding inside the button
                        decoration: BoxDecoration(
                            border: Border.all(color: MyTheme.inputColor)),
                        child: Text(
                          "New SMS",
                          style: TextStyle(
                              fontSize: 13, color: MyTheme.inputColor),
                        )),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: listUserNotes.length,
                itemBuilder: (context, index) {
                  return drawSMSView(listUserNotes[index]);
                }),
          ],
        ),
      ),
    );
  }
}
