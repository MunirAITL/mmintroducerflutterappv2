import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../view_model/rx/ManageLeadSearchController.dart';
import '../../widgets/btn/MMBtn.dart';
import '../../widgets/dialog/DatePickerView.dart';
import '../../widgets/dropdown/DropDownPicker.dart';
import '../../widgets/txt/Txt.dart';
import 'package:intl/intl.dart';

enum eSearch {
  Stage,
  Status,
  DueDate,
}

abstract class BaseLead<T extends StatefulWidget> extends State<T> with Mixin {
  final manageLeadController = Get.put(ManageLeadSearchController());

  refreshData();
  doSearch(eSearch v);

  final searchByTxt = TextEditingController();

  final DateTime dateNow = DateTime.now();

  var date1 = "";
  var date2 = "";
  var time1 = TimeOfDay.now();
  var time2 = TimeOfDay.now();

  var datelast;
  var datefirst;
  var datelast2;
  var datefirst2;

  //  list dropdown case owner
  final selectCaseOwner = "Select case owner";
  DropListModel ddCaseOwner = DropListModel([]);
  OptionItem optCaseOwner;

  double wBox;
  String selectedDateTime = "";
  List<String> itemsDateTime = [
    "",
    "12 Hours",
    "24 Hours",
    "2 Days",
    "7 Days",
    "Custom Date Time",
    "All Dates",
  ];

  final List<String> listSortBy = [
    "Task Due Date",
    "Last Update Date",
    "My Last Call",
    "My Last Email",
    "My Last Text",
    "Last Creation Date",
    "Name",
    "None"
  ];

  drawNF() {
    return Container(
      width: getW(context),
      height: getH(context) / 2,
      //color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        //shrinkWrap: true,
        children: [
          Container(
            width: getWP(context, 50),
            height: getWP(context, 30),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                    "assets/images/screens/db_cus/my_cases/case_nf2.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          //SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: Container(
              child: Txt(
                txt: "Manage listing not found.\nTry with other options",
                txtColor: MyTheme.mycasesNFBtnColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
                //txtLineSpace: 1.5,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> drawTimePicker(TimeOfDay t, Function(TimeOfDay) callback) async {
    final picked_s = await showTimePicker(
        context: context,
        initialTime: t,
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme: ColorScheme.light(
                // change the border color
                primary: MyTheme.bgColor,
                // change the text color
                onSurface: Colors.purple,
              ),
              // button colors
              buttonTheme: ButtonThemeData(
                colorScheme: ColorScheme.light(
                  primary: Colors.green,
                ),
              ),
            ),
            child: child,
          );
        });
    if (picked_s != null && picked_s != t) {
      callback(picked_s);
    }
  }

  drawCustomDateTime() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(height: 5),
          _drawCusDateRow("From", date1, datefirst, datelast, datefirst,
              (DateTime d) {
            setState(() {
              try {
                time1 = TimeOfDay.now();
                date1 = DateFormat('dd-MMM-yyyy HH:mm').format(d).toString();
                drawTimePicker(time1, (t) {
                  final dd = DateTime(d.year, d.month, d.day, t.hour, t.minute);
                  date1 = DateFormat('dd-MMM-yyyy HH:mm').format(dd).toString();
                  print(t);
                });
              } catch (e) {
                myLog(e.toString());
              }
            });
          }),
          SizedBox(height: 3),
          _drawCusDateRow("To", date2, datefirst2, datelast2, DateTime.now(),
              (d) {
            setState(() {
              try {
                time2 = TimeOfDay.now();
                date2 = DateFormat('dd-MMM-yyyy HH:mm').format(d).toString();
                drawTimePicker(time2, (t) {
                  final dd = DateTime(d.year, d.month, d.day, t.hour, t.minute);
                  date2 = DateFormat('dd-MMM-yyyy HH:mm').format(dd).toString();
                  print(t);
                });
              } catch (e) {
                myLog(e.toString());
              }
            });
          }),
        ],
      ),
    );
  }

  _drawCusDateRow(String title, String dLabel, fdate, ldate, initialDate,
      Function(DateTime) callback) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: getWP(context, 25),
          decoration: BoxDecoration(
              color: Colors.grey.shade200,
              border: Border.all(
                color: Colors.black26,
              ),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5),
                bottomLeft: Radius.circular(5),
              )),
          child: Padding(
            padding: const EdgeInsets.all(9),
            child: Txt(
                txt: title,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.center,
                isBold: false),
          ),
        ),
        Expanded(
          flex: 3,
          child: GestureDetector(
            onTap: () {
              showDatePicker(
                context: context,
                initialDate: initialDate,
                firstDate: fdate,
                lastDate: ldate,
                fieldHintText: "dd/mm/yyyy",
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme: ColorScheme.light(
                        // change the border color
                        primary: MyTheme.bgColor,
                        // change the text color
                        onSurface: Colors.purple,
                      ),
                      // button colors
                      buttonTheme: ButtonThemeData(
                        colorScheme: ColorScheme.light(
                          primary: Colors.green,
                        ),
                      ),
                    ),
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  callback(value);
                }
              });
            },
            child: Container(
              width: getW(context),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: .5),
                  borderRadius: BorderRadius.all(Radius.circular(0))),
              child: Padding(
                padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
                child: Text(
                  dLabel,
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  drawHeaderRadioDialog({
    String title,
    List<String> list,
    eSearch e,
  }) {
    return GestureDetector(
      onTap: () {
        showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                backgroundColor: Colors.white,
                content: StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                    return Obx(() => SingleChildScrollView(
                          primary: true,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Txt(
                                  txt: title,
                                  txtColor: MyTheme.titleColor,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: true),
                              Divider(
                                color: Colors.black,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: List<Widget>.generate(list.length,
                                    (int index) {
                                  return Theme(
                                    data: MyTheme.radioThemeData,
                                    child: Row(
                                      children: <Widget>[
                                        Radio<int>(
                                          groupValue: (e == eSearch.Stage)
                                              ? manageLeadController
                                                  .stageRB.value
                                              : manageLeadController
                                                  .statusRB.value,
                                          value: index,
                                          onChanged: (newValue) {
                                            (e == eSearch.Stage)
                                                ? manageLeadController
                                                    .stageRB.value = newValue
                                                : manageLeadController
                                                    .statusRB.value = newValue;
                                          },
                                        ),
                                        Flexible(
                                            child: Text(list[index],
                                                style: TextStyle(
                                                    color: Colors.black))),
                                      ],
                                    ),
                                  );
                                }),
                              ),
                              MMBtn(
                                  txt: "Search",
                                  height: getHP(context, 6),
                                  width: getWP(context, 20),
                                  callback: () {
                                    Navigator.pop(context);
                                    doSearch(e);
                                  }),
                            ],
                          ),
                        ));
                  },
                ),
              );
            });
      },
      child: Container(
        padding: EdgeInsets.all(0),
        width: wBox,
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(
              color: Colors.black,
            ),
            borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Obx(() => Text(
                (e == eSearch.Stage)
                    ? list[manageLeadController.stageRB.value]
                    : list[manageLeadController.statusRB.value],
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black),
              )),
        ),
      ),
    );
  }

  drawHeaderCustomDialog() {
    return GestureDetector(
        onTap: () {
          showDialog<void>(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  backgroundColor: Colors.white,
                  content: StatefulBuilder(
                    builder: (BuildContext context, StateSetter setState) {
                      return SingleChildScrollView(
                        primary: true,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Align(
                                alignment: Alignment.center,
                                child: Obx(
                                  () => Txt(
                                      txt: listSortBy[
                                          manageLeadController.sortByRB.value],
                                      txtColor: MyTheme.titleColor,
                                      txtSize: MyTheme.txtSize,
                                      txtAlign: TextAlign.start,
                                      isBold: true),
                                )),
                            Divider(
                              color: Colors.black,
                            ),
                            DropDownPicker(
                              cap: "Case owner",
                              itemSelected: optCaseOwner,
                              dropListModel: ddCaseOwner,
                              onOptionSelected: (optionItem) {
                                optCaseOwner = optionItem;
                                setState(() {});
                              },
                            ),
                            SizedBox(height: 20),
                            Text(
                              'Sort by',
                              style: TextStyle(
                                  fontSize: 17, color: MyTheme.inputColor),
                            ),
                            Obx(() => Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: List<Widget>.generate(
                                      listSortBy.length, (int index) {
                                    return Theme(
                                      data: MyTheme.radioThemeData,
                                      child: Row(
                                        children: <Widget>[
                                          Radio<int>(
                                            groupValue: manageLeadController
                                                .sortByRB.value,
                                            value: index,
                                            onChanged: (newValue) {
                                              manageLeadController
                                                  .sortByRB.value = newValue;
                                            },
                                          ),
                                          Flexible(
                                              child: Text(listSortBy[index],
                                                  style: TextStyle(
                                                      color: Colors.black))),
                                        ],
                                      ),
                                    );
                                  }),
                                )),
                            SizedBox(height: 20),
                            Align(
                              alignment: Alignment.center,
                              child: MMBtn(
                                  txt: "Search",
                                  height: getHP(context, 6),
                                  width: getWP(context, 20),
                                  callback: () {
                                    Navigator.pop(context);
                                    doSearch(eSearch.DueDate);
                                  }),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                );
              });
        },
        child: Container(
          padding: EdgeInsets.all(0),
          width: wBox,
          decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(
                color: Colors.black,
              ),
              borderRadius: BorderRadius.all(Radius.circular(20))),
          child: Padding(
            padding: const EdgeInsets.all(5),
            child: Obx(
              () => Text(
                listSortBy[manageLeadController.sortByRB.value],
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black),
              ),
            ),
          ),
        ));
  }
}
