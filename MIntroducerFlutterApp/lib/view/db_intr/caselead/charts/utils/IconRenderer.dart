/// Bar chart with custom symbol in legend example.
import 'package:flutter/material.dart';
//import 'package:flutter/material.dart' as mat;
import 'package:charts_flutter/flutter.dart';
//import 'package:charts_flutter/flutter.dart' as charts;

/// Example custom renderer that renders [IconData].
///
/// This is used to show that legend symbols can be assigned a custom symbol.
class IconRenderer extends CircleSymbolRenderer {
  final IconData iconData;

  IconRenderer(this.iconData);

  @override
  Widget build(BuildContext context, {Size size}) {
    return new SizedBox.fromSize(
        size: size, child: new Icon(iconData, size: 12.0));
  }
}
