import 'dart:convert';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/auth/LoginApiMgr.dart';
import 'package:aitl/controller/api/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/ProfileHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/hreflogin/LoginAccExistsByEmailApiModel.dart';
import 'package:aitl/model/json/auth/hreflogin/PostEmailOtpAPIModel.dart';
import 'package:aitl/model/json/auth/hreflogin/SendUserEmailOtpAPIModel.dart';
import 'package:aitl/model/json/auth/otp/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/auth/otp/OtpByMobilePage.dart';
import 'package:aitl/view/base_app.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/href_login_dialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../../NoAccess.dart';
import 'check_email_page.dart';

class SigninPage extends StatefulWidget {
  final hrefLoginData;
  const SigninPage({Key key, this.hrefLoginData}) : super(key: key);

  @override
  _SigninPageState createState() => _SigninPageState();
}

class _SigninPageState extends BaseApp<SigninPage> {
  final email = TextEditingController();

  callCheckUserEmailAPI() async {
    try {
      await APIViewModel().req<LoginAccExistsByEmailApiModel>(
          context: context,
          url: Server.LOGINACCEXISTSBYEMAIL_POST_URL,
          reqType: ReqType.Post,
          param: {"Email": email.text.trim()},
          callback: (model1) async {
            if (mounted) {
              if (model1 != null) {
                if (model1.success) {
                  await APIViewModel().req<PostEmailOtpAPIModel>(
                    context: context,
                    url: Server.POSTEMAILOTP_POST_URL,
                    reqType: ReqType.Post,
                    param: {
                      "Email": email.text.trim(),
                      "Status": "101",
                    },
                    callback: (model2) async {
                      if (mounted) {
                        if (model2 != null) {
                          if (model2.success) {
                            await APIViewModel().req<SendUserEmailOtpAPIModel>(
                                context: context,
                                url: Server.SENDUSEREMAILOTP_GET_URL.replaceAll(
                                    "#otpId#",
                                    model2.responseData.userOTP.id.toString()),
                                reqType: ReqType.Get,
                                callback: (model3) async {
                                  if (mounted) {
                                    if (model3 != null) {
                                      if (model3.success) {
                                        Get.to(() => CheckEmailPage(
                                            userOTP:
                                                model3.responseData.userOTP,
                                            email: email.text.trim()));
                                      } else {
                                        final err = model1.messages.login[0];
                                        showToast(
                                            context: context,
                                            msg: err,
                                            which: 0);
                                      }
                                    }
                                  }
                                });
                          } else {
                            final err = model1.messages.login[0];
                            showToast(context: context, msg: err, which: 0);
                          }
                        }
                      }
                    },
                  );
                } else {
                  Get.dialog(HrefLoginDialog(
                      context: context,
                      callback: () {
                        Get.off(
                            () => RegScreen(emailAddress: email.text.trim()));
                      }));
                }
              }
            }
          });
    } catch (e) {}
  }

  callOtpMobileAPI() async {
    var countryCode = "+44";
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryCode = cd;
        });
      }
    } catch (e) {
      print("Sms2 screen country code and name problem ");
    }
    Sms2APIMgr().wsLoginMobileOtpPostAPI(
      context: context,
      countryCode: countryCode,
      mobile: email.text.trim(),
      callback: (model) {
        if (model != null && mounted) {
          try {
            if (model.success) {
              Sms2APIMgr().wsSendOtpNotiAPI(
                  context: context,
                  otpId: model.responseData.userOTP.id,
                  callback: (model) {
                    if (model != null && mounted) {
                      try {
                        if (model.success) {
                          Get.to(() => OtpByMobilePage(
                              mobile: model.responseData.userOTP.mobileNumber));
                        } else {
                          try {
                            if (mounted) {
                              final err =
                                  model.messages.postUserotp[0].toString();
                              showToast(context: context, msg: err);
                            }
                          } catch (e) {
                            myLog(e.toString());
                          }
                        }
                      } catch (e) {
                        myLog(e.toString());
                      }
                    }
                  });
            } else {
              try {
                if (mounted) {
                  final err = model.messages.postUserotp[0].toString();
                  showToast(context: context, msg: err);
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          } catch (e) {
            myLog(e.toString());
          }
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    //testLogin();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        resizeToAvoidBottomInset: false,
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.black,
              )),
          SizedBox(height: 20),
          Align(
              alignment: Alignment.center,
              child: Image.asset(
                "assets/images/logo/mm.png",
                fit: BoxFit.cover,
                width: getWP(context, 70),
              )),
          SizedBox(height: 20),
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Txt(
                      txt:
                          "Enter the email or mobile associated with your account.",
                      txtColor: MyTheme.inputColor,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      fontWeight: FontWeight.w500,
                      isBold: false,
                    ),
                  ),
                  SizedBox(height: 20),
                  Txt(
                    txt: "Email / Mobile:",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    fontWeight: FontWeight.w400,
                    isBold: false,
                  ),
                  TextField(
                      controller: email,
                      autofocus: true,
                      maxLength: 50,
                      autocorrect: false,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize + .5),
                        height: MyTheme.txtLineSpace,
                      ),
                      keyboardType: TextInputType.text,
                      decoration: new InputDecoration(
                        isDense: true,
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                        counterText: '',
                        enabledBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.black, width: .5),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.black, width: .5),
                        ),
                        border: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.white, width: .5),
                        ),
                      )),
                  SizedBox(height: 30),
                  MMBtn(
                      txt: "Next",
                      txtColor: Colors.white,
                      height: getHP(context, 7),
                      width: getW(context),
                      radius: 10,
                      callback: () {
                        //if (!Server.isOtp) {
                        //testLogin();
                        //} else {

                        if (email.text.trim().isEmpty) {
                          showToast(
                              context: context,
                              msg: "Invalid email or mobile entered");
                        } else if (email.text.contains("@")) {
                          if (RegExp(UserProfileVal.EMAIL_REG)
                              .hasMatch(email.text.trim())) {
                            callCheckUserEmailAPI();
                          } else {
                            showToast(
                                context: context,
                                msg: "Invalid email address entered");
                          }
                        } else {
                          final str = email.text.trim();
                          try {
                            if (Common.isNumeric(str) &&
                                str.length > UserProfileVal.PHONE_LIMIT) {
                              callOtpMobileAPI();
                            } else {
                              showToast(
                                  context: context,
                                  msg: "Invalid mobile number entered");
                            }
                          } catch (e) {
                            showToast(
                                context: context,
                                msg: "Invalid mobile number entered");
                          }
                        }
                      }),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
