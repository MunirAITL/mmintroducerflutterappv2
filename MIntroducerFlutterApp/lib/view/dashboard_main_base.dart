import 'package:aitl/Mixin.dart';
import 'package:aitl/view/common/ActionReqPage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controller/helper/db_cus/tab_newcase/action_req_helper.dart';
import '../model/DashBoardListType.dart';
import 'db_cus/doc_scan/scan/selfie/selfie_page.dart';

abstract class BaseMainDashboard<T extends StatefulWidget> extends State<T>
    with Mixin {
  Widget widActionRequired;

  bool isLoading = false;

  refreshData();

  drawActionRequiredItems(listUserList, listUserNotesModel, caseReviewModelList,
      caseMortgageAgreementReviewInfosList, bool isMore) {
    widActionRequired = ActionReqHelper().drawActionRequiredItems(
        context: context,
        listUserList: listUserList,
        listUserNotesModel: listUserNotesModel,
        caseReviewModelList: caseReviewModelList,
        caseMortgageAgreementReviewInfosList:
            caseMortgageAgreementReviewInfosList,
        isMore: isMore,
        callbackReload: () {
          refreshData();
        },
        callbackRefresh: () {
          setState(() {
            drawActionRequiredItems(
                listUserList,
                listUserNotesModel,
                caseReviewModelList,
                caseMortgageAgreementReviewInfosList,
                isMore);
          });
        },
        callbackUserBadges: (Map<String, dynamic> map) {
          if (map["email"] == true) {
            final isOk = map["email"];
            if (isOk) {
              showToast(
                  context: context,
                  msg:
                      "We have sent an email for verification, please check your email",
                  which: 1);
            } else {
              showToast(
                  context: context,
                  msg: "Sorry, something went wrong",
                  which: 0);
            }
          } else if (map["eid"] == true) {
            Get.to(() => SelfiePage()).then((value) {
              refreshData();
            });
          } else {
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        },
        callbackUserNotes: (userNotesModel) {
          if (mounted) {
            refreshData();
          }
        },
        callbackMoreAR: (List<DashBoardListType> listUserList) async {
          //print(listUserList);
          widActionRequired = null;
          await Get.to(() => ActionReqPage(
                listUserList: listUserList,
                listUserNotesModel: listUserNotesModel,
                caseReviewModelList: caseReviewModelList,
                caseMortgageAgreementReviewInfosList:
                    caseMortgageAgreementReviewInfosList,
              ));
          if (mounted) refreshData();
        });
  }
}
